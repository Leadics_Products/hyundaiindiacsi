package com.leadics.application.common;

import com.leadics.utils.DataSourceUtil;
import com.leap.core.common.dao.DbCredentialsDao;
import com.leap.core.common.representations.DbCredentialsRepresentation;
import com.leap.core.common.spi.LpDataSource;
import com.leap.core.common.spi.MultitenantDataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@WebListener
public class MultitenantConfiguration implements ServletContextListener {

    public static Log log = LogFactory.getLog(MultitenantConfiguration.class);

    public static final String DRIVER = "com.mysql.jdbc.Driver";
    static final String DIALECT = "org.hibernate.dialect.MySQLDialect";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("On start web app");
        try {
            this.dataSource();
        } catch (Exception e) {
            throw new RuntimeException("Failed to load datasource ", e);
        }
    }


    public void dataSource() throws Exception {

        List<DbCredentialsRepresentation> entityList = tenantDbList();
        LpDataSource dataSource = new LpDataSource(entityList);

        MultitenantDataSource.setLpDataSource(dataSource);

    }

    private List<DbCredentialsRepresentation> tenantDbList() throws Exception {
        DataSource coreDataSource = mainDataSource();
        Properties properties = DataSourceUtil.properties;
        String accessKey = properties.getProperty("lp.accessKey");
        DbCredentialsDao dao = new DbCredentialsDao();
        Connection con = coreDataSource.getConnection();
        List<DbCredentialsRepresentation> representations = dao.findBy(con, accessKey);

        return representations;
    }

    /**
     * Creates the default data source for the application
     *
     * @return
     */
    private DataSource mainDataSource() {
        Properties props = DataSourceUtil.properties;

        String url = props.getProperty("hibernate.connection.url");
        String username = props.getProperty("hibernate.connection.username");
        String password = props.getProperty("hibernate.connection.password");
        String minString = props.getProperty("hibernate.hikari.minimumIdle");
        String maxString = props.getProperty("hibernate.hikari.maximumPoolSize");
        Integer minPool = Integer.parseInt(minString);
        Integer maxPool = Integer.parseInt(maxString);

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(password);
        config.setDriverClassName(DRIVER);
        config.setMinimumIdle(minPool);
        config.setMaximumPoolSize(maxPool);
        HikariDataSource ds = new HikariDataSource(config);
        return ds;
    }
}
