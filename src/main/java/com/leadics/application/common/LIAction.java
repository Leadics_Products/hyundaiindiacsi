/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.application.common;

import com.leadics.application.exceptions.ForbiddenException;
import com.leadics.application.exceptions.SessionExpiredException;
import com.leadics.application.service.LIControllermapService;
import com.leadics.application.to.LIControllermapRecord;
import com.leadics.consumers.ConsumerToInsertSystemUserRecord;
import com.leadics.consumers.ConsumerToUpdateSystemUserRecord;
import com.leadics.utils.PropertyUtil;
import com.leap.core.auth.spi.HttpRequestValidator;
import com.leap.core.configuration.Config;
import com.leap.core.configuration.LCoreConfiguration;
import com.leap.core.events.core.EventConfiguration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

//import static com.leadics.application.common.LIRecord.logger;

/**
 *
 * @author varma.sagi
 */
public class LIAction extends HttpServlet {


    private static final Set<String> NO_SESSION_ACTIONS = new HashSet<>(
            Arrays.asList("-Systemuserview-authenticate","-Systemuserview-logOut")
    );
    public static final String AUTHORIZATION = "Authorization";
    static {
         Integer APPLICATION_ID = Integer.parseInt(PropertyUtil.getProperty("APPLICATION_ID"));
         final  String APPNAMEFORSQS = PropertyUtil.getProperty("APPNAMEFORSQS");
        Config config = new Config();
        String domain = PropertyUtil.getProperty("domain");
        config.setDomain(domain);
        config.setEnvironment(PropertyUtil.getProperty("environment").toLowerCase());
        EventConfiguration eventConfig = new EventConfiguration();
        eventConfig.setConsumerName(APPNAMEFORSQS);
        eventConfig.setConsumersPackage("com.leap.core.events.consumers");
        LCoreConfiguration.setEventConfiguration(eventConfig);
        LCoreConfiguration.setConfig(config);
        try {
            new ConsumerToInsertSystemUserRecord();
            new ConsumerToUpdateSystemUserRecord();
        } catch (Exception e) {
//            logger.trace(
//                    "onEvent failed instantiate ConsumerToInsertSystemUserRecord or ConsumerToDeleteSystemUserRecord in hyundai-api"
//                            + e.getMessage());
        }
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LIAction</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LIAction at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.setContentType("text/html");
        request.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        try {
            HttpServletRequest req = request;
            HttpServletResponse res = response;
            String actionType = request.getParameter("hiddenActionType");
            response.setContentType("text/html");
            
            String tableCode = actionType.substring(0,
                    actionType.lastIndexOf('-') + 1);
            String actionCode = actionType.substring(actionType
                    .lastIndexOf('-') + 1);
            LIControllermapService service = new LIControllermapService();
            
            LIControllermapRecord searchRecord = new LIControllermapRecord();
            searchRecord.setCode(tableCode);
            LIControllermapRecord[] records = service
                    .searchLIControllermapRecords(searchRecord);
            Class c = Class.forName(records[0].getClassname());
            Method m = c.getDeclaredMethod("processWebRequest", new Class[]{
                HttpServletRequest.class,
                HttpServletResponse.class,
                String.class});
            Object i = c.newInstance();
            Object r = m.invoke(i,
                    new Object[]{request, response, actionCode});
        }
        catch(ForbiddenException fe) {
        out.println("-3");
            response.setStatus(403);
            return;
        }
        catch (SessionExpiredException se) {
            out.println("-2");
            response.setStatus(401);
            return;
        } catch (Exception exception) {
            exception.printStackTrace();
            out.println("Unauthorized Access");
            out.println("<H1><c>LeadICS</H1>");
        }
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    	 // TODO Auto-generated method stub
//        response.setContentType("text/html");
        request.setCharacterEncoding("utf-8");
        
        response.setContentType("application/json; charset=utf-8");
        PrintWriter out = response.getWriter();
        try {
            HttpServletRequest req = request;
            HttpServletResponse res = response;
            String actionType = request.getParameter("hiddenActionType");
           
            String tableCode = actionType.substring(0,
                    actionType.lastIndexOf('-') + 1);
            String actionCode = actionType.substring(actionType
                    .lastIndexOf('-') + 1);
            LIControllermapService service = new LIControllermapService();
         
            LIControllermapRecord searchRecord = new LIControllermapRecord();
            searchRecord.setCode(tableCode);
            LIControllermapRecord[] records = service
                    .searchLIControllermapRecords(searchRecord);
            Class c = Class.forName(records[0].getClassname());
            Method m = c.getDeclaredMethod("processWebRequest", new Class[]{
                HttpServletRequest.class,
                HttpServletResponse.class,
                String.class});
            Object i = c.newInstance();
            Object r = m.invoke(i,
                    new Object[]{request, response, actionCode});
        }
        catch(ForbiddenException fe) {
        out.println("-3");
            response.setStatus(403);
            return;
        }
        catch (SessionExpiredException se) {
            out.println("-2");
            response.setStatus(401);
            return;
        } catch (Exception exception) {
            exception.printStackTrace();
            out.println("Unauthorized Access");
            out.println("<H1><c>LeadICS</H1>");
        }
    }

 
  
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

