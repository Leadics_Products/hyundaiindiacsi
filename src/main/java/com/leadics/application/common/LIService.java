package com.leadics.application.common;

import com.amazonaws.util.IOUtils;
import com.google.common.base.Charsets;
import com.google.common.cache.LoadingCache;
import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.leadics.application.dao.LIReportquiresDAO;
import com.leadics.application.service.LIWhereconditionmapService;
import com.leadics.application.to.LIReportquiresRecord;
import com.leadics.application.to.LIWhereconditionmapRecord;
import com.leadics.application.webactions.common.ActionPlan;
import com.leadics.utils.DateUtils;
import com.leadics.utils.HierarchicalLookupData;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.ScopeGuavaCacheUtil;
import com.leap.core.auth.idam.adapters.KeycloakUser;
import com.leap.core.auth.idam.adapters.KeycloakUserFactory;
import com.leap.core.auth.idam.adapters.UserContext;
import com.leap.core.auth.internal.TrustedResource;
import com.leap.core.services.users.spi.UserClient;
import org.apache.http.protocol.HTTP;
import org.yaml.snakeyaml.Yaml;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.leadics.application.common.LIAction.AUTHORIZATION;

public class LIService {

    public static Map<String, String> obj;
    public static List<HashMap<String, String>> ActionPlanData;

    public static Map queryMap = null;
    public static final String MAIN_CRITERIA = "<<MAIN_CRITERIA>>";
    public static final String ACKNOWLEDGMENT = "ACKNOWLEDGMENT";
    public static final String CONDITION_START = "<<";
    public static final String CONDITION_END = ">>";
    public static final String DEFAULT_CRITERIA = "DEFAULT_DIMENSION";
    public static final String QUERY_TEMPLATE = "QUERY";

    static {
        Yaml yamlObj = new Yaml();
        InputStream inputStream = LIService.class.getClassLoader().getResourceAsStream("hmil_csi.yml");
        queryMap = yamlObj.load(inputStream);
    }



    public Map loadMap(String Query, String Column1, String Column2, boolean insertBlank)
            throws Exception {
        LIDAO dao = new LIDAO();
        return dao.loadMap(Query, Column1, Column2, insertBlank);
    }

    public HashMap<String, String> buildWherecondition(HttpServletRequest request, Map<String, LIWhereconditionmapRecord> filtersMap, String query) {
        HashMap<String, String> acknowledgmentMap = new HashMap<>();
        try {
            String authHeader = request.getHeader(AUTHORIZATION);
            KeycloakUser keycloakUser = KeycloakUserFactory.authenticate(authHeader);
            UserContext userContext = new UserContext(keycloakUser);
            UserContext uc = (UserContext) request.getAttribute("user");
            String whereCondition = "", condition = "";
            Set<String> whereMaps = filtersMap.keySet();
            String saltId = null;
            if (authHeader == null) {
                if (TrustedResource.isTrusted(request)) {
                    saltId = request.getParameter("userId");
                }
            } else {
                saltId = uc.getKeycloakUser().getId();
            }
            LoadingCache<String, Map<String,String>> scopeCache = ScopeGuavaCacheUtil.getLoadingCache();
            Map<String, String> criteriaMap = scopeCache.get(saltId);
            System.out.println("cache ended "+ LocalTime.now());
            String mainScope = criteriaMap.get("main");
            if (mainScope == null) {
                mainScope = criteriaMap.get(DEFAULT_CRITERIA);
            }
            mainScope = mainScope.toString().toLowerCase().replace("dealer", "dealer_id");
            mainScope = mainScope.toString().toLowerCase().replace("region", "region_id");

            query = query.replace(MAIN_CRITERIA, mainScope);
            Iterator whereMapIterator = whereMaps.iterator();

            while (whereMapIterator.hasNext()) {
                condition = "";
                String whereTemplate = (String) whereMapIterator.next();
                LIWhereconditionmapRecord whereRecord = (LIWhereconditionmapRecord) filtersMap.get(whereTemplate);

                if (whereRecord == null) {
                    continue;
                }



                if (whereRecord.getType().equalsIgnoreCase("constant")) {
                    // if the type if constant the get it from the properties file
                    String fieldValue = whereRecord.getColnames();
                    ResourceBundle rb = ResourceBundle.getBundle("application", Locale.getDefault());
                    String constantReplace = rb.getString(fieldValue);
                    if (constantReplace == null) {
                        throw new Exception("NO constant value set for " + fieldValue);
                    }
                    query = query.replace(whereRecord.getWheretemplate(), constantReplace);
                    continue;
                }
                if (whereRecord.getType().equalsIgnoreCase("trendFilter")) {
                    String replace = request.getParameter("timePeriod");
                    String trend = request.getParameter("timeTrends");
                    String[] dates = replace.split("-");
                    String startDate = DateUtils.formatDate(dates[0], "dd/mm/yyyy", "yyyy-mm-dd");
                    String endDate = DateUtils.formatDate(dates[1], "dd/mm/yyyy", "yyyy-mm-dd");
                    
                    query = query.replace("<<TRENDFILTER>>", " and start_date between '" + startDate + "' and '" + endDate + "' or end_date >= last_day('"+ startDate +"') and start_date <= '"+ startDate +"'");
                    continue;
                }


                List<String> filterList = this.convertCommaBasedToArray(whereRecord.getColnames());
                if (filterList != null) {
                    for (String filter : filterList) {
                        String values[] = request.getParameterValues(filter);

                        if (values == null || values.length == 1 && values[0].equalsIgnoreCase("All")) {

                        } else if (values.length == 1) {
                            // if it is single valued
                            condition += " and " + filter + "=\"" + values[0] + "\"";

                        } //if it is of type array
                        else {
                            condition += " and " + filter + " in (";
                            for (String value : values) {
                                condition += "\"" + value + "\",";
                            }
                            condition = replaceLast(condition, ",", ")");

                        }

                    }
                    query = query.replace(whereTemplate, condition);
                    acknowledgmentMap.put(whereTemplate, condition);
                }

            }

            acknowledgmentMap.put(QUERY_TEMPLATE, query);

            return acknowledgmentMap;
        } catch (Exception e) {
            e.printStackTrace();
            e.getStackTrace();
            return acknowledgmentMap;
        }
    }


    public static String replaceLast(String string, String toReplace, String replacement) {
        int pos = string.lastIndexOf(toReplace);
        if (pos > -1) {
            return string.substring(0, pos)
                    + replacement
                    + string.substring(pos + toReplace.length(), string.length());
        } else {
            return string;
        }
    }




    public String getQuery(String methodName) throws Exception {
        Map queryMaps = (Map) queryMap.get(methodName);
        return (String) queryMaps.get("QUERY");
    }

    public void writeOutput(HttpServletResponse response, Object object) throws Exception {
        response.setContentType("application/json");

        PrintWriter out = response.getWriter();
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String plainText;
        plainText = gson.toJson(object);
        String cipher = plainText;

        out.println(cipher);
    }

    public String getUniqueString() {
        return "" + (UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE);
    }

    public String getCurrentTimeStamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());

    }

    public Set<String> getFilterGroups(String query) {

        Set<String> groupSet = new HashSet<>();

        int i = 1;
        int start = ordinalIndexOf(query, "<<", i);
        int end = ordinalIndexOf(query, ">>", i) + 2;

        // if and only if start and end are greater than 0 then string has conditions else it will return -1
        while (start > 0 && end > start) {

            String cond = query.substring(start, end);
            groupSet.add(cond);
            // get the next where condition postions
            i++;
            start = ordinalIndexOf(query, "<<", i);
            end = ordinalIndexOf(query, ">>", i) + 2;
        }
        System.out.println("groupSet");
        System.out.println(groupSet);
        return groupSet;

    }

    public static int ordinalIndexOf(String str, String substr, int n) {
        int count = 0;
        int pos = -1;
        for (int i = 0; i < str.length() - 2; i++) {

            if (str.substring(i, i + substr.length()).equals(substr)) {

                count++;
                if (count == n) {
                    pos = i;
                    break;
                }
            }
        }
        return pos;
    }

    public Map<String, LIWhereconditionmapRecord> getFiltersMap(String query) throws Exception {

        // get all the filter groups by means of query
        Set<String> filterSet = this.getFilterGroups(query);

        LIWhereconditionmapService whereMapService = new LIWhereconditionmapService();
        LIWhereconditionmapRecord whereMapRecord;

        Map<String, LIWhereconditionmapRecord> filtersMap = new HashMap<>();
        Iterator filterGroupIterator = filterSet.iterator();
        while (filterGroupIterator.hasNext()) {
            String filterGroup = filterGroupIterator.next().toString();

            String queryForWhereGroup = "select * from  where_condition_map where where_template=\"" + filterGroup + "\"";

            whereMapRecord = whereMapService.loadLIWhereconditionmapRecord(queryForWhereGroup);

            if (whereMapRecord == null) {

                filtersMap.put(filterGroup, null);

            }

            // add it to the collection
            filtersMap.put(filterGroup, whereMapRecord);
        }

        return filtersMap;
    }

    public List<String> convertCommaBasedToArray(String str) {
        String groupFields = str;
        String field = "";
        if (str.contentEquals("") || str.contentEquals(" ") || str.trim().contentEquals("")) {
            return new ArrayList<>();
        }
        ArrayList<String> groupList = new ArrayList<>();
        // used to put into collection by filterGroup and its corresponding list of filter fields
        for (int i = 0; i < groupFields.length(); i++) {
            String ch = "" + groupFields.charAt(i);
            if (!ch.equals(",")) {
                //append all the characters of a field until , is reached
                field += ch;

            } else {
                // if , is found then it is a complete field
                groupList.add(field);
                field = "";
            }
        }

        // add the last element
        if (field != "") {
            groupList.add(field);
        }

        return groupList;
    }





    public String getQureyRebuild(HttpServletRequest request) throws Exception {
        Map queryMapInner = (Map) queryMap.get(request.getParameter("reportName"));
        String query = (String) queryMapInner.get("QUERY");
        System.out.println("query");
        System.out.println(query);
        Map<String, LIWhereconditionmapRecord> filtersMap = getFiltersMap(query);
        Set<String> whereMaps = filtersMap.keySet();
        Iterator<String> it = whereMaps.iterator();
        Map<String, String[]> mapValue = request.getParameterMap();

        while (it.hasNext()) {
            String tempString = it.next();
            System.out.println(tempString);
            if(queryMapInner.get(tempString) instanceof  String ){
                query=query.replace(tempString,(String) queryMapInner.get(tempString));
            }else
            {
                Map innerMap = (Map) queryMapInner.get(tempString);
                System.out.println(queryMapInner);
                System.out.println(innerMap);
                if (innerMap != null) {
                    query = getRebuiledQuery(innerMap, query, mapValue, tempString);
                }
            }

        }
        System.out.println("******************************************");
        System.out.println(query);
        filtersMap = getFiltersMap(query);
        Map<String, String> acknowledgmentMap = buildWherecondition(request, filtersMap, query);
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        return query;
    }

    private String getRebuiledQuery(Map queyMap, String query, Map<String, String[]> mapValue, String next) {

        for (Iterator it = queyMap.entrySet().iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();

            System.out.println(entry.getKey() + "/" + entry.getValue());

            for (Map.Entry<String, String[]> entrys : mapValue.entrySet()) {
                if (entrys.getValue().length > 0) {
                    if (entry.getKey().toString().equalsIgnoreCase(entrys.getKey().toString())) {
                        Map innerMap = (Map) entry.getValue();
                        if (innerMap.get(entrys.getValue()[0]) instanceof String || innerMap.get("**") instanceof String ) {
                            if(innerMap.containsKey("All") ||innerMap.containsKey("ALL") || innerMap.containsKey("all")  ){
                                if(innerMap.get("ALL") instanceof String || innerMap.get(entrys.getValue()) instanceof String)
                                {
                                    String innerQuery = (String) innerMap.get(entrys.getValue()[0]);
                                    System.out.println("replace " + next);
                                    if(innerQuery==null){
                                        innerQuery=(String) innerMap.get("**");
                                    }
                                    query = query.replace(next, innerQuery);
                                    return query;
                                }else {
                                    System.out.println(entrys.getValue()[0]);
                                    if(!entrys.getValue()[0].toString().equalsIgnoreCase("All")){
                                        String innerQuery = (String) innerMap.get("**");
                                        query = query.replace(next, innerQuery);
                                        return query;
                                    }
                                    else
                                    {
                                        if(innerMap.get(entrys.getValue()[0]) instanceof  String){
                                            String innerQuery = (String) innerMap.get(entrys.getValue()[0]);
                                            query = query.replace(next, innerQuery);
                                            return query;
                                        }else{
                                            innerMap = (Map) innerMap.get(entrys.getValue()[0]);
                                            return getRebuiledQuery(innerMap, query, mapValue, next);
                                        }
//                                        innerMap = (Map) innerMap.get(entrys.getValue()[0]);
//                                        return getRebuiledQuery(innerMap, query, mapValue, next);
                                    }

                                }
                            }
                            else if(innerMap.containsKey(entrys.getValue()[0])){
                                String innerQuery = (String) innerMap.get(entrys.getValue()[0]);
                                query = query.replace(next, innerQuery);
                                return query;
                            }
                        } else {
                            System.out.println(entrys.getValue()[0]);
                            if(!entrys.getValue()[0].toString().equalsIgnoreCase("All")){
                                //need to be refacotred
                                if(innerMap.containsKey("**") && innerMap.get("**") instanceof String ){
                                    String innerQuery = (String) innerMap.get("**");
                                    System.out.println("replace " + next);
                                    if(innerQuery==null){
                                        innerQuery=(String) innerMap.get("**");
                                    }
                                    query = query.replace(next, innerQuery);
                                    return query;
                                }else if(!innerMap.containsKey("**"))
                                {
                                    innerMap=(Map) innerMap.get(entrys.getValue()[0]);
                                    return getRebuiledQuery(innerMap, query, mapValue, next);
                                }else{
                                    innerMap=(Map) innerMap.get("**");
                                    return getRebuiledQuery(innerMap, query, mapValue, next);
                                }

                            }else{
                                if(innerMap.get(entrys.getValue()[0]) instanceof String){
                                    String innerQuery = (String) innerMap.get(entrys.getValue()[0]);
                                    query = query.replace(next, innerQuery);
                                    return query;
                                }else {
                                    innerMap = (Map) innerMap.get(entrys.getValue()[0]);
                                    return getRebuiledQuery(innerMap, query, mapValue, next);
                                }
                            }


                        }
                    }
                }
            }

        }
        return query;
    }



    public String getQueryFromDb(String actionString) throws Exception {
        LIReportquiresDAO reportquiresDAO = new LIReportquiresDAO();
        LIDAO dao = new LIDAO();
        LIReportquiresRecord reportsquiresRecord = reportquiresDAO
                .loadFirstLIReportquiresRecord("SELECT * FROM reportquires WHERE reportname=\'" + actionString + "\'");
        System.out.println(reportsquiresRecord.getQuery());
        return reportsquiresRecord.getQuery();
    }



    public String getQureyRebuildString(String query, HttpServletRequest request) throws Exception {
        Map<String, LIWhereconditionmapRecord> filtersMap = getFiltersMap(query);
        Set<String> whereMaps = filtersMap.keySet();
        Iterator<String> it = whereMaps.iterator();
        Map<String, String[]> mapValue = request.getParameterMap();
        Map queryMapInner = (Map) queryMap.get(request.getParameter("reportName"));
        while (it.hasNext()) {
            String tempString = it.next();
            System.out.println(tempString);
            if(queryMapInner.get(tempString) instanceof  String ){
                query=query.replace(tempString,(String) queryMapInner.get(tempString));
            }else
            {
                Map innerMap = (Map) queryMapInner.get(tempString);
                System.out.println(queryMapInner);
                System.out.println(innerMap);
                if (innerMap != null) {
                    query = getRebuiledQuery(innerMap, query, mapValue, tempString);
                }
            }

        }
        System.out.println("******************************************");
        System.out.println(query);
        filtersMap = getFiltersMap(query);
        Map<String, String> acknowledgmentMap = buildWherecondition(request, filtersMap, query);
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        return query;
    }

    public String getQureyRebuild(HttpServletRequest request, String query) throws Exception {
        Map queryMapInner = (Map) queryMap.get(request.getParameter("reportName"));
        Map<String, LIWhereconditionmapRecord> filtersMap = getFiltersMap(query);
        Set<String> whereMaps = filtersMap.keySet();
        Iterator<String> it = whereMaps.iterator();
        Map<String, String[]> mapValue = request.getParameterMap();

        while (it.hasNext()) {
            String tempString = it.next();
            System.out.println(tempString);
            if(queryMapInner.get(tempString) instanceof  String ){
                query=query.replace(tempString,(String) queryMapInner.get(tempString));
            }else
            {
                Map innerMap = (Map) queryMapInner.get(tempString);
                System.out.println(queryMapInner);
                System.out.println(innerMap);
                if (innerMap != null) {
                    query = getRebuiledQuery(innerMap, query, mapValue, tempString);
                }
            }

        }
        System.out.println("******************************************");
        System.out.println(query);
        filtersMap = getFiltersMap(query);
        Map<String, String> acknowledgmentMap = buildWherecondition(request, filtersMap, query);
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        return query;
    }
    public String getQureyRebuild(String reportName, String query,HttpServletRequest request) throws Exception {
        Map queryMapInner = (Map) queryMap.get(reportName);
        Map<String, LIWhereconditionmapRecord> filtersMap = getFiltersMap(query);
        Set<String> whereMaps = filtersMap.keySet();
        Iterator<String> it = whereMaps.iterator();
        Map<String, String[]> mapValue = request.getParameterMap();

        while (it.hasNext()) {
            String tempString = it.next();
            System.out.println(tempString);
            if(queryMapInner.get(tempString) instanceof  String ){
                query=query.replace(tempString,(String) queryMapInner.get(tempString));
            }else
            {
                Map innerMap = (Map) queryMapInner.get(tempString);
                System.out.println(queryMapInner);
                System.out.println(innerMap);
                if (innerMap != null) {
                    query = getRebuiledQuery(innerMap, query, mapValue, tempString);
                }
            }

        }
        System.out.println("******************************************");
        System.out.println(query);
        filtersMap = getFiltersMap(query);
        Map<String, String> acknowledgmentMap = buildWherecondition(request, filtersMap, query);
        query = acknowledgmentMap.get(QUERY_TEMPLATE);
        return query;
    }


    public List<ActionPlan.Detractor>  getPostBody(HttpServletRequest request) throws IOException  {

//        String body = IOUtils.toString(request.getInputStream());
       
    	String body = CharStreams.toString(new InputStreamReader(
    		      request.getInputStream(), Charsets.UTF_8));
    	
        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArr = (JsonArray)jsonParser.parse(body);
        List<ActionPlan.Detractor> listOfPostData = new ArrayList<ActionPlan.Detractor>();
        Gson gson = new Gson();
        Type listType = new TypeToken<List<ActionPlan.Detractor>>(){}.getType();
        listOfPostData = gson.fromJson(jsonArr ,listType);
        return listOfPostData;

    }
}
