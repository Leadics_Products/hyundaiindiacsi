/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.application.webactions;

import com.google.gson.Gson;
import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.application.to.LIWhereconditionmapRecord;
import com.leadics.application.to.NameValuePair;
import com.leap.core.auth.idam.adapters.UserContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.leadics.application.common.LIAction.AUTHORIZATION;

/**
 *
 * @author Krishna-Leadics
 */
public class FiltersGeneric { 
    Gson gson;
    public static final String QUERY_TEMPLATE = "QUERY";
    public static String USER = "user";

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException { 
        response.setContentType("text/html");
        gson = new Gson();
        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void genericFilters(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        String authHeader = request.getHeader(AUTHORIZATION);
        UserContext userContext = (UserContext) request.getAttribute(USER);
        List<Integer> roles = userContext.getRoles();
        Integer roleId = roles.get(0);
        LIDAO dao = new LIDAO();
        String query = "select * from (select * from config_filters) as a\n"
                + "left join\n"
                + "(select max(PARENTS) as  PARENTS,FILTER_NAME from config_page_filter group by 2) as b\n"
                + "on a.FILTER_NAME=b.FILTER_NAME group by a.FILTER_NAME,FILTER_ACTION_STRING";
        System.out.println("Query Filtes" + query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("FILTER_NAME", "FILTER_NAME");
        columnInfo.put("FILTER_ACTION_STRING", "FILTER_ACTION_STRING");
        columnInfo.put("PARENTS", "PARENTS");
        columnInfo.put("Filter_data_type", "Filter_data_type");
        columnInfo.put("Filter_data_table", "Filter_data_table");
        columnInfo.put("queryExits", "queryExits");
        columnInfo.put("roleCheckRequired", "roleCheckRequired");
        columnInfo.put("roleMapTable", "roleMapTable");
        columnInfo.put("dimensionObject", "dimensionObject");
        columnInfo.put("order_required", "order_required");
        columnInfo.put("order_column", "order_column");
        System.out.println("QUERUNG FILTERS :::::::: "+query);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        List rowFiltes = rows.stream().collect(
                Collectors.mapping(rowScore -> new PackerFilters(rowScore.get("FILTER_NAME"), genericFiltersData(rowScore.get("FILTER_NAME"), rowScore.get("FILTER_ACTION_STRING"), rowScore.get("PARENTS"), rowScore.get("Filter_data_table"), rowScore.get("Filter_data_type"), request,rowScore.get("queryExits"),rowScore.get("roleCheckRequired"),rowScore.get("roleMapTable"),roleId,rowScore.get("dimensionObject"),rowScore.get("order_required"),rowScore.get("order_column"))),
                        Collectors.toList()));
        service.writeOutput(response, rowFiltes);
    }
    public Map<List<String>, List<HashMap<String, String>>> getFiltesWithParent(String query, String filterName, String Parents, HttpServletRequest request) {
        try {
            LIService service = new LIService();
            LIDAO dao = new LIDAO();
            List<String> groupByFieldNames = Arrays.asList(Parents.split("\\s*,\\s*"));
            HashMap<String, String> columnInfo = new HashMap<>();
            for (String strTemp : groupByFieldNames) {
                columnInfo.put(strTemp, strTemp);
            }
            columnInfo.put(filterName, filterName);
            columnInfo.put("value", "value");

            Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
            Set<String> whereMaps = filtersMap.keySet();
            Iterator whereMapIterator = whereMaps.iterator();
            while (whereMapIterator.hasNext()) {
                String whereTemplate = (String) whereMapIterator.next();
            }
            Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
            query = acknowledgmentMap.get(QUERY_TEMPLATE);
            System.out.println("QUERUNG FILTERS :::::::: "+query);
            List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
            LinkedHashMap configurationList = rows
                    .stream()
                    .collect(Collectors.groupingBy((map) -> filterName, LinkedHashMap::new, Collectors.mapping(map
                            -> getDataPacked(map, groupByFieldNames, filterName) //                new FilterObjectCreate(filterName, new NameValuePair(map.get(filterName), map.get("value")))
                            ,
                             Collectors.toList())));
            return configurationList;
        } catch (Exception ex) {
            Logger.getLogger(FiltersGeneric.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    public LinkedHashMap getFiltesWithOutParent(String query, String filterName, String Parents, HttpServletRequest request) {
        try {
            LIService service = new LIService();
            LIDAO dao = new LIDAO();
            HashMap<String, String> columnInfo = new HashMap<>();
            final String column1 = filterName.replace("_id","");
            final String column2 = filterName;
            columnInfo.put(column1, column1);
            columnInfo.put(column2, column2);
            Map<String, LIWhereconditionmapRecord> filtersMap = null;
            try {
                filtersMap = service.getFiltersMap(query);
            } catch (Exception ex) {
                Logger.getLogger(FiltersGeneric.class.getName()).log(Level.SEVERE, null, ex);
            }
            Set<String> whereMaps = filtersMap.keySet();
            Iterator whereMapIterator = whereMaps.iterator();
            while (whereMapIterator.hasNext()) {
                String whereTemplate = (String) whereMapIterator.next();
                System.out.println("filter");
                System.out.println(whereTemplate);
            }
            Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
            query = acknowledgmentMap.get(QUERY_TEMPLATE);
            System.out.println("QUERUNG FILTERS :::::::: "+query);
            List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
            Iterator i = rows.iterator();
            LinkedList li = new LinkedList<>();
            LinkedHashMap configurationList = new LinkedHashMap();
            NameValuePair<String, String> nvp;
            while (i.hasNext()) {
                HashMap<String, String> map = (HashMap<String, String>) i.next();
                LinkedHashMap output = new LinkedHashMap();
                output.put(filterName, new NameValuePair(map.get(column1), map.get(column2)));
                li.add(output);

            }
            configurationList.put(filterName, li);
            return configurationList;

        } catch (SQLException ex) {
            Logger.getLogger(FiltersGeneric.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private LinkedHashMap genericFiltersData(String filterName, String actionString, String parents, String filterDataTable, String filterDataType, HttpServletRequest request, String queryExits, String StrRole, String roleMapTable, Integer roleId, String dimension, String order_required, String order_column) {
        try {
            LIService service = new LIService();
            LIDAO dao = new LIDAO();
            String query=null;
            String orderString="";
            if(order_required !=null && Integer.parseInt(order_required)==1){
                orderString=" ORDER BY "+order_column;
            }
            if(Integer.parseInt(queryExits)==1)            {
                 query=actionString;
                if (parents == null || parents.equals("")) {
                    return getFiltesWithOutParent(query, filterName, parents, request);
                }else{
                    return (LinkedHashMap) getFiltesWithParent(query, filterName, parents, request);
                }
            }else            {
                 query = getQueryCreated(filterName, parents, filterDataType, filterDataTable,StrRole,roleMapTable,roleId,dimension,orderString);
                if (parents == null || parents.equals("")) {
                    return getFiltesWithOutParent(query, filterName, parents, request);
                } else {
                    return (LinkedHashMap) getFiltesWithParent(query, filterName, parents, request);
                }

            }

        } catch (Exception ex) {
            Logger.getLogger(FiltersGeneric.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private HashMap getDataPacked(HashMap<String, String> map, List<String> groupByFieldNames, String filterName) {
        HashMap output = new HashMap();
        output.put(filterName, new NameValuePair(map.get(filterName), map.get("value")));
        groupByFieldNames.forEach(action -> {
            output.put(action, map.get(action));
        });
        return output;

    }

    private String getQueryCreated(String filterName, String parents, String filterDataType, String filterDataTable,String StrRole,String roleMapTable,Integer roleId,String dimension,String orderString) {

        String query = "";
        String originalFilterName = filterName;
        filterName = filterName.replaceAll("_id", "");
        if (StrRole==null || StrRole.equalsIgnoreCase("0"))
        {
            if (filterDataType.equalsIgnoreCase("static")) {
                query = "SELECT distinct value as " + filterName + " as " + originalFilterName + ",value as value  FROM  " + filterDataTable + "  where name='" + filterName + "' " +orderString;
                System.out.println(query);
                return query;
            } else if (filterDataType.equalsIgnoreCase("Non-Static")) {
                if (parents == null || parents.equals("")) {
                    query = "SELECT Distinct " + filterName + "," + originalFilterName + " as " + originalFilterName + " from  " + filterDataTable + " where "+dimension+"    group by 1,2 "+orderString;
                    System.out.println(query);
                    return query;
                } else {
                    List<String> groupByFieldNames = Arrays.asList(parents.split("\\s*,\\s*"));
                    String parentString = "";
                    for (String strTemp : groupByFieldNames) {
                        parentString += strTemp + ",";
                    }

                    query = "SELECT " + parentString + filterName + "_id as value," + filterName + " as " + originalFilterName + " from   " + filterDataTable + " WHERE "+dimension+" group by " + parents + "," + filterName + "  "+orderString;
                    System.out.println(query);
                    return query;
                }
            } else {

                String parentString = "";
                if (parents == null || parents.equals("")) {
                    query = "SELECT Distinct " + filterName + "," + originalFilterName + "  from  " + filterDataTable + "  where " + filterName + " is not null   "+orderString;
                     System.out.println(query);
                     return query;
                } else {
                    List<String> groupByFieldNames = Arrays.asList(parents.split("\\s*,\\s*"));
                    for (String strTemp : groupByFieldNames) {
                        parentString += strTemp + ",";
                    }
                    query = "SELECT " + parentString + filterName + " as "+originalFilterName+"," + originalFilterName + " as value from  " + filterDataTable + "  where " + filterName + " is not null  "+orderString;
                    System.out.println(query);
                    return query;
                }
            }
    }else
    {

        String parentString = "";
        if (parents == null || parents.equals("")) {
             query="select b."+filterName+",b."+originalFilterName+" from (select * from "+roleMapTable+") as a\n" +
                         "left join\n" +
                         "(select * from "+filterDataTable+") as b\n" +
                         "on a.filter_names=b."+filterName+" where "+filterName+" is not null and role_id="+roleId+" "+orderString;
            System.out.println(query);
            return query;
        } else {
            List<String> groupByFieldNames = Arrays.asList(parents.split("\\s*,\\s*"));
            for (String strTemp : groupByFieldNames) {
                parentString += "b."+strTemp + ",";
            }
            query="select "+parentString+"b."+filterName+" as "+originalFilterName+",b."+originalFilterName+" as value from (select * from "+roleMapTable+") as a\n" +
                    "left join\n" +
                    "(select * from "+filterDataTable+") as b\n" +
                    "on a.filter_names=b."+filterName+" where "+filterName+" is not null and role_id="+roleId+" "+orderString;
            System.out.println(query);
            return query ;
        }
    }
    }
}

class FilterObjectCreate {

    String Filtername;
    NameValuePair nameValuePair;

    public String getFiltername() {
        return Filtername;
    }

    public void setFiltername(String Filtername) {
        this.Filtername = Filtername;
    }

    public NameValuePair getNameValuePair() {
        return nameValuePair;
    }

    public void setNameValuePair(NameValuePair nameValuePair) {
        this.nameValuePair = nameValuePair;
    }

    FilterObjectCreate(String Filtername, NameValuePair nameValuePair) {
        this.Filtername = Filtername;
        this.nameValuePair = nameValuePair;
    }
}

class PackerFilters {

    String filterName;
    LinkedHashMap filtesData;

    PackerFilters(String filterName, LinkedHashMap genericFiltersData) {
        this.filterName = filterName;
        this.filtesData = genericFiltersData;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public LinkedHashMap getFiltesData() {
        return filtesData;
    }

    public void setFiltesData(LinkedHashMap filtesData) {
        this.filtesData = filtesData;
    }
}
