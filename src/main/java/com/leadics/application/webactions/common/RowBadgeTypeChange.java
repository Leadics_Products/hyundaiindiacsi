package com.leadics.application.webactions.common;

class RowBadgeTypeChange {

    String title;
    String id;
    String score1;
    String score2;
    String score;
    String badgeScore;
    Boolean badgeVisible;
    Boolean scoreOneVisible;
    Boolean scoreTwoVisible;
    String badgeScoreColor;
    String color;
    String badgeLable;

    public String getBadgeScoreColor() {
        return badgeScoreColor;
    }

    public void setBadgeScoreColor(String badgeScoreColor) {
        this.badgeScoreColor = badgeScoreColor;
    }

    public RowBadgeTypeChange(String title, String id, String score1, String score2, String score, String badgeScore, Boolean badgeVisible, Boolean scoreOneVisible, Boolean scoreTwoVisible, String badgeScoreColor,String color,String badgeLable) {
        this.title = title;
        this.id = id;
        this.score1 = score1;
        this.score2 = score2;
        this.score = score;
        this.badgeScore = badgeScore;
        this.badgeVisible = badgeVisible;
        this.scoreOneVisible = scoreOneVisible;
        this.scoreTwoVisible = scoreTwoVisible;
        this.badgeScoreColor = badgeScoreColor;
        this.color=color;
        this.badgeLable=badgeLable;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1 = score1;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getBadgeScore() {
        return badgeScore;
    }

    public void setBadgeScore(String badgeScore) {
        this.badgeScore = badgeScore;
    }

    public Boolean getBadgeVisible() {
        return badgeVisible;
    }

    public void setBadgeVisible(Boolean badgeVisible) {
        this.badgeVisible = badgeVisible;
    }



    public Boolean getScoreBestVisible() {
        return scoreOneVisible;
    }

    public void setScoreBestVisible(Boolean scoreOneVisible) {
        this.scoreOneVisible = scoreOneVisible;
    }

    public Boolean getscoreTwoVisible() {
        return scoreTwoVisible;
    }

    public void setscoreTwoVisible(Boolean scoreTwoVisible) {
        this.scoreTwoVisible = scoreTwoVisible;
    }

}
