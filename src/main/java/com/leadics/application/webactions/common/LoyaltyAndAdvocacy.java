package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class LoyaltyAndAdvocacy {
    public static final String GREY_COLOR = "#4CB341";
    public static final String BLACK_COLOR = "#000000";
    public static final String BLUE_COLOR = "#087894";
    public static final String AMBER_COLOR = "#FEAD5C";
    public static final String RED_COLOR = "#E10000";
    public static final String GREEN_COLOR = "#4CB341";
    public static final String BLUE_COLOR_BENCHMARK = "#087894";
    public static final String RED_COLOR_BENCHMARK = "#E10000";
    public static final String GREEN_COLOR_BENCHMARK = "#4CB341";
    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }
            
    private HashMap getDataPackFactorScore(String score, String wave){
        HashMap temp=new HashMap();
        temp.put("score",score);
        temp.put("wave",wave);
        return temp;
    }
    
    public void getLoyaltyScore(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = service.getQureyRebuild(request);
        query = service.getQureyRebuild("dealers-region-marker",query,request);
        String[] columns = {"score", "categories"};
        System.out.println("query checks *********** " + query);
        HashMap<String, String> columnInfo = new HashMap<>();
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        columnList.stream().forEach(x -> columnInfo.put(x, x));
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();
        List<String> categories = getCategories(rows);
        List FactorScore = packingutils.getScoresAsIntWithFilterValue(rows, "#6F7CEF", "score","filterValue");
        if (FactorScore.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        Map innerOutput = new HashMap();
        innerOutput.put("categories", categories);
        innerOutput.put("factor", FactorScore);
        Map output = new HashMap();
        output.put("data", innerOutput);
        Map outeOoutput = new HashMap();
        outeOoutput.put("data", output);
        ArrayList seriesName = new ArrayList();
        seriesName.add("factor");
        ArrayList seriesValue = new ArrayList();
        seriesValue.add("Factor Scores");        
        output.put("seriesName", seriesName);
        output.put("seriesValue", seriesValue);
        outeOoutput.put("data", output);
        service.writeOutput(response, outeOoutput);

    }
    
    private List<String> getCategories(List<HashMap<String, String>> rows) {
        List<String> Categories = rows.stream()
                .filter(map -> map.containsKey("categories"))
                .map(map -> map.get("categories"))
                .collect(Collectors.toList());
        System.out.println("Categories ::::::::: "+Categories);
        return Categories;
    }    
}
