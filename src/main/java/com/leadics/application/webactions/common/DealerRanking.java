package com.leadics.application.webactions.common;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.application.to.LIWhereconditionmapRecord;
import org.json.simple.JSONObject;

public class DealerRanking {

	public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
			throws ServletException, IOException {
		response.setContentType("text/html");

		try (PrintWriter out = response.getWriter()) {
			try {
				Method method = this.getClass().getMethod(actionType, HttpServletRequest.class,
						HttpServletResponse.class, String.class);
				method.invoke(this, request, response, actionType);
			} catch (Exception e) {
				e.printStackTrace();
				out.println("-1");
				return;
			}
		}
	}
	
	public void getDealerScore(HttpServletRequest request, HttpServletResponse response, String methodName)
			throws Exception {
		LIDAO dao = new LIDAO();
		LIService service = new LIService();

		// fetching data starts
		String query = service.getQureyRebuild(request);
		dao.executeUpdateQuery("SET @r1=0,  @r2=0,  @r3=0,  @r4=0,  @r5=0,  @r6=0,  @r7=0, @r8=1000, @r9=10000,@rsum=0, @rorder=0;");
		Map<String, LIWhereconditionmapRecord> filtersMapdelaer = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMapdelaer, query);
        query=acknowledgmentMap.get("QUERY");
        System.out.println(query);
        
		//get headers - column names
		LinkedHashMap<String, String> columnList = dao.getColumnsFromResultSet(query);
		
		List<HashMap<String, String>> rowsData = new ArrayList<HashMap<String, String>>();
		rowsData = dao.loadValuesRowWise(query, columnList);
		
		List<HashMap<String, Object>> headers = new ArrayList<HashMap<String, Object>>();

		for (Map.Entry element : columnList.entrySet()) {
			LinkedHashMap<String, Object> header = new LinkedHashMap<String, Object>();

			String key = (String) element.getKey();
			String value = (String) element.getValue();
			
			Integer minWidth=(value=="Delivery Date")?10:100;
				
			header.put("field", value);
			header.put("maxWidth", null);
			header.put("minWidth", minWidth);
			header.put("sortable", true);
			header.put("headerName", value);
			header.put("tooltipField", value);
			headers.add(header);
		}
		
		
		List<HashMap<String, String>> tableData = new ArrayList<HashMap<String, String>>();
		rowsData.forEach(x -> {
			HashMap<String, String> values = new HashMap<String, String>();
			for (String key : x.keySet()) {
				String value = x.get(key);
				values.put(key, value);
			}
			tableData.add(values);
		});

		HashMap data = new HashMap();
		data.put("header", headers);
		if(rowsData.size()<=0){
			System.out.println(rowsData.size());
			List temp=new ArrayList();
			data.put("tableData", temp);
			service.writeOutput(response, data);
		}else{
			data.put("tableData", tableData);
			String stringData= URLEncoder.encode(new JSONObject(data).toString(), "UTF-8");
			service.writeOutput(response, stringData);
		}


	}
}
