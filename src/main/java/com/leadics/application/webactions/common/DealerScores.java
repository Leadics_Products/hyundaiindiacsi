
package com.leadics.application.webactions.common;

        import com.leadics.application.common.LIDAO;
        import com.leadics.application.common.LIService;
        import com.leadics.application.to.LIWhereconditionmapRecord;

        import javax.servlet.ServletException;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
        import java.io.IOException;
        import java.io.PrintWriter;
        import java.lang.reflect.Method;
        import java.util.*;
        import java.util.stream.Collectors;
        import java.util.stream.Stream;

        import static java.util.stream.Collectors.groupingBy;

public class DealerScores {
    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }
    public void dealerScore(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        String query=service.getQureyRebuild(request);
        System.out.println(query);
        ArrayList<String> columnListDealer = new ArrayList<String>(Arrays.asList("rank","factor_name","dealer_name","region_name","score","is_study_score","best","worst"));

        HashMap<String, String> columnInfoDealer = new HashMap<>();
        columnListDealer.stream()
                .forEach(x -> columnInfoDealer.put(x, x));
        Map<String, LIWhereconditionmapRecord> filtersMapdelaer = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMapdelaer, query);
        query=acknowledgmentMap.get("QUERY");
        System.out.print(query);
        List<HashMap<String, String>> rowsDealer = dao.loadValuesRowWise(query, columnInfoDealer);
        if(rowsDealer.size()<=0){
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        LinkedHashMap<String,List<HashMap<String,String>>> dealerMapedRows=rowsDealer.stream().collect(groupingBy(rowDealer -> rowDealer.get("dealer_name"),
               LinkedHashMap::new,
                        Collectors.mapping(row-> row,
                                Collectors.toList())));

        List<HashMap> dealersDetailsList=dealerMapedRows.entrySet().stream().map(x->getDealerDetails(x.getValue())).collect(Collectors.toList());
        service.writeOutput(response,dealersDetailsList);

    }
    public LinkedHashMap getDealerDetails(List<HashMap<String,String>> dealerDetailsList){
        LinkedHashMap  dealerDetailsMap=new LinkedHashMap();
        try {

            dealerDetailsMap.put("rank", dealerDetailsList.get(0).get("rank").equalsIgnoreCase("99999") || dealerDetailsList.get(0).get("rank").equalsIgnoreCase("9999")?"":Integer.parseInt(dealerDetailsList.get(0).get("rank")));
            dealerDetailsMap.put("dealerName", new DataValueSmaple(dealerDetailsList.get(0).get("dealer_name"),getConvetedToBoolean(dealerDetailsList.get(0).get("insufficientSample")),getConvetedToBoolean(dealerDetailsList.get(0).get("smallSample"))));
            dealerDetailsMap.put("region", dealerDetailsList.get(0).get("region_name"));
            dealerDetailsList.stream().forEach(row -> {

                dealerDetailsMap.put(row.get("factor_name"), new DataClassValueBestWorst(row.get("score"),getConvetedToBoolean(row.get("bestBadge")),getConvetedToBoolean(row.get("worstBadge"))));
            });
            dealerDetailsMap.put("% Disappointed", new DataClassOfDElighted(dealerDetailsList.get(0).get("min"),dealerDetailsList.get(0).get("max"),dealerDetailsList.get(0).get("actual"),dealerDetailsList.get(0).get("percentage"),dealerDetailsList.get(0).get("color"),dealerDetailsList.get(0).get("suffix")));
        }
        catch(Exception e){
            System.out.println("err"+e);
        }
        return dealerDetailsMap;
    }

    private Boolean getConvetedToBoolean(String insufficinet) {
        if(insufficinet!=null && insufficinet.equalsIgnoreCase("True") || insufficinet.equalsIgnoreCase("1") ) {
            return true;
        }else {
            return false;
        }
    }


    public void getDealerScores(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        String query=service.getQureyRebuild(request);
        System.out.println("dealer Scores +++++++ "+query);
        ArrayList<String> columnListDealer = new ArrayList<String>(Arrays.asList("rank","factor_name","dealer_name","region_name","score","insufficientSample","smallSample","min","actual","percentage","max","color","suffix","bestBadge","worstBadge"));
        HashMap<String, String> columnInfoDealer = new HashMap<>();
        columnListDealer.stream()
                .forEach(x -> columnInfoDealer.put(x, x));
        List<HashMap<String, String>> rowsDealer = dao.loadValuesRowWise(query, columnInfoDealer);
        LinkedHashMap<String, List<HashMap<String, String>>> dealerList = rowsDealer.stream().collect(groupingBy(rowDealer -> rowDealer.get("dealer_name"),
                LinkedHashMap::new,
                Collectors.mapping(row -> row,
                        Collectors.toList())));
        List<HashMap> dealersDetailsList=dealerList.entrySet().stream().map(x->getDealerDetails(x.getValue())).collect(Collectors.toList());
//        Collections.sort(dealersDetailsList, new MapComparator("rank"));
        List Innerdata=getTopScore(request);
        HashMap data=new HashMap();
        data.put("tableData",dealersDetailsList);
        data.put("chartData",Innerdata);
        service.writeOutput(response,data);
    }

    private List<HashMap<String, String>> getTopScore(HttpServletRequest request) throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQuery("mg-motors-ssi-dealer-scores-Dealer-Score-Q1");
        query=service.getQureyRebuild("mg-motors-ssi-dealer-scores-Dealer-Score-Q1",query,request);
        String[] columns = {"score", "title"};
        System.out.println("QUERY :::::::::::: "+query);
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnList.forEach(x->{
            columnInfo.put(x,x);
        });
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        List outPut = rows.stream().map(map -> getTopScoreObject(map.get("score"), map.get("title"))).collect(Collectors.toList());

        return outPut;
    }

    private HashMap getTopScoreObject(String score, String title) {
        HashMap temp=new HashMap();
        Double temps=Double.parseDouble(score);
        temp.put("score",(int)Math.round(temps));
        temp.put("title",title);
        return temp;
    }


}

class DataValueSmaple{
    String value;
    Boolean insufficientSample;
    Boolean smallSample;
     DataValueSmaple(String value, Boolean insufficientSample, Boolean smallSample){
         this.value=value;
         this.insufficientSample=insufficientSample;
         this.smallSample=smallSample;
     }
}
class MapComparator implements Comparator<Map<String, String>>
{
    private final String key;

    public MapComparator(String key)
    {
        this.key = key;
    }

    public int compare(Map<String, String> first,
                       Map<String, String> second)
    {
        // TODO: Null checking, both for maps and values
        String firstValue = first.get(key);
        String secondValue = second.get(key);
        return firstValue.compareTo(secondValue);
    }
}



class DataClassValueBestWorst{
	String value;
    Boolean bestBadge;
    Boolean worstBadge;


    DataClassValueBestWorst(String value, Boolean best,Boolean worst){
    	 this.worstBadge=worst;
         this.bestBadge=best;
//         Double temp=Double.parseDouble(value);
//         this.value=(int)Math.round(temp);
         this.value=value;

    }

}

class DataClassOfDElighted{
    Integer min;
    Integer max;
    Integer actual;
    Integer percentage;
    String color;
    String suffix;

    public DataClassOfDElighted(String min, String max, String actual, String percenatge, String color, String suffix) {
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
        this.actual = Integer.parseInt(actual);
        this.percentage = Integer.parseInt(percenatge);
        this.color = color;
        this.suffix = suffix;
    }
}
