package com.leadics.application.webactions.common;

import com.leadics.application.to.SeriesDoubleProperties;
import com.leadics.application.to.SeriesDoubleWithFilterProperties;
import com.leadics.application.to.SeriesIntProperties;
import com.leadics.application.to.SeriesIntWithFilterProperties;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.leadics.application.webactions.common.Summary.*;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;

public class packingutils {
    public List getScoresAsInt(List<HashMap<String, String>> rows, String color, String colName) {
        List FactorScore = rows.stream()
                .map(map -> getIntColoredSeriesData(map, colName, color))
                .collect(Collectors.toList());

        return FactorScore;
    }
    public List getScoresAsDouble(List<HashMap<String, String>> rows, String color, String colName) {
        List FactorScore = rows.stream()
                .map(map -> getDoubleColoredSeriesData(map, colName, color))
                .collect(Collectors.toList());

        return FactorScore;
    }
    public List getScoresAsIntWithFilterValue(List<HashMap<String, String>> rows, String color, String colName,String filterValue) {
        List FactorScore = rows.stream()
                .map(map -> getIntColoredSeriesDataWithFilterValue(map, colName, color,filterValue))
                .collect(Collectors.toList());

        return FactorScore;
    }
    public List getScoresAsDoubleWithFilterValue(List<HashMap<String, String>> rows, String color, String colName,String filterValue) {
        List FactorScore = rows.stream()
                .map(map -> getDoubleColoredSeriesDataWithFilterValue(map, colName, color,filterValue))
                .collect(Collectors.toList());

        return FactorScore;
    }
    public static Integer getConvertToInt(String score) {
        if(score==null){
            return null;
        }
        try{
            return Integer.parseInt(score);
        }catch (NumberFormatException e){
            double data = Double.parseDouble(score);
            int value = (int)Math.round(data);
            return  value;
        }

    }
    public Double getConvertToDouble(String score) {
        if(score==null){
            return null;
        }
        try{
            return Double.parseDouble(score);
        }catch (NumberFormatException e){
            double data = Double.parseDouble(score);
            return  data;
        }

    }
    public Set<String> getCategories(List<HashMap<String, String>> rows) {
        List<String> Categories = rows.stream()
                .filter(map -> map.containsKey("categories"))
                .map(map -> map.get("categories"))
                .collect(Collectors.toList());
        System.out.println("Categories ::::::::: "+Categories);
        LinkedHashSet<String> hashSet = new LinkedHashSet(Categories);
        ArrayList<String> listOfCategories = new ArrayList<String>(hashSet);
        Set setCategories = new LinkedHashSet<>(listOfCategories);
        return setCategories;
    }


    public DataClass getDataObjet(String value, String name, String color) {

        return new DataClass(value,name,color);
    }

    public String getColor(String diffrence) {
        if (Double.parseDouble(diffrence) > 2) {
            return GREEN_COLOR;
        } else if (Double.parseDouble(diffrence) < 2 && Double.parseDouble(diffrence) > 0) {
            return BLUE_COLOR;
        } else if (Double.parseDouble(diffrence) > -2 && Double.parseDouble(diffrence) < 0) {
            return AMBER_COLOR;
        } else if (Double.parseDouble(diffrence) < -2) {
            return RED_COLOR;
        }
        return GREY_COLOR;

    }

    public SeriesIntProperties getIntColoredSeriesData(HashMap<String, String> map, String colName, String color) {

        if (map.isEmpty()) {
            return null;
        } else {

            System.out.print(map);
            if (map.get(colName) != null) {
                return new SeriesIntProperties(Integer.parseInt(map.get(colName)), color);
            } else {
                return null;
            }

        }
    }
    
    public SeriesDoubleProperties getDoubleColoredSeriesData(HashMap<String, String> map, String colName, String color) {

        if (map.isEmpty()) {
            return null;
        } else {

            System.out.print(map);
            if (map.get(colName) != null) {
                return new SeriesDoubleProperties(Double.parseDouble(map.get(colName)), color);
            } else {
                return null;
            }

        }
    }

    public SeriesIntWithFilterProperties getIntColoredSeriesDataWithFilterValue(HashMap<String, String> map, String colName, String color,String filterValue) {

        if (map.isEmpty()) {
            return null;
        } else {

            System.out.print(map);
            if (map.get(colName) != null) {
            	
                return new SeriesIntWithFilterProperties(Integer.parseInt(map.get(colName)), color,map.get(filterValue));
            } else {
                return null;
            }

        }
    }
    public SeriesDoubleWithFilterProperties getDoubleColoredSeriesDataWithFilterValue(HashMap<String, String> map, String colName, String color,String filterValue) {

        if (map.isEmpty()) {
            return null;
        } else {

            System.out.print(map);
            if (map.get(colName) != null) {
            	
                return new SeriesDoubleWithFilterProperties(Double.parseDouble(map.get(colName)), color,map.get(filterValue));
            } else {
                return null;
            }

        }
    }
    public List getColoredChagedScore(List<HashMap<String, String>> rows, String colName, String compareCol, HttpServletRequest request) {
        List<SeriesPropertiesInteger> MinScore = rows.stream()
                .filter(map -> map.containsKey("categories"))
                .map(map -> getColoredSeriesDatagetColor(map, colName, compareCol, request))
                .collect(Collectors.toList());
        List<SeriesPropertiesInteger> MinScoreFilter = MinScore.stream().filter(x -> x.getColor() == "#E10000").collect(toList());
        return MinScore;
    }
    private SeriesPropertiesInteger getColoredSeriesDatagetColor(HashMap<String, String> map, String colName, String compareCol, HttpServletRequest request){
        if (map.isEmpty()) {
            return null;
        } else {
            if (map.get(colName) != null) {
                return new SeriesPropertiesInteger(Integer.parseInt(map.get(colName)), getColorBasedOnStudy(map.get(colName), map.get(compareCol)));
            } else {
                return new SeriesPropertiesInteger(null, getColorBasedOnStudy(map.get(colName), map.get(compareCol)));
            }

        }
    }

    private String getColorBasedOnStudy(String colOne, String colTwo) {
        System.out.println("Score one"+colOne);
        System.out.println("Score one"+colTwo);
        if (Integer.parseInt(colTwo) <Integer.parseInt(colOne)) {
            return "RED";
        } else {
            return "#FB9E9F";
        }
    }
}

class SeriesPropertiesInteger {


    Integer y;
    String color;

    public SeriesPropertiesInteger(Integer y, String color) {
        this.y = y;
        this.color = color;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
