package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class mainClass {
    public static final String BLACK_COLOR = "#000000";
    public static final String BLUE_COLOR = "#087894";
    public static final String AMBER_COLOR = "#FEAD5C";
    public static final String RED_COLOR = "#E10000";
    public static final String GREEN_COLOR = "#4CB341";

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getMultiScoreCardData(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query=service.getQureyRebuild(request);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("display_name", "text");
        columnInfo.put("kpi_id", "id");
        columnInfo.put("maxscore", "score1");
        columnInfo.put("minscore", "score2");
        columnInfo.put("badgeScore", "badgeScore");
        columnInfo.put("badgeVisible", "badgeVisible");
        columnInfo.put("score", "score");
        columnInfo.put("target", "target");
        columnInfo.put("diffrence", "diffrence");
        columnInfo.put("ribbonVisible", "ribbonVisible");
        columnInfo.put("annotationVisible", "annotationVisible");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
       List<MultiScoreCardTrasform> rowbadge = rows.stream().collect(
                Collectors.mapping(rowScore -> new MultiScoreCardTrasform(rowScore.get("text"),                              rowScore.get("id"), rowScore.get("score1"),
                                rowScore.get("score2"), rowScore.get("score"),
                                rowScore.get("badgeScore"),changeTypeToBoolean(rowScore.get("badgeVisible")),
                                changeTypeToBoolean(rowScore.get("ribbonVisible")),
                                changeTypeToBoolean(rowScore.get("annotationVisible")),
                                changeTypeWithFilter("nones"),
                                changeTypeWithFilter("nones"),
                                getColor(rowScore.get("diffrence")),
                                rowScore.get("target")
                        ),
                        Collectors.toList()
                ));
        Map output=new HashMap();
        output.put("data",rowbadge);
        service.writeOutput(response, rowbadge);
    }
    private Boolean changeTypeToBoolean(String get) {
        if (get.equalsIgnoreCase("False")) {
            return false;
        } else {
            return true;
        }
    }

    private Boolean changeTypeWithFilter(String get) {
        if (get.equalsIgnoreCase("none")) {
            return false;
        } else {
            return true;
        }
    }
    public String getColor(String diffrence) {

        if (Double.parseDouble(diffrence) == 0.00) {
            return BLACK_COLOR;
        } else if (Double.parseDouble(diffrence) >= 5) {
            return GREEN_COLOR;
        } else if (Double.parseDouble(diffrence) < 5 && Double.parseDouble(diffrence) >= 0.01) {
            return BLUE_COLOR;
        } else if (Double.parseDouble(diffrence) >-5 && Double.parseDouble(diffrence) <= -0.01) {
            return AMBER_COLOR;
        } else if (Double.parseDouble(diffrence) < -5) {
            return RED_COLOR;
        }
        return BLACK_COLOR;
    }
    public void getChartDataDemCategoryScore(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        String testData="{\"data\": {\"data\": {\"categoryScore\": [{\"y\": 66, \"color\": \"#50B4E4\", \"filterValue\": \"4\"}, {\"y\": 99, \"color\": \"#50B4E4\", \"filterValue\": \"5\"}, {\"y\": 55, \"color\": \"#50B4E4\", \"filterValue\": \"3\"}, {\"y\": 77, \"color\": \"#50B4E4\", \"filterValue\": \"2\"}, {\"y\": 99, \"color\": \"#50B4E4\", \"filterValue\": \"1\"}, {\"y\": 100, \"color\": \"#50B4E4\", \"filterValue\": \"6\"} ], \"categories\": [\"Sales\", \"Service\", \"Parts\", \"InfraStructure\", \"General\", \"Add. InfraStructure \"] }, \"seriesName\": [\"categoryScore\"], \"seriesValue\": [\"Category Score (%KPI Achieved)\"] } }";
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(testData);
        service.writeOutput(response, json);
    }
    public  void getChartDataDemScoreTrend(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        String testData="{\"data\": {\"data\": {\"categoryScore\": [{\"y\": 1000, \"color\": \"#50B4E4\", \"filterValue\": \"1\"}, {\"y\": 929, \"color\": \"#50B4E4\", \"filterValue\": \"2\"}, {\"y\": 525, \"color\": \"#50B4E4\", \"filterValue\": \"3\"}, {\"y\": 787, \"color\": \"#50B4E4\", \"filterValue\": \"4\"}, {\"y\": 969, \"color\": \"#50B4E4\", \"filterValue\": \"5\"} , {\"y\": 1060, \"color\": \"#50B4E4\", \"filterValue\": \"6\"} , {\"y\": 1500, \"color\": \"#50B4E4\", \"filterValue\": \"6\"} , {\"y\": 150, \"color\": \"#50B4E4\", \"filterValue\": \"6\"} ], \"categories\": [\"Q1'18\", \"Q2'18\", \"Q3'18\", \"Q4'18\", \"Q1'19\", \"Q2'19\", \"Q3'19\", \"Q4'19\"] }, \"seriesName\": [\"categoryScore\"], \"seriesValue\": [\"DEM Score Trend\"] } }";
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(testData);
        service.writeOutput(response, json);
    }
    public  void getChartDataDealersummaryGrid(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query=service.getQureyRebuild(request);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("score", "score");
        columnInfo.put("category", "category");
        columnInfo.put("quarter", "quarter");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        Map parent = rows.stream().collect(groupingBy(row -> row.get("category"),
                groupingBy(rowKpi -> rowKpi.get("quarter"), LinkedHashMap::new,
                        Collectors.mapping(rowScore -> new HighChartsData(rowScore.get("score"),"#7cb5ec"),
                                Collectors.toList()))));
        Set parentKeySet = parent.keySet();
        Map outerList=new HashMap();
        parentKeySet.stream().forEach(x->{
            Map innerOutput=new HashMap();
            LinkedHashMap parentHashMap = (LinkedHashMap) (HashMap) parent.get(x);
            List innerList=new LinkedList();
            Set<String> Categories = parentHashMap.keySet();
            Iterator<Map.Entry> it = parentHashMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String,List> entry = it.next();
                System.out.println(entry.getKey() + " => " + entry.getValue());
                innerList.add(entry.getValue().get(0));
            }

            innerOutput.put(x,innerList);
            innerOutput.put("categories",Categories);
            outerList.put(x,innerOutput);
        });

        Map output=new HashMap();
        output.put("data",outerList);
        service.writeOutput(response, output);
    }
    public List<String> getCategories(List<HashMap<String, String>> rows) {
        List<String> Categories = rows.stream()
                .filter(map -> map.containsKey("categories"))
                .map(map -> map.get("categories"))
                .collect(Collectors.toList());

        return Categories;
    }

    public void getChartDataDealersummaryGridTest(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        String testData="{\"data\": {\"data\": {\"categoryScore\": [{\"y\": 1000, \"color\": \"#50B4E4\", \"filterValue\": \"1\"}, {\"y\": 929, \"color\": \"#50B4E4\", \"filterValue\": \"2\"}, {\"y\": 525, \"color\": \"#50B4E4\", \"filterValue\": \"3\"}, {\"y\": 787, \"color\": \"#50B4E4\", \"filterValue\": \"4\"}, {\"y\": 969, \"color\": \"#50B4E4\", \"filterValue\": \"5\"} , {\"y\": 1060, \"color\": \"#50B4E4\", \"filterValue\": \"6\"} , {\"y\": 1500, \"color\": \"#50B4E4\", \"filterValue\": \"6\"} , {\"y\": 150, \"color\": \"#50B4E4\", \"filterValue\": \"6\"} ], \"categories\": [\"Q1'18\", \"Q2'18\", \"Q3'18\", \"Q4'18\", \"Q1'19\", \"Q2'19\", \"Q3'19\", \"Q4'19\"] }, \"seriesName\": [\"categoryScore\"], \"seriesValue\": [\"DEM Score Trend\"] } }";
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(testData);
        service.writeOutput(response, json);
    }
    public void testmehtod(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String query=service.getQureyRebuild(request);
        service.writeOutput(response, query);
    }
}
