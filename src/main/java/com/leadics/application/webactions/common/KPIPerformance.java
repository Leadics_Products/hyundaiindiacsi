package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.application.to.LIWhereconditionmapRecord;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class KPIPerformance {
	public static final String GREY_COLOR = "#4CB341";
	public static final String BLACK_COLOR = "#000000";
	public static final String BLUE_COLOR = "#087894";
	public static final String AMBER_COLOR = "#FEAD5C";
	public static final String RED_COLOR = "#E10000";
	public static final String GREEN_COLOR = "#4CB341";
	public static final String BLUE_COLOR_BENCHMARK = "#087894";
	public static final String RED_COLOR_BENCHMARK = "#E10000";
	public static final String GREEN_COLOR_BENCHMARK = "#4CB341";

	public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
			throws ServletException, IOException {
		response.setContentType("text/html");

		try (PrintWriter out = response.getWriter()) {
			try {
				Method method = this.getClass().getMethod(actionType, HttpServletRequest.class,
						HttpServletResponse.class, String.class);
				method.invoke(this, request, response, actionType);
			} catch (Exception e) {
				e.printStackTrace();
				out.println("-1");
				return;
			}
		}
	}

	private List<String> getCategories(List<HashMap<String, String>> rows, HttpServletRequest request)
			throws Exception {
		LIService service = new LIService();
//		String query = service.getQuery("mg-motors-ssi-sample-found");
//		query = service.getQureyRebuild("mg-motors-ssi-sample-found", query, request);
//		LIDAO dao = new LIDAO();
//		System.out.println("QUERY :::::::::::::::: " + query);
//		String countValue = dao.loadString(query, "total_count");
//		int count = countValue == null ? null : Integer.parseInt(countValue);
		List<String> Categories = rows.stream().filter(map -> map.containsKey("categories")).map(map -> {
			String cat = map.get("categories");
			return cat;
		}).collect(Collectors.toList());
		System.out.println("Categories ::::::::: " + Categories);
		return Categories;
	}

	public void getKPIPerformance(HttpServletRequest request, HttpServletResponse response, String methodName)
			throws Exception {
		LIDAO dao = new LIDAO();
		LIService service = new LIService();
		String query = service.getQureyRebuild(request);
		String belowStudyAverage = request.getParameter("belowStudyAverage");
		String[] columns = { "categories", "score", "study_average", "best_score", "impact" };
		ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
		String[] seriesName = { "score" };
		ArrayList<String> seriesNames = new ArrayList<String>(Arrays.asList(seriesName));
		String upperBenchmark = request.getParameter("upper_benchmark");
		LinkedHashMap columnInfo = new LinkedHashMap();
		columnList.stream().forEach(x -> columnInfo.put(x, x));
		Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
		Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
		System.out.println(query);
		query = acknowledgmentMap.get("QUERY");

		System.out.print("-----------------test------------------");
		System.out.print(query);
		List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
		Set dataCheck = rows.stream().filter(x -> x.get("score") != null).map(s -> s.get("score"))
				.collect(Collectors.toSet());
		if (dataCheck.size() <= 0) {
			response.setStatus(204);
			service.writeOutput(response, " ");
		}
		packingutils packingUitility = new packingutils();
		List<String> categories = getCategories(rows, request);
		List FactorScore = new LinkedList();
		if (belowStudyAverage != null && belowStudyAverage.equalsIgnoreCase("true")) {
			FactorScore = packingUitility.getColoredChagedScore(rows, "score", "study_average", request);
			if (FactorScore.size() <= 0) {
				response.setStatus(204);
				service.writeOutput(response, " ");
			}
		} else {
			FactorScore = rows.stream().collect(Collectors.mapping(map -> {
				return new DataClassWithImpact(packingutils.getConvertToInt(map.get("score")), map.get("impactScore"));
			}, Collectors.toList()));
		}
		List Maxscore = packingUitility.getScoresAsInt(rows, GREEN_COLOR_BENCHMARK, "best_score");
		List MinScore = packingUitility.getScoresAsInt(rows, RED_COLOR_BENCHMARK, "worst_score");

		Map cols = new HashMap();		
		if ((upperBenchmark != null) && (upperBenchmark.contains("Best"))) {
			seriesNames.add(upperBenchmark);
			cols.put(upperBenchmark, Maxscore);
		}

		cols.put("score", FactorScore);
		cols.put("categories", categories);
		HashMap finalOutput = new HashMap();
		finalOutput.put("percentPrefix", "%");
		finalOutput.put("data", cols);
		finalOutput.put("seriesName", seriesNames);
		finalOutput.put("seriesValue", seriesNames);

		HashMap output = new HashMap();
		output.put("data", finalOutput);
		service.writeOutput(response, output);
	}
	
	public void getKPITrend(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
		LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = service.getQureyRebuild(request);
        query = service.getQureyRebuild("dealers-region-marker",query,request);
        String upperBenchmark = request.getParameter("upper_benchmark");
        String lowerBenchmark = request.getParameter("lower_benchmark");
        String[] columns = {"score", "maxscore", "minscore", "categories"};
        System.out.println("query checks *********** " + query);
        HashMap<String, String> columnInfo = new HashMap<>();
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        columnList.stream().forEach(x -> columnInfo.put(x, x));
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();
        List<String> categories = getCategories(rows);
        List FactorScore = packingutils.getScoresAsIntWithFilterValue(rows, "#6F7CEF", "score","filterValue");
        if (FactorScore.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        List Maxscore = packingutils.getScoresAsInt(rows, GREEN_COLOR_BENCHMARK, "maxscore");
        String colorAverage;
        if (lowerBenchmark.contains("Average")) {
            colorAverage = BLUE_COLOR_BENCHMARK;
        } else {
            colorAverage = RED_COLOR_BENCHMARK;
        }
        List MinScore = packingutils.getScoresAsInt(rows, colorAverage, "minscore");
        Map innerOutput = new HashMap();
        innerOutput.put("categories", categories);
        innerOutput.put("factor", FactorScore);
        innerOutput.put("maxscore", Maxscore);
        innerOutput.put("minscore", MinScore);
        Map output = new HashMap();
        output.put("data", innerOutput);
        Map outeOoutput = new HashMap();
        outeOoutput.put("data", output);
        ArrayList seriesName = new ArrayList();
        seriesName.add("factor");
        ArrayList seriesValue = new ArrayList();
        seriesValue.add("Factor Scores");
        if ((upperBenchmark != null) && !upperBenchmark.contains("None")) {
            seriesValue.add(upperBenchmark);
        }

        if ((lowerBenchmark != null) && !lowerBenchmark.contains("None")) {
            seriesValue.add(lowerBenchmark);
        }
        Boolean checkUpperStudyBench = (upperBenchmark != null) && upperBenchmark.contains("Best") ? seriesName.add("maxscore") : false;
        Boolean checkLowerStudyBench = (lowerBenchmark != null) && (lowerBenchmark.contains("Worst") || lowerBenchmark.contains("Average")) ? seriesName.add("minscore") : false;
        output.put("seriesName", seriesName);
        output.put("seriesValue", seriesValue);
        output.put("percentPrefix", "%");
        outeOoutput.put("data", output);
        service.writeOutput(response, outeOoutput);
    }

	private Boolean getConvertedToBoolean(String isgrowthIncremented) {
		if (isgrowthIncremented.equalsIgnoreCase("1")) {
			return true;
		} else {
			return false;
		}
	}

	private List<String> getCategories(List<HashMap<String, String>> rows) {
		List<String> Categories = rows.stream().filter(map -> map.containsKey("categories"))
				.map(map -> map.get("categories")).collect(Collectors.toList());
		System.out.println("Categories ::::::::: " + Categories);
		return Categories;
	}

}

class DataClassWithImpact {
	private Integer y;
	private String impactScore;

	public DataClassWithImpact(Integer score, String impactScore) {
		this.y = score;
		this.impactScore = impactScore;
	}
}
