package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.application.to.LIWhereconditionmapRecord;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class DetractionDriver {

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }
    public void topReasonsforDetraction(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();


        service.writeOutput(response, new ArrayList() );

    }
    public void factordetractors(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);

        String[] columns = {"factor_name", "factor_id",  "score","suffix"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        System.out.println("--query one getFactorScores" + query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("title", "title");
        columnInfo.put("id", "id");
        columnInfo.put("score", "score");
        columnInfo.put("suffix", "suffix");


        Map<String, LIWhereconditionmapRecord> filtersMapdelaer = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMapdelaer, query);
        query=acknowledgmentMap.get("QUERY");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("score")!=null)
                .map(s->s.get("score")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        Map output = new HashMap();
        output.put("data", rows);
        service.writeOutput(response, output);

    }
}
