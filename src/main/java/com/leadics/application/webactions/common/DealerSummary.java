package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.application.to.LIWhereconditionmapRecord;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class DealerSummary {
	public static final String GREY_COLOR = "#4CB341";
	public static final String BLACK_COLOR = "#000000";
	public static final String BLUE_COLOR = "#087894";
	public static final String AMBER_COLOR = "#FEAD5C";
	public static final String RED_COLOR = "#E10000";
	public static final String GREEN_COLOR = "#4CB341";
	public static final String BLUE_COLOR_BENCHMARK = "#087894";
	public static final String RED_COLOR_BENCHMARK = "#E10000";
	public static final String GREEN_COLOR_BENCHMARK = "#4CB341";
	
    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }

    public void getDealerSummaryDealerIndexScore(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
    	LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        System.out.println("index QUERY :::::::"+query);
        String[] columns = {"score", "wave"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnList.forEach(y->{
            columnInfo.put(y,y);
        });
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        List<HashMap> innerRows=rows.stream().map(x-> getDataPackNationaScore(x.get("score"),x.get("wave"))).collect(Collectors.toList());
        Map data=new HashMap();
        data.putAll(innerRows.get(0));
        data.put("chartData",null);
        service.writeOutput(response,data );
    }
    
    public void getTop5Delighted(HttpServletRequest request, HttpServletResponse response, String methodName)
			throws Exception {
		LIDAO dao = new LIDAO();
		LIService service = new LIService();
		String query = service.getQureyRebuild(request);
		String belowStudyAverage = request.getParameter("belowStudyAverage");
		String[] columns = { "categories", "score", "studyavg", "bestscore", "worstscore", "mean" };
		ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
		String[] seriesName = { "score" };
		ArrayList<String> seriesNames = new ArrayList<String>(Arrays.asList(seriesName));
		String upperBenchmark = request.getParameter("upper_benchmark");
		LinkedHashMap columnInfo = new LinkedHashMap();
		columnList.stream().forEach(x -> columnInfo.put(x, x));
		Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
		Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
		System.out.println(query);
		query = acknowledgmentMap.get("QUERY");

		System.out.print("-----------------test------------------");
		System.out.print(query);
		List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
		Set dataCheck = rows.stream().filter(x -> x.get("score") != null).map(s -> s.get("score"))
				.collect(Collectors.toSet());
		if (dataCheck.size() <= 0) {
			response.setStatus(204);
			service.writeOutput(response, " ");
		}
		packingutils packingUitility = new packingutils();
		List<String> categories = getCategories(rows, request);
		List FactorScore = new LinkedList();
		if (belowStudyAverage != null && belowStudyAverage.equalsIgnoreCase("true")) {
			FactorScore = packingUitility.getColoredChagedScore(rows, "score", "worstscore", request);
			if (FactorScore.size() <= 0) {
				response.setStatus(204);
				service.writeOutput(response, " ");
			}
		} else {
			FactorScore = rows.stream().collect(Collectors.mapping(map -> {
				return new DataClassWithImpact(packingutils.getConvertToInt(map.get("score")), map.get("mean"));
			}, Collectors.toList()));
		}
		List Maxscore = packingUitility.getScoresAsInt(rows, GREEN_COLOR_BENCHMARK, "best_score");
		List MinScore = packingUitility.getScoresAsInt(rows, RED_COLOR_BENCHMARK, "worst_score");

		Map cols = new HashMap();		
		if ((upperBenchmark != null) && (upperBenchmark.contains("Best"))) {
			seriesNames.add(upperBenchmark);
			cols.put(upperBenchmark, Maxscore);
		}

		cols.put("score", FactorScore);
		cols.put("categories", categories);
		HashMap finalOutput = new HashMap();
		finalOutput.put("percentPrefix", "%");
		finalOutput.put("data", cols);
		finalOutput.put("seriesName", seriesNames);
		finalOutput.put("seriesValue", seriesNames);

		HashMap output = new HashMap();
		output.put("data", finalOutput);
		service.writeOutput(response, output);
	}
    
    private Boolean getConvertedToBoolean(String isgrowthIncremented) {
        if(isgrowthIncremented.equalsIgnoreCase("1")){
            return true;
        }else{
            return false;
        }
    }

    
    private HashMap getDataPackNationaScore(String score, String growth, String title, String label, String maxValue, Boolean isgrowthIncremented,String rank,String toolTip){
        HashMap temp=new HashMap();
        temp.put("score",score);
        temp.put("growth",growth);
        temp.put("title",title);
        temp.put("label",label);
        temp.put("maxValue",maxValue);
        temp.put("isgrowthIncremented",isgrowthIncremented);
        temp.put("rank",rank);
        temp.put("toolTip",toolTip);

        return temp;
    }
           
    private HashMap getDataPackNationaScore(String score, String wave){
        HashMap temp=new HashMap();
        temp.put("score",score);
        temp.put("wave",wave);
        return temp;
    }
    
    private List<String> getCategories(List<HashMap<String, String>> rows, HttpServletRequest request) {
		List<String> Categories = rows.stream().filter(map -> map.containsKey("categories"))
				.map(map -> map.get("categories")).collect(Collectors.toList());
		System.out.println("Categories ::::::::: " + Categories);
		return Categories;
	}
}
