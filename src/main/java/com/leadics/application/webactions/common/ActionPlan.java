package com.leadics.application.webactions.common;

import static com.leadics.application.common.LIAction.AUTHORIZATION;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.services.cognitoidp.model.transform.UserPoolAddOnsTypeJsonUnmarshaller;
import org.json.simple.JSONObject;

import com.leadics.application.to.NameValuePair; 

import com.leadics.application.common.*;
import com.leadics.utils.PropertyUtil;
import com.leap.core.auth.idam.utils.TokenUtil;
import com.leap.core.common.spi.TenantContext;
import com.leap.core.services.actionPlan.representations.ActionPlanCreateRepresentation;
import com.leap.core.services.actionPlan.spi.ActionPlanClient;

public class ActionPlan {
	public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
			throws Exception {
		response.setContentType("text/html");

		try (PrintWriter out = response.getWriter()) {
			try {
				Method method = this.getClass().getMethod(actionType, HttpServletRequest.class,
						HttpServletResponse.class, String.class);
				method.invoke(this, request, response, actionType);
			} catch (Exception e) {
				e.printStackTrace();
				LIService service=new LIService();
				response.setStatus(555);
				service.writeOutput(response,"-1");
			}
		}
	}

	public void createActionPlan(HttpServletRequest request, HttpServletResponse response, String methodName)
			throws Exception {

//		String authHeader = request.getHeader(AUTHORIZATION);
//		String tenant = TokenUtil.fetchRealmFromToken(authHeader);
		LIDAO dao = new LIDAO();
		LIService service= new  LIService();
		
		List<Detractor> detractorList = service.getPostBody(request);
		String query = "select * from dealer_action_plan_info ";
		HashMap<String, String> columnInfo = new HashMap<>();
		columnInfo.put("dealer", "dealer");
		columnInfo.put("dealer_code", "dealer_code");
		columnInfo.put("area", "area");
		columnInfo.put("category", "category");
		columnInfo.put("kpi_target", "kpi_target");
		columnInfo.put("description_template", "description_template");
		columnInfo.put("kpi", "kpi");
		columnInfo.put("assinee_mail_id", "assinee_mail_id");
		columnInfo.put("assigner_mail_id", "assigner_mail_id");
		columnInfo.put("assinee_name", "assinee_name");
		try {
			List<HashMap<String, String>> actionPlanData = dao.loadValuesRowWise(query, columnInfo);
			List dealers = actionPlanData.stream().collect(Collectors.toList());

			for (Detractor detractor : detractorList) {
				String dealerCode = detractor.getMGDealerCode();
				HashMap<String, String> detractorInfo = getDetractorInfo(dealerCode, dealers);
				if (detractorInfo != null) {
					String description = detractorInfo.get("description_template");
					String area = detractor.getDealer();
					String category = detractorInfo.get("category");
					String kpi = detractorInfo.get("kpi");
					String kpi_target = detractorInfo.get("kpi_target");
					String assigneeId = detractorInfo.get("assinee_mail_id").trim();
					String assignerId = detractorInfo.get("assigner_mail_id").trim();
					String assignerName = detractorInfo.get("assinee_name");
					Integer APPLICATION_ID = Integer.parseInt(PropertyUtil.getProperty("APPLICATION_ID"));
					// replace the string
					description = description.replace("<userName>", assignerName);
					description = description.replace("<respondentName>", detractor.getName());
					description = description.replace("<phoneNum>", detractor.getPhoneNumber());
					description = description.replace("<modelName>", detractor.getModelId());
					description = description.replace("<dealerName>", detractor.getDealer());
					description = description.replace("<Date>", detractor.getDate().toString());
					description = description.replace("<Overall_satisfaction>", detractor.getOverall_satisfaction().toString());
					description = description.replace("<Likelihood_to_recommend>", detractor.getLikelihood_to_recommend().toString());
					description = description.replace("<Other_feedback>", detractor.getOther_feedback().toString());
					
					String factorsString = "";
					ArrayList<NameValuePair> temp = detractor.getFactors();
					for(NameValuePair tempArray:temp){
						factorsString+=tempArray.getName()+" : "+tempArray.getValue()+", ";
					}
					factorsString=factorsString.substring(0,factorsString.lastIndexOf(","));
					description = description.replace("<FACTOR_COUNT>", temp.size()+"");
					if(temp.size()>1){
						description=description.replace("<FACTOR_STRING>"," factors");
					}else {
						description=description.replace("<FACTOR_STRING>"," factor");
					}
					description = description.replace("<FACTORS>", factorsString);
					Integer tempDate= Integer.valueOf(detractor.getDate().toString().substring(0,1));
					String proposedDate= String.valueOf(tempDate+15);
					proposedDate=proposedDate+" "+detractor.getDate().toString().substring(2,detractor.getDate().length());
					ActionPlanCreateRepresentation rps = ActionPlanCreateRepresentation
							.Builder.create(APPLICATION_ID,
							area, category, kpi, description, Arrays.asList(assigneeId), kpi_target, assignerId).
					build();
					//TenantContext.setCurrentTenant(tenant);
					ActionPlanClient client = new ActionPlanClient();
					try {
						client.create(rps);
						System.out.println("Action plan created by Dashboard API");
						response.setStatus(200);
						service.writeOutput(response, " ");

					} catch (Exception e) {
						System.out.println("Action plan failed by Dashboard API");
						response.setStatus(555);
						service.writeOutput(response, " ");

					}
				}else{
					System.out.println("Action plan failed by Dashboard API");
					response.setStatus(555);
					service.writeOutput(response, " ");
				}

			}

		} catch (SQLException ex) {
			System.out.println("Action plan failed by Dashboard API");
			ex.printStackTrace();
			response.setStatus(555);
			Logger.getLogger(ActionPlan.class.getName()).log(Level.SEVERE, null, ex);
		}



	}

	private HashMap<String, String> getDetractorInfo(String dealerCode, List<HashMap<String, String>> dealers) {
		for (HashMap<String, String> dealer : dealers) {
			if (dealer.get("dealer_code").equalsIgnoreCase(dealerCode)) {
				System.out.println("true");
				return dealer;
			}

		}
		System.out.println("false");
		return null;
	}


	public class Detractor {
		private String respodentName;
		private String phoneNumber;
		private String dealer;
		private String MGDealerCode;
		private String date;
		private String modelId;
		private String Overall_satisfaction;
		private String Likelihood_to_recommend;
		private String Other_feedback;
		private ArrayList<NameValuePair> factors;
		public Detractor(String name, String phoneNumber, String dealer,  String date, String modelId,String Overall_satisfaction, String Likelihood_to_recommend, String Other_feedback,  ArrayList<NameValuePair> factors) {
            this.respodentName = name;
            this.phoneNumber = phoneNumber;
            this.dealer = dealer;
            this.MGDealerCode = MGDealerCode;
            this.Overall_satisfaction = Overall_satisfaction;
            this.Likelihood_to_recommend = Likelihood_to_recommend;
            this.Other_feedback = Other_feedback;         
            this.date = date;
            this.modelId = modelId;
            this.factors = factors;
        }
		public String getName() {
			return respodentName;
		}

		
		public String getOverall_satisfaction() {
			return Overall_satisfaction;
		}
		public String getLikelihood_to_recommend() {
			return Likelihood_to_recommend;
		}
		public String getOther_feedback() {
			return Other_feedback;
		}
		public void setOverall_satisfaction(String Overall_satisfaction) {
			this.Overall_satisfaction = Overall_satisfaction;
		}
		public void setLikelihood_to_recommend(String Likelihood_to_recommend) {
			this.Likelihood_to_recommend = Likelihood_to_recommend;
		}
		public void setOther_feedback(String Other_feedback) {
			this.Other_feedback = Other_feedback;
		}
		
		
		
		public void setName(String respodentName) {
			this.respodentName = respodentName;
		}

		public String getPhoneNumber() {
			return phoneNumber;
		}

		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}

		public String getDealer() {
			return dealer;
		}

		public void setDealer(String dealer) {
			this.dealer = dealer;
		}
		
		public String getMGDealerCode() {
			return MGDealerCode;
		}

		public void setMGDealerCode(String MGDealerCode) {
			this.MGDealerCode = MGDealerCode;
		}
		public String getDate() {
			return date;
		}
		

		public void setDate(String date) {
			this.date = date;
		}

		public String getModelId() {
			return modelId;
		}

		public void setModelId(String modelId) {
			this.modelId = modelId;
		}

		public ArrayList<NameValuePair> getFactors() {
			return factors;
		}

		public void setFactors(ArrayList<NameValuePair> factors) {
			this.factors = factors;
		}

	}
}
