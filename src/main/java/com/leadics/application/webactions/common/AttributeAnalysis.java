package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.application.to.LIWhereconditionmapRecord;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.leadics.application.webactions.common.TrendAnalysis.GREEN_COLOR_BENCHMARK;
import static com.leadics.application.webactions.common.TrendAnalysis.RED_COLOR_BENCHMARK;
import static java.util.stream.Collectors.groupingBy;

public class AttributeAnalysis {


    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }
    public void factorScores(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        System.out.println("QUERY ::::::::"+query);
        System.out.println("query one getFactorScores" + query);
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("factor_name", "title");
        columnInfo.put("factor_id", "id");
        columnInfo.put("score1", "score1");
        columnInfo.put("score2", "score2");
        columnInfo.put("score", "score");
        columnInfo.put("badgeScore", "badgeScore");
        columnInfo.put("badgeVisible", "badgeVisible");
        columnInfo.put("studyAverage", "studyAverage");
        columnInfo.put("badgeLable","badgeLable");

        String upperBenchmark = request.getParameter("upper_benchmark");
        String lowerBenchmark = request.getParameter("lower_benchmark");

        Map<String, LIWhereconditionmapRecord> filtersMapdelaer = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMapdelaer, query);
        System.out.print("final query");
        System.out.print(query);
        String belowStudyAverage = request.getParameter("belowStudyAverage");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        List FactorScore = new LinkedList();
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("score")!=null)
                .map(s->s.get("score")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        if (belowStudyAverage != null && belowStudyAverage.equalsIgnoreCase("true")) {

            FactorScore = rows.stream().collect(
                    Collectors.mapping(rowScore
                                    -> new RowBadgeTypeChange(
                                    rowScore.get("title"),
                                    rowScore.get("id"),
                                    rowScore.get("score1"),
                                    rowScore.get("score2"),
                                    rowScore.get("score"),
                                    rowScore.get("badgeScore"),
                                    changeType(rowScore.get("badgeVisible")),
                                    changeTypeWithFilter(upperBenchmark),
                                    changeTypeWithFilter(lowerBenchmark),
                                    changeColorbaseOnScore(rowScore.get("badgeScore")),
                                    getColorBasedOnStudyFactors(rowScore.get("score"),rowScore.get("studyAverage")),
                                    rowScore.get("badgeLable")
                            ),
                            Collectors.toList()));
            if (FactorScore.size() <= 0) {
                response.setStatus(204);
                service.writeOutput(response, " ");
            }
        } else {
            FactorScore =rows.stream().collect(
                    Collectors.mapping(rowScore
                                    -> new RowBadgeTypeChange(
                                    rowScore.get("title"),
                                    rowScore.get("id"),
                                    rowScore.get("score1"),
                                    rowScore.get("score2"),
                                    rowScore.get("score"),
                                    rowScore.get("badgeScore"),
                                    changeType(rowScore.get("badgeVisible")),
                                    changeTypeWithFilter(upperBenchmark),
                                    changeTypeWithFilter(lowerBenchmark),
                                    changeColorbaseOnScore(rowScore.get("badgeScore")),
                                    "Balck",
                                    rowScore.get("badgeLable")
                            ),
                            Collectors.toList()));
        }


        Map output = new HashMap();
        output.put("data", FactorScore);
        service.writeOutput(response, output);


    }

    private String getColorBasedOnStudy(String colOne, String colTwo) {
        System.out.println("Score one"+colOne);
        System.out.println("Score two"+colTwo);
        if (Integer.parseInt(colTwo) <Integer.parseInt(colOne)) {
            return "RED";
        } else {
            return "Black";
        }
    }

    private String getColorBasedOnStudyFactors(String colOne, String colTwo) {
        System.out.println("Score one"+colOne);
        System.out.println("Score two"+colTwo);
        if (colOne !=null && colTwo !=null && Integer.parseInt(colTwo) >Integer.parseInt(colOne)) {
            return "RED";
        } else {
            return "BLACK";
        }
    }

    private List<String> getCategories(List<HashMap<String, String>> rows,HttpServletRequest request) throws Exception {
        LIService service = new LIService();
        String query=service.getQuery("mg-motors-ssi-sample-found");
        query=service.getQureyRebuild("mg-motors-ssi-sample-found",query,request);
        LIDAO dao=new LIDAO();
        System.out.println("QUERY :::::::::::::::: "+query);
        String countValue=dao.loadString(query,"total_count");
        int count=countValue==null?null:Integer.parseInt(countValue);
        List<String> Categories = rows.stream()
                .filter(map -> map.containsKey("categories"))
                .map(map -> {
                    String cat=map.get("categories");
                    if(count<10){
                        return cat+"**";
                    }else if(count >10 && count <30){
                        return cat+"*";
                    }else{
                        return cat;
                    }
                })
                .collect(Collectors.toList());
        System.out.println("Categories ::::::::: "+Categories);
        return Categories;
    }
    public void reasonforDetraction(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        String belowStudyAverage = request.getParameter("belowStudyAverage");
        String[] columns = {"categories", "score","study_average","best_score","worst_score"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        String[] seriesName={"score"};
        ArrayList<String> seriesNames = new ArrayList<String>(Arrays.asList(seriesName));
        String upperBenchmark = request.getParameter("upper_benchmark");
        String lowerBenchmark = request.getParameter("lower_benchmark");
        LinkedHashMap columnInfo=new LinkedHashMap();
        columnList.stream().forEach(x->columnInfo.put(x,x));
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        System.out.println(query);
        query=acknowledgmentMap.get("QUERY");
       
        System.out.print("-----------------test------------------");
        System.out.print(query);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("score")!=null)
                .map(s->s.get("score")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        packingutils packingUitility = new packingutils();
        List<String> categories =getCategories(rows,request);
        List FactorScore = new LinkedList();
        if (belowStudyAverage != null && belowStudyAverage.equalsIgnoreCase("true")) {
            FactorScore = packingUitility.getColoredChagedScore(rows, "score", "study_average",request);
            if (FactorScore.size() <= 0) {
                response.setStatus(204);
                service.writeOutput(response, " ");
            }
        } else {
            FactorScore = packingUitility.getScoresAsInt(rows, "#FB9E9F", "score");
        }
        List Maxscore = packingUitility.getScoresAsInt(rows, GREEN_COLOR_BENCHMARK, "best_score");
        List MinScore = packingUitility.getScoresAsInt(rows, RED_COLOR_BENCHMARK, "worst_score");

        Map cols=new HashMap();
        if ((lowerBenchmark != null) && (lowerBenchmark.contains("Worst"))) {
            seriesNames.add(lowerBenchmark);
            cols.put(lowerBenchmark, MinScore);
        }
        if ((upperBenchmark != null) && (upperBenchmark.contains("Best"))) {
            seriesNames.add(upperBenchmark);
            cols.put(upperBenchmark, Maxscore);
        }

        cols.put("score",FactorScore);
        cols.put("categories",categories);
        HashMap finalOutput=new HashMap();
        finalOutput.put("percentPrefix","%");
        finalOutput.put("data",cols);
        finalOutput.put("seriesName",seriesNames);
        finalOutput.put("seriesValue",seriesNames);

         HashMap output=new HashMap();
         output.put("data",finalOutput);
        service.writeOutput(response, output );

    }
    private Boolean changeTypeWithFilter(String get) {
        if (get.equalsIgnoreCase("none")) {
            return false;
        } else {
            return true;
        }
    }

    private String changeColorbaseOnScore(String score) {

        if (score.contains("-")) {
            return "danger";
        } else {
            return "success";
        }

    }
    public Boolean changeType(String get) {
        if (get.equalsIgnoreCase("False")) {
            return false;
        } else {
            return true;
        }
    }
}
class SeriesObj{
    Object y;
    String color;

    public SeriesObj(Object y, String color) {
        this.y = y;
        this.color = color;
    }
}
