package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.utils.ChartData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;


public class TrendAnalysis {
    public static final String BLUE_COLOR_BENCHMARK = "#087894";
    public static final String RED_COLOR_BENCHMARK = "#E10000";
    public static final String GREEN_COLOR_BENCHMARK = "#4CB341";
    packingutils packingutils=new packingutils();

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }
    
    public void getFactorTrend(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = service.getQureyRebuild(request);
        query = service.getQureyRebuild("dealers-region-marker",query,request);
        String upperBenchmark = request.getParameter("upper_benchmark");
        String lowerBenchmark = request.getParameter("lower_benchmark");
        String[] columns = {"score", "maxscore", "minscore", "categories"};
        System.out.println("query checks *********** " + query);
        HashMap<String, String> columnInfo = new HashMap<>();
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        columnList.stream().forEach(x -> columnInfo.put(x, x));
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();
        List<String> categories = getCategories(rows);
        List FactorScore = packingutils.getScoresAsIntWithFilterValue(rows, "#6F7CEF", "score","filterValue");
        if (FactorScore.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        List Maxscore = packingutils.getScoresAsInt(rows, GREEN_COLOR_BENCHMARK, "maxscore");
        String colorAverage;
        if (lowerBenchmark.contains("Average")) {
            colorAverage = BLUE_COLOR_BENCHMARK;
        } else {
            colorAverage = RED_COLOR_BENCHMARK;
        }
        List MinScore = packingutils.getScoresAsInt(rows, colorAverage, "minscore");
        Map innerOutput = new HashMap();
        innerOutput.put("categories", categories);
        innerOutput.put("factor", FactorScore);
        innerOutput.put("maxscore", Maxscore);
        innerOutput.put("minscore", MinScore);
        Map output = new HashMap();
        output.put("data", innerOutput);
        Map outeOoutput = new HashMap();
        outeOoutput.put("data", output);
        ArrayList seriesName = new ArrayList();
        seriesName.add("factor");
        ArrayList seriesValue = new ArrayList();
        seriesValue.add("Factor Scores");
        if ((upperBenchmark != null) && !upperBenchmark.contains("None")) {
            seriesValue.add(upperBenchmark);
        }

        if ((lowerBenchmark != null) && !lowerBenchmark.contains("None")) {
            seriesValue.add(lowerBenchmark);
        }
        Boolean checkUpperStudyBench = (upperBenchmark != null) && upperBenchmark.contains("Best") ? seriesName.add("maxscore") : false;
        Boolean checkLowerStudyBench = (lowerBenchmark != null) && (lowerBenchmark.contains("Worst") || lowerBenchmark.contains("Average")) ? seriesName.add("minscore") : false;
        output.put("seriesName", seriesName);
        output.put("seriesValue", seriesValue);
        outeOoutput.put("data", output);
        service.writeOutput(response, outeOoutput);

    }
    
    public void getAttributeMeanTrend(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
    	LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = service.getQureyRebuild(request);
        System.out.println(query);
        String upperBenchmark = request.getParameter("upper_benchmark");
        String lowerBenchmark = request.getParameter("lower_benchmark");
        String[] columns = {"score", "categories","attribute"};
        System.out.println("query checks *********** "+query);
        HashMap<String, String> columnInfo = new HashMap<>();
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        columnList.stream().forEach(x -> columnInfo.put(x, x));
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();
        List categories = getCategories(rows);
        Set setCategories = new LinkedHashSet<>(categories);
        LinkedHashMap innerListofReasons = rows.stream().collect(
                Collectors.groupingBy(map -> map.get("attribute"), LinkedHashMap::new, Collectors.mapping(map -> {
            return new DataClassDouble(map.get("score"), null);
        }, Collectors.toList())));
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("score")!=null)
                .map(s->s.get("score")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        Map outPut=new HashMap();
        outPut.put("data",innerListofReasons);
        outPut.put("categories",setCategories);


        Set seriesName =  innerListofReasons.keySet();
        Set seriesValue= innerListofReasons.keySet();
        Map outPuts=new HashMap();
        outPuts.put("data",outPut);
        outPuts.put("seriesName",seriesName);
        outPuts.put("seriesValue",seriesValue);
        service.writeOutput(response,outPuts);
    }
    public List<String> getCategories(List<HashMap<String, String>> rows) {
        List<String> Categories = rows.stream()
                .filter(map -> map.containsKey("categories"))
                .map(map -> map.get("categories"))
                .collect(Collectors.toList());
        System.out.println("Categories ::::::::: "+Categories);
        return Categories;
    }
}
class DataClassDouble {
    Double y;
    String name;
    String color;
    public DataClassDouble(String value) {
        this.y= Double.valueOf(value);
    }
    public DataClassDouble(String value,String name,String color){
    	Double temp;
        try {
             temp = value == null ? null : Double.valueOf(value);
        }catch (NumberFormatException ne){
            double d = value == null ?null:Double.parseDouble(value);
            temp = (double) d;
        }
        this.y= temp;
        this.name=name;
        this.color=color;
    }
    public DataClassDouble(String value,String color){
    	Double temp=value==null?null:Double.valueOf(value);
        this.y= temp;
        this.color=color;
        

    }
}