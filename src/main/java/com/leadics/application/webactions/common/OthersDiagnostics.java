package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.application.to.LIWhereconditionmapRecord;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

import static com.leadics.application.webactions.common.TrendAnalysis.GREEN_COLOR_BENCHMARK;
import static com.leadics.application.webactions.common.TrendAnalysis.RED_COLOR_BENCHMARK;

public class OthersDiagnostics {
    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }
    
    public void getOthersTable(HttpServletRequest request, HttpServletResponse response, String methodName)
			throws Exception {
		LIDAO dao = new LIDAO();
		LIService service = new LIService();

		// fetching data starts
		String query = service.getQureyRebuild(request);
//		dao.executeUpdateQuery("SET @r1=0,  @r2=0,  @r3=0,  @r4=0,  @r5=0,  @r6=0,  @r7=0, @r8=1000, @r9=10000,@rsum=0, @rorder=0;");
		Map<String, LIWhereconditionmapRecord> filtersMapdelaer = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMapdelaer, query);
        query=acknowledgmentMap.get("QUERY");
        System.out.println(query);
        
		//get headers - column names
		LinkedHashMap<String, String> columnList = dao.getColumnsFromResultSet(query);
		
		List<HashMap<String, String>> rowsData = new ArrayList<HashMap<String, String>>();
		rowsData = dao.loadValuesRowWise(query, columnList);
		
		List<HashMap<String, Object>> headers = new ArrayList<HashMap<String, Object>>();

		for (Map.Entry element : columnList.entrySet()) {
			LinkedHashMap<String, Object> header = new LinkedHashMap<String, Object>();

			String key = (String) element.getKey();
			String value = (String) element.getValue();
			
			Integer minWidth=(value=="Delivery Date")?10:100;
				
			header.put("field", value);
			header.put("maxWidth", null);
			header.put("minWidth", minWidth);
			header.put("sortable", true);
			header.put("headerName", value);
			header.put("tooltipField", value);
			headers.add(header);
		}
		
		
		List<HashMap<String, String>> tableData = new ArrayList<HashMap<String, String>>();
		rowsData.forEach(x -> {
			HashMap<String, String> values = new HashMap<String, String>();
			for (String key : x.keySet()) {
				String value = x.get(key);
				values.put(key, value);
			}
			tableData.add(values);
		});

		HashMap data = new HashMap();
		data.put("header", headers);
		if(rowsData.size()<=0){
			System.out.println(rowsData.size());
			List temp=new ArrayList();
			data.put("tableData", temp);
			service.writeOutput(response, data);
		}else{
			data.put("tableData", tableData);
			String stringData= URLEncoder.encode(new JSONObject(data).toString(), "UTF-8");
			service.writeOutput(response, stringData);
		}


	}

    public  void getOthersCharts(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        System.out.println("QUERY ::::::::"+query);
        String[] columns = {"categories", "dealerscore"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("categories", "categories");
        columnInfo.put("dealerscore", "dealerscore");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        List categories = getCategories(rows);
        packingutils packingutils=new packingutils();
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("dealerscore")!=null)
                .map(s->s.get("dealerscore")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        List<DataClass> innerData = rows
                .stream()
                .map(s -> packingutils.getDataObjet(s.get("dealerscore"), s.get("categories"), null))
                .collect(Collectors.toList());

        Map output = new HashMap();
        output.put("dataset", innerData);
        output.put("categories", categories);
        service.writeOutput(response, output);
    }
    
    public  void getOverallBuyingExperience(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        System.out.println("QUERY ::::::::"+query);
        String[] columns = {"name", "score","color"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("name", "name");
        columnInfo.put("score", "score");
        columnInfo.put("color", "color");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("score")!=null && !x.get("name").equalsIgnoreCase("Total Count"))
                .map(s->s.get("score")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        List<DataClass> innerData = rows
                .stream()
                .filter(f -> !f.get("name").equalsIgnoreCase("Total Count"))
                .map(s -> packingutils.getDataObjet(s.get("score"), s.get("name"), s.get("color")))
                .collect(Collectors.toList());
        List<String> innerDatacount = rows
                .stream()
                .filter(f -> f.get("name").equalsIgnoreCase("Total Count"))
                .map(s -> s.get("score"))
                .collect(Collectors.toList());

        Integer temp;
        try {
            temp = innerDatacount.get(0) == null ? null : Integer.valueOf(innerDatacount.get(0));
        }catch (NumberFormatException ne){
            double d = innerDatacount.get(0) == null ?null:Double.parseDouble(innerDatacount.get(0));
            temp = (int) d;
        }
        Map output = new HashMap();
        output.put("dataset", innerData);
        output.put("Total Count", temp);

        service.writeOutput(response, output);
    }

    public  void getOverallTestDriveExperience(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        System.out.println("QUERY ::::::::"+query);
        String[] columns = {"name", "score","color"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("name", "name");
        columnInfo.put("score", "score");
        columnInfo.put("color", "color");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("score")!=null && !x.get("name").equalsIgnoreCase("Total Count"))
                .map(s->s.get("score")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        List<DataClass> innerData = rows
                .stream()
                .filter(f -> !f.get("name").equalsIgnoreCase("Total Count"))
                .map(s -> packingutils.getDataObjet(s.get("score"), s.get("name"), s.get("color")))
                .collect(Collectors.toList());
        List<String> innerDatacount = rows
                .stream()
                .filter(f -> f.get("name").equalsIgnoreCase("Total Count"))
                .map(s -> s.get("score"))
                .collect(Collectors.toList());


        Map output = new HashMap();
        output.put("dataset", innerData);
        output.put("Total Count", innerDatacount.get(0));

        service.writeOutput(response, output);
    }

    public  void getEngagementDuringWaiting(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        System.out.println("QUERY ::::::::"+query);
        String[] columns = {"name", "score","color"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("name", "name");
        columnInfo.put("score", "score");
        columnInfo.put("color", "color");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("score")!=null && !x.get("name").equalsIgnoreCase("Total Count"))
                .map(s->s.get("score")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }

        List<DataClass> innerData = rows
                .stream()
                .filter(f -> !f.get("name").equalsIgnoreCase("Total Count"))
                .map(s -> packingutils.getDataObjet(s.get("score"), s.get("name"), s.get("color")))
                .collect(Collectors.toList());
        List<String> innerDatacount = rows
                .stream()
                .filter(f -> f.get("name").equalsIgnoreCase("Total Count"))
                .map(s -> s.get("score"))
                .collect(Collectors.toList());


        Map output = new HashMap();
        output.put("dataset", innerData);
        output.put("Total Count", innerDatacount.get(0));

        service.writeOutput(response, output);
    }
    public  void getImprovemetOppertunities(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        String belowStudyAverage = request.getParameter("belowStudyAverage");
        String[] columns = {"categories", "score","study_average","best_score","worst_score"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        String[] seriesName={"score"};
        ArrayList<String> seriesNames = new ArrayList<String>(Arrays.asList(seriesName));
        String upperBenchmark = request.getParameter("upper_benchmark");
        String lowerBenchmark = request.getParameter("lower_benchmark");
        LinkedHashMap columnInfo=new LinkedHashMap();
        columnList.stream().forEach(x->columnInfo.put(x,x));
        Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
        System.out.println(query);
        query=acknowledgmentMap.get("QUERY");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("score")!=null)
                .map(s->s.get("score")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        packingutils packingUitility = new packingutils();
        List<String> categories =getCategories(rows,request);
        List FactorScore = new LinkedList();
        if (belowStudyAverage != null && belowStudyAverage.equalsIgnoreCase("true")) {
            FactorScore = packingUitility.getColoredChagedScore(rows, "score", "study_average",request);
            if (FactorScore.size() <= 0) {
                response.setStatus(204);
                service.writeOutput(response, " ");
            }
        } else {
            FactorScore = packingUitility.getScoresAsInt(rows, "#FB9E9F", "score");
        }
        List Maxscore = packingUitility.getScoresAsInt(rows, GREEN_COLOR_BENCHMARK, "best_score");
        List MinScore = packingUitility.getScoresAsInt(rows, RED_COLOR_BENCHMARK, "worst_score");

        Map cols=new HashMap();
        if ((lowerBenchmark != null) && (lowerBenchmark.contains("Worst"))) {
            seriesNames.add(lowerBenchmark);
            cols.put(lowerBenchmark, MinScore);
        }
        if ((upperBenchmark != null) && (upperBenchmark.contains("Best"))) {
            seriesNames.add(upperBenchmark);
            cols.put(upperBenchmark, Maxscore);
        }

        cols.put("score",FactorScore);
        cols.put("categories",categories);
        HashMap finalOutput=new HashMap();
        finalOutput.put("percentPrefix","%");
        finalOutput.put("data",cols);
        finalOutput.put("seriesName",seriesNames);
        finalOutput.put("seriesValue",seriesNames);

        HashMap output=new HashMap();
        output.put("data",finalOutput);
        service.writeOutput(response, output );
    }

    private List<String> getCategories(List<HashMap<String, String>> rows,HttpServletRequest request) throws Exception {
        LIService service = new LIService();
        String query=service.getQuery("mg-motors-ssi-sample-found");
        query=service.getQureyRebuild("mg-motors-ssi-sample-found",query,request);
        LIDAO dao=new LIDAO();
        String countValue=dao.loadString(query,"total_count");
        int count=Integer.parseInt(countValue);
        List<String> Categories = rows.stream()
                .filter(map -> map.containsKey("categories"))
                .map(map -> {
                    String cat=map.get("categories");
                    if(count<10){
                        return cat+"**";
                    }else if(count >10 && count <30){
                        return cat+"*";
                    }else{
                        return cat;
                    }
                })
                .collect(Collectors.toList());
        System.out.println("Categories ::::::::: "+Categories);
        return Categories;
    }
    
    public List<String> getCategories(List<HashMap<String, String>> rows) {
        List<String> Categories = rows.stream()
                .filter(map -> map.containsKey("categories"))
                .map(map -> map.get("categories"))
                .collect(Collectors.toList());
        System.out.println("Categories ::::::::: "+Categories);
        return Categories;
    }
}
