package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class ImprovementOppertunities {
    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }
    public void getTop5ImprovementOpportunities(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = service.getQureyRebuild(request);
        System.out.println(query);
        String upperBenchmark = request.getParameter("upper_benchmark");
        String lowerBenchmark = request.getParameter("lower_benchmark");
        String[] columns = {"score", "categories","reason_display_name","color"};
        System.out.println("query checks *********** "+query);
        HashMap<String, String> columnInfo = new HashMap<>();
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        columnList.stream().forEach(x -> columnInfo.put(x, x));
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();
        List categories = getCategories(rows);
        Set setCategories = new LinkedHashSet<>(categories);
        LinkedHashMap innerListofReasons = rows.stream().collect(
                Collectors.groupingBy(map -> map.get("reason_display_name"), LinkedHashMap::new, Collectors.mapping(map -> {
            return new DataClass(map.get("score"), map.get("color"));
        }, Collectors.toList())));
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("score")!=null)
                .map(s->s.get("score")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        Map outPut=new HashMap();
        outPut.put("data",innerListofReasons);
        outPut.put("categories",setCategories);


        Set seriesName =  innerListofReasons.keySet();
        Set seriesValue= innerListofReasons.keySet();
        Map outPuts=new HashMap();
        outPuts.put("data",outPut);
        outPuts.put("seriesName",seriesName);
        outPuts.put("seriesValue",seriesValue);
        service.writeOutput(response,outPuts);

    }

    private List<String> getCategories(List<HashMap<String, String>> rows) {
        List<String> Categories = rows.stream()
                .filter(map -> map.containsKey("categories"))
                .map(map -> map.get("categories"))
                .collect(Collectors.toList());
        System.out.println("Categories ::::::::: "+Categories);
        return Categories;
    }
    public void getPercentageDelighted(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = service.getQureyRebuild(request);
        System.out.println(query);
        String[] columns = {"score", "title","id"};
        System.out.println("query checks sss *********** "+query);
        HashMap<String, String> columnInfo = new HashMap<>();
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        columnList.stream().forEach(x -> columnInfo.put(x, x));
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Map output=new HashMap();
        output.put("data",rows);
        service.writeOutput(response,output);

    }

}
