/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.application.webactions.common;

class MultiScoreCardTrasform {

    String title;
    String id;
    String score1;
    String score2;
    String score;
    String badgeScore;
    Boolean badgeVisible;
    Boolean ribbonVisible;
    Boolean annotationVisible;
    Boolean scoreOneVisible;
    Boolean scoreTwoVisible;
    String color;
    String target;

    public MultiScoreCardTrasform(String text,
            String id,
            String score1,
            String score2,
            String score,
            String badgeScore,
            Boolean badgeVisible,
            Boolean ribbonVisible,
            Boolean annotationVisible,
            Boolean scoreOneVisible,
            Boolean scoreTwoVisible,
            String color,
    String target) {
        this.title = text;
        this.id = id;
        this.score1 = score1;
        this.score2 = score2;
        this.score = score;
        this.badgeScore = badgeScore;
        this.badgeVisible = badgeVisible;
        this.ribbonVisible = ribbonVisible;
        this.annotationVisible = annotationVisible;
        this.scoreOneVisible = scoreOneVisible;
        this.scoreTwoVisible = scoreTwoVisible;
        this.color = color;
        this.target=target;
    }

    public Boolean getAnnotationVisible() {
        return annotationVisible;
    }

    public void setAnnotationVisible(Boolean annotationVisible) {
        this.annotationVisible = annotationVisible;
    }

    public Boolean getScoreOneVisible() {
        return scoreOneVisible;
    }

    public void setScoreOneVisible(Boolean scoreOneVisible) {
        this.scoreOneVisible = scoreOneVisible;
    }

    public Boolean getScoreTwoVisible() {
        return scoreTwoVisible;
    }

    public void setScoreTwoVisible(Boolean scoreTwoVisible) {
        this.scoreTwoVisible = scoreTwoVisible;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getText() {
        return title;
    }

    public void setText(String text) {
        this.title = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1 = score1;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getBadgeScore() {
        return badgeScore;
    }

    public void setBadgeScore(String badgeScore) {
        this.badgeScore = badgeScore;
    }

    public Boolean getBadgeVisible() {
        return badgeVisible;
    }

    public void setBadgeVisible(Boolean badgeVisible) {
        this.badgeVisible = badgeVisible;
    }

    public Boolean getRibbonVisible() {
        return ribbonVisible;
    }

    public void setRibbonVisible(Boolean ribbonVisible) {
        this.ribbonVisible = ribbonVisible;
    }

    public Boolean getSymbolVisible() {
        return annotationVisible;
    }

    public void setSymbolVisible(Boolean annotationVisible) {
        this.annotationVisible = annotationVisible;
    }

    public Boolean getScoreBestVisible() {
        return scoreOneVisible;
    }

    public void setScoreBestVisible(Boolean scoreOneVisible) {
        this.scoreOneVisible = scoreOneVisible;
    }

    public Boolean getscoreTwoVisible() {
        return scoreTwoVisible;
    }

    public void setscoreTwoVisible(Boolean scoreTwoVisible) {
        this.scoreTwoVisible = scoreTwoVisible;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
