/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.application.webactions.common;

import com.google.gson.Gson;
import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.application.to.LIWhereconditionmapRecord;
import com.leap.core.auth.idam.adapters.KeycloakUser;
import com.leap.core.auth.idam.adapters.KeycloakUserFactory;
import com.leap.core.auth.idam.adapters.UserContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.leadics.application.common.LIAction.AUTHORIZATION;
import static java.util.stream.Collectors.groupingBy;

/**
 *
 * @author Krishna-Kanth;
 */
public class SumamryAccordionTable {
   public static  HashMap listOfTables=new HashMap();

    static{
        listOfTables.put("Top Line Summary","top_line_summary");
        listOfTables.put("Mechanical Shop Performance","mechanical_shop_performance");
        listOfTables.put("Bodyshop performance","bodyshop_performance");
        listOfTables.put("Customer Delight","customer_delight");
        listOfTables.put("Manpower Capability","manpower_capability");
        listOfTables.put("Business Enablers","business_enablers");

    }
    Gson gson;
    public static final String QUERY_TEMPLATE = "QUERY";


    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");
        gson = new Gson();

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }

    public void getSummaryTableData(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        System.out.println("Start of Method "+LocalTime.now());
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String area = request.getParameter("Area");
        String authHeader = request.getHeader(AUTHORIZATION);
        KeycloakUser keycloakUser = KeycloakUserFactory.authenticate(authHeader);
        UserContext userContext = new UserContext(keycloakUser);
        UserContext uc = (UserContext) request.getAttribute("user");
        List<Integer> Roles = uc.getRoles();
        String topLinesumamry = "0";
        System.out.println("Key cloack Auth "+LocalTime.now());
        String headQuery="select * from  kpi_orders_update where 1=1 !areas! order by `order`";  
        String querydealer = "select * from hyundai_dashboard."+listOfTables.get(area)+"  where dealer_flag=1 and <<MAIN_CRITERIA>>   <<YEAR>> <<MONTH>> ORDER BY dealer_flag desc ;";
        String queryworkshop = "select * from hyundai_dashboard."+listOfTables.get(area)+"  where 1=1 and <<MAIN_CRITERIA>>   <<YEAR>> <<MONTH>>    ORDER BY dealer_flag desc;";

        System.out.println("Get Queries "+LocalTime.now());
        Map<String, LIWhereconditionmapRecord> filtersMapdelaer = service.getFiltersMap(querydealer);
        Map<String, String> acknowledgmentMadealer = service.buildWherecondition(request, filtersMapdelaer, querydealer);
        System.out.println("Build Query Where Condtion Dealer "+LocalTime.now());
        Map<String, LIWhereconditionmapRecord> filtersMapworkshop = service.getFiltersMap(queryworkshop);
        Map<String, String> acknowledgmentMapworkshop = service.buildWherecondition(request, filtersMapworkshop, queryworkshop);
        querydealer = acknowledgmentMadealer.get(QUERY_TEMPLATE);
        System.out.println(querydealer);
        System.out.println("Build Query Where Condtion Workshop "+LocalTime.now());
        queryworkshop = acknowledgmentMapworkshop.get(QUERY_TEMPLATE);
        System.out.println("Manual Build Query Start "+LocalTime.now());
        headQuery = headQuery.replaceAll("!areas!", "  and `area`='" + area + "' ");
        if (area.equalsIgnoreCase("Top Line Summary")) {
            topLinesumamry = "1";
            querydealer = querydealer.replaceAll("!area!", " AND `in_summary`='" + topLinesumamry + "' ");
            queryworkshop = queryworkshop.replaceAll("!area!", " AND `in_summary`='" + topLinesumamry + "' ");
            headQuery = headQuery.replaceAll("!area!", " AND `in_summary`='" + topLinesumamry + "' ");
        } else {
            querydealer = querydealer.replaceAll("!area!", " and `Area`='" + area + "' ");
            queryworkshop = queryworkshop.replaceAll("!area!", "  and `Area`='" + area + "' ");
            headQuery = headQuery.replaceAll("!area!", "  and `Area`='" + area + "' ");
        }
        System.out.println("Manual Build Query End "+LocalTime.now());
        ArrayList<String> columnListDealer = new ArrayList<String>(Arrays.asList("dealer", "score", "growth", "kpi", "cal", "underperforming", "show_growth"));
        ArrayList<String> columnListWorkshop = new ArrayList<String>(Arrays.asList("dealer", "workshop", "score", "growth", "kpi", "cal", "underperforming", "show_growth"));
        HashMap<String, String> columnInfoDealer = new HashMap<>();
        columnListDealer.stream()
                .forEach(x -> columnInfoDealer.put(x, x));
        HashMap<String, String> columnInfoWorkshop = new HashMap<>();
        columnListWorkshop.stream()
                .forEach(x -> columnInfoWorkshop.put(x, x));
        System.out.println("Before Fetching Data all set Ready  "+LocalTime.now());
        System.out.println(querydealer);
        System.out.println(queryworkshop);
        List<HashMap<String, String>> rowsDealer = dao.loadValuesRowWise(querydealer, columnInfoDealer);
        System.out.println("get Dealers Data  "+LocalTime.now());
        List<HashMap<String, String>> rowsWorkshop = dao.loadValuesRowWise(queryworkshop, columnInfoWorkshop);
        System.out.println("get Dealers Workshops   "+LocalTime.now());
        Map dealers = rowsWorkshop.stream().collect(groupingBy(rowDealer -> rowDealer.get("dealer"), LinkedHashMap::new,
                groupingBy(rowGroup -> rowGroup.get("workshop"), LinkedHashMap::new,
                        groupingBy(rowKpi -> rowKpi.get("kpi"), LinkedHashMap::new,
                                Collectors.mapping(rowScore -> new Scores(rowScore.get("score"), rowScore.get("growth"), rowScore.get("cal"), rowScore.get("underperforming"), rowScore.get("show_growth")),
                                        Collectors.toList())))));
        System.out.println(LocalTime.now());
        Map parent = rowsDealer.stream().collect(groupingBy(rowDealer -> rowDealer.get("dealer"),
                groupingBy(rowKpi -> rowKpi.get("kpi"), LinkedHashMap::new,
                        Collectors.mapping(rowScore -> new Scores(rowScore.get("score"), rowScore.get("growth"), rowScore.get("cal"), rowScore.get("underperforming"), rowScore.get("show_growth")),
                                Collectors.toList()))));
        System.out.println(LocalTime.now());
        List finalArray = new ArrayList();
        Set parentKeySet = parent.keySet();
        List listparentKeySet = new ArrayList(parentKeySet);
        Collections.sort(listparentKeySet);
        System.out.println(LocalTime.now());
        listparentKeySet.stream().forEach(x -> {
            LinkedHashMap parentHashMap = (LinkedHashMap) (HashMap) parent.get(x);
            HashMap childHashMap = (HashMap) dealers.get(x);
            parentHashMap.put("dealer", x);
            parentHashMap.put("hasChild", true);
            List childernList = new ArrayList();
            Set childKeySet = childHashMap.keySet();
            childKeySet.stream().forEach(y -> {
                HashMap childern = (HashMap) childHashMap.get(y);
                childern.put("dealer", y);
                 childern.put("dealerName", x);
                childernList.add(childern);
                parentHashMap.put("children", childernList);
            });
            finalArray.add(parentHashMap);
        });
      
        ArrayList<String> columnListHead = new ArrayList<String>(Arrays.asList("columns_display"));
        HashMap<String, String> columnInfoHead = new HashMap<>();
        columnListHead.stream()
                .forEach(x -> columnInfoDealer.put(x, x));
        ArrayList rowsHead = dao.loadColumnList(headQuery, "columns_display", false);

//        Set headList = rowsHead.stream().map(x -> x.get("columns_display")).collect(Collectors.toCollection(LinkedHashSet::new));
//        headList.add("dealer");
        rowsHead.add(0, "dealer");
        Map output = new HashMap();
        output.put("data", finalArray);
        output.put("head", rowsHead);
        System.out.println(LocalTime.now());
        service.writeOutput(response, output);

    }
}

class Scores {

    String score;
    String growth;
    String symbol;
    String underperforming;
    String showGrowth;
    String dealerType;

    public Scores(String score, String growth, String symbol, String underperforming, String showGrowth) {
        this.score = score;
        this.growth = growth;
        this.symbol = symbol;
        this.underperforming = underperforming;
        this.showGrowth = showGrowth;
    }

    public Scores(String score, String growth, String cal, String underperforming, String show_growth, String dealerType) {

        this.score = score;
        this.growth = growth;
        this.symbol = symbol;
        this.underperforming = underperforming;
        this.showGrowth = showGrowth;
        this.dealerType=dealerType;

    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getGrowth() {
        return growth;
    }

    public void setGrowth(String growth) {
        this.growth = growth;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getUnderperforming() {
        return underperforming;
    }

    public void setUnderperforming(String underperforming) {
        this.underperforming = underperforming;
    }
    
    public String getShowGrowth() {
        return showGrowth;
    }

    public void setShowGrowth(String showGrowth) {
        this.showGrowth = showGrowth;
    }

}
