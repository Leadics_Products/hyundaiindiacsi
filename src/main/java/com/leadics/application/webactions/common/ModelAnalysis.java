package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.application.to.LIWhereconditionmapRecord;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

public class ModelAnalysis {
	public static final String GREY_COLOR = "#4CB341";
	public static final String BLACK_COLOR = "#000000";
	public static final String BLUE_COLOR = "#087894";
	public static final String AMBER_COLOR = "#FEAD5C";
	public static final String RED_COLOR = "#E10000";
	public static final String GREEN_COLOR = "#4CB341";
	public static final String BLUE_COLOR_BENCHMARK = "#087894";
	public static final String RED_COLOR_BENCHMARK = "#E10000";
	public static final String GREEN_COLOR_BENCHMARK = "#4CB341";

	public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
			throws ServletException, IOException {
		response.setContentType("text/html");

		try (PrintWriter out = response.getWriter()) {
			try {
				Method method = this.getClass().getMethod(actionType, HttpServletRequest.class,
						HttpServletResponse.class, String.class);
				method.invoke(this, request, response, actionType);
			} catch (Exception e) {
				e.printStackTrace();
				out.println("-1");
				return;
			}
		}
	}

	private List<String> getCategories(List<HashMap<String, String>> rows, HttpServletRequest request)
			throws Exception {
		LIService service = new LIService();
//		String query = service.getQuery("mg-motors-ssi-sample-found");
//		query = service.getQureyRebuild("mg-motors-ssi-sample-found", query, request);
//		LIDAO dao = new LIDAO();
//		System.out.println("QUERY :::::::::::::::: " + query);
//		String countValue = dao.loadString(query, "total_count");
//		int count = countValue == null ? null : Integer.parseInt(countValue);
		List<String> Categories = rows.stream().filter(map -> map.containsKey("categories")).map(map -> {
			String cat = map.get("categories");
			return cat;
		}).collect(Collectors.toList());
		System.out.println("Categories ::::::::: " + Categories);
		return Categories;
	}

	public void getCSIbyModel(HttpServletRequest request, HttpServletResponse response, String methodName)
			throws Exception {
		LIDAO dao = new LIDAO();
		LIService service = new LIService();
		String query = service.getQureyRebuild(request);
		String belowStudyAverage = request.getParameter("belowStudyAverage");
		String[] columns = { "categories", "score", "sortindex" };
		ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
		String[] seriesName = { "score" };
		ArrayList<String> seriesNames = new ArrayList<String>(Arrays.asList(seriesName));
		LinkedHashMap columnInfo = new LinkedHashMap();
		columnList.stream().forEach(x -> columnInfo.put(x, x));
		Map<String, LIWhereconditionmapRecord> filtersMap = service.getFiltersMap(query);
		Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMap, query);
		System.out.println(query);
		query = acknowledgmentMap.get("QUERY");
		System.out.print(query);
		List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
		Set dataCheck = rows.stream().filter(x -> x.get("score") != null).map(s -> s.get("score"))
				.collect(Collectors.toSet());
		if (dataCheck.size() <= 0) {
			response.setStatus(204);
			service.writeOutput(response, " ");
		}
		packingutils packingUitility = new packingutils();
		List<String> categories = getCategories(rows, request);
		List FactorScore = new LinkedList();
		if (belowStudyAverage != null && belowStudyAverage.equalsIgnoreCase("true")) {
			FactorScore = packingUitility.getColoredChagedScore(rows, "score", "study_average", request);
			if (FactorScore.size() <= 0) {
				response.setStatus(204);
				service.writeOutput(response, " ");
			}
		} else {
			FactorScore = rows.stream().collect(Collectors.mapping(map -> {
				return new DataClassWithImpact(packingutils.getConvertToInt(map.get("score")), null);
			}, Collectors.toList()));
		}
		
		Map cols = new HashMap();
		cols.put("score", FactorScore);
		cols.put("categories", categories);
		HashMap finalOutput = new HashMap();
		finalOutput.put("percentPrefix", "");
		finalOutput.put("data", cols);
		finalOutput.put("seriesName", seriesNames);
		finalOutput.put("seriesValue", seriesNames);

		HashMap output = new HashMap();
		output.put("data", finalOutput);
		service.writeOutput(response, output);
	}
	
	public void getModelTrend(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
		LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = service.getQureyRebuild(request);
        query = service.getQureyRebuild("dealers-region-marker",query,request);
        String[] columns = {"score","categories"};
        System.out.println("query checks *********** " + query);
        HashMap<String, String> columnInfo = new HashMap<>();
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        columnList.stream().forEach(x -> columnInfo.put(x, x));
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();
        List<String> categories = getCategories(rows);
        List FactorScore = packingutils.getScoresAsIntWithFilterValue(rows, "#6F7CEF", "score","filterValue");
        if (FactorScore.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        Map innerOutput = new HashMap();
        innerOutput.put("categories", categories);
        innerOutput.put("factor", FactorScore);
        Map output = new HashMap();
        output.put("data", innerOutput);
        Map outeOoutput = new HashMap();
        outeOoutput.put("data", output);
        ArrayList seriesName = new ArrayList();
        seriesName.add("factor");
        ArrayList seriesValue = new ArrayList();
        seriesValue.add("Factor Scores");
        
        output.put("seriesName", seriesName);
        output.put("seriesValue", seriesValue);
        outeOoutput.put("data", output);
        service.writeOutput(response, outeOoutput);
    }
	
	public void getFactorScoresByModel(HttpServletRequest request, HttpServletResponse response, String methodName)
			throws Exception {
		LIDAO dao = new LIDAO();
		LIService service = new LIService();

		// fetching data starts
		String query = service.getQureyRebuild(request);
		Map<String, LIWhereconditionmapRecord> filtersMapdelaer = service.getFiltersMap(query);
        Map<String, String> acknowledgmentMap = service.buildWherecondition(request, filtersMapdelaer, query);
        query=acknowledgmentMap.get("QUERY");
        System.out.println(query);
        
		//get headers - column names
		LinkedHashMap<String, String> columnList = dao.getColumnsFromResultSet(query);
		
		List<HashMap<String, String>> rowsData = new ArrayList<HashMap<String, String>>();
		rowsData = dao.loadValuesRowWise(query, columnList);
		
		List<HashMap<String, Object>> headers = new ArrayList<HashMap<String, Object>>();

		for (Map.Entry element : columnList.entrySet()) {
			LinkedHashMap<String, Object> header = new LinkedHashMap<String, Object>();

			String key = (String) element.getKey();
			String value = (String) element.getValue();
			
			Integer minWidth=(value=="Delivery Date")?10:100;
				
			header.put("field", value);
			header.put("maxWidth", null);
			header.put("minWidth", minWidth);
			header.put("sortable", true);
			header.put("headerName", value);
			header.put("tooltipField", value);
			headers.add(header);
		}
		
		
		List<HashMap<String, String>> tableData = new ArrayList<HashMap<String, String>>();
		rowsData.forEach(x -> {
			HashMap<String, String> values = new HashMap<String, String>();
			for (String key : x.keySet()) {
				String value = x.get(key);
				values.put(key, value);
			}
			tableData.add(values);
		});

		HashMap data = new HashMap();
		data.put("header", headers);
		if(rowsData.size()<=0){
			System.out.println(rowsData.size());
			List temp=new ArrayList();
			data.put("tableData", temp);
			service.writeOutput(response, data);
		}else{
			data.put("tableData", tableData);
			String stringData= URLEncoder.encode(new JSONObject(data).toString(), "UTF-8");
			service.writeOutput(response, stringData);
		}


	}

	private Boolean getConvertedToBoolean(String isgrowthIncremented) {
		if (isgrowthIncremented.equalsIgnoreCase("1")) {
			return true;
		} else {
			return false;
		}
	}

	private List<String> getCategories(List<HashMap<String, String>> rows) {
		List<String> Categories = rows.stream().filter(map -> map.containsKey("categories"))
				.map(map -> map.get("categories")).collect(Collectors.toList());
		System.out.println("Categories ::::::::: " + Categories);
		return Categories;
	}

}
