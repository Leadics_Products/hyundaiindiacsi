package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class AccordianTableData {
    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }
    public void getOemSummaryTableData(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
            String dealerTypeQuery="select Distinct dealer,dealerType from dealer_master";
            String headQuery="select Distinct category as columns_display from category_master order by id";
            String mainQuery="select a.growthPointColor,d.dealer,d.dealerType,c.region,b.kpi,a.score,a.growth,e.category,a.cal,a.underperforming,a.show_growth from \n" +
                    "(select * from dem_score_aggregate_monthly where month='Apr' and year='2019'  group by dealer,category ) as a\n" +
                    "left join\n" +
                    "kpi_master  as b on a.id=b.kpi\n" +
                    "left join\n" +
                    "region_master as c on a.region=c.id\n" +
                    "left join\n" +
                    "dealer_master as d on d.id=a.dealer\n" +
                    "left join\n" +
                    "category_master as e on e.id=a.category";
        System.out.println(mainQuery);
            ArrayList rowsHead = dao.loadColumnList(headQuery, "columns_display", false);
            ArrayList<String> columnListDealer = new ArrayList<String>(Arrays.asList("dealer", "score", "growth", "category", "cal", "underperforming", "growthPointColor","dealerType"));
            HashMap<String, String> columnInfoDealer = new HashMap<>();
            columnListDealer.stream()
                .forEach(x -> columnInfoDealer.put(x, x));
            List<HashMap<String, String>> rowsDealer = dao.loadValuesRowWise(mainQuery, columnInfoDealer);
            Map parent = rowsDealer.stream().collect(groupingBy(rowDealer -> rowDealer.get("dealer"),
                groupingBy(rowKpi -> rowKpi.get("category"), LinkedHashMap::new,
                        Collectors.mapping(rowScore -> new OemScoresPack(rowScore.get("score"), rowScore.get("cal"), rowScore.get("underperforming"), rowScore.get("growthPointColor")),
                                Collectors.toList()))));

        Map dealertype= dao.loadMap(dealerTypeQuery,"dealer","dealerType",false);

        Set parentKeySet = parent.keySet();
        List listparentKeySet = new ArrayList(parentKeySet);
        Collections.sort(listparentKeySet);
        List finalArray = new ArrayList();
        listparentKeySet.stream().forEach(x -> {
            LinkedHashMap parentHashMap = (LinkedHashMap) (HashMap) parent.get(x);
            parentHashMap.put("dealer", x);
            parentHashMap.put("dealerType",dealertype.get(x));
            parentHashMap.put("navigatePath","dealer-summary");
            finalArray.add(parentHashMap);
                });

        Map output = new HashMap();
        rowsHead.add(0,"dealer");
        output.put("data", finalArray);
        output.put("head", rowsHead);
        System.out.println(LocalTime.now());
        service.writeOutput(response, output);

    }

    public  void  getDealerDEMSummaryData(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String headQuery="select Distinct quarter as columns_display from quarter_master order by id";
        String queryCategory = "select d.dealer,a.quarter,c.region,b.kpi,a.score,a.growth,e.category,a.cal,a.underperforming,a.show_growth from \n" +
                "(select * from dem_score_aggregate_monthly  group by quarter,category ) as a\n" +
                "left join\n" +
                "kpi_master  as b on a.kpi=b.id\n" +
                "left join\n" +
                "region_master as c on a.region=c.id\n" +
                "left join\n" +
                "dealer_master as d on d.id=a.dealer\n" +
                "left join\n" +
                "category_master as e on e.id=a.category";
        String queryKpi = "select d.dealer,a.quarter,c.region,b.kpi,a.score,a.growth,e.category,a.cal,a.underperforming,a.show_growth from \n" +
                "(select * from dem_score_aggregate_monthly   group by quarter,kpi) as a\n" +
                "left join\n" +
                "kpi_master  as b on a.kpi=b.id\n" +
                "left join\n" +
                "region_master as c on a.region=c.id\n" +
                "left join\n" +
                "dealer_master as d on d.id=a.dealer\n" +
                "left join\n" +
                "category_master as e on e.id=a.category";
        System.out.println(queryCategory);
        System.out.println(queryKpi);
        ArrayList<String> columnListCategory = new ArrayList<String>(Arrays.asList("category", "score", "growth", "kpi", "cal", "underperforming", "show_growth","quarter"));
        ArrayList<String> columnListKpi = new ArrayList<String>(Arrays.asList("category", "score", "growth", "kpi", "cal", "underperforming", "show_growth","quarter"));
        HashMap<String, String> columnInfoCategory = new HashMap<>();
        columnListCategory.stream()
                .forEach(x -> columnInfoCategory.put(x, x));
        HashMap<String, String> columnInfoKpi = new HashMap<>();
        columnListKpi.stream()
                .forEach(x -> columnInfoKpi.put(x, x));
        List<HashMap<String, String>> rowsDealer = dao.loadValuesRowWise(queryCategory, columnInfoCategory);

        List<HashMap<String, String>> rowsWorkshop = dao.loadValuesRowWise(queryKpi, columnInfoKpi);

        Map dealers = rowsWorkshop.stream().collect(groupingBy(row -> row.get("category"), LinkedHashMap::new,
                groupingBy(rowGroup -> rowGroup.get("kpi"), LinkedHashMap::new,
                        groupingBy(rowKpi -> rowKpi.get("quarter"), LinkedHashMap::new,
                                Collectors.mapping(rowScore -> new Scores(rowScore.get("score"), rowScore.get("growth"), rowScore.get("cal"), rowScore.get("underperforming"), rowScore.get("show_growth")),
                                        Collectors.toList())))));

        Map parent = rowsDealer.stream().collect(groupingBy(row -> row.get("category"),
                groupingBy(rowKpi -> rowKpi.get("quarter"), LinkedHashMap::new,
                        Collectors.mapping(rowScore -> new Scores(rowScore.get("score"), rowScore.get("growth"), rowScore.get("cal"), rowScore.get("underperforming"), rowScore.get("show_growth")),
                                Collectors.toList()))));

        List finalArray = new ArrayList();
        Set parentKeySet = parent.keySet();
        List listparentKeySet = new ArrayList(parentKeySet);
        Collections.sort(listparentKeySet);

        listparentKeySet.stream().forEach(x -> {
            LinkedHashMap parentHashMap = (LinkedHashMap) (HashMap) parent.get(x);
            HashMap childHashMap = (HashMap) dealers.get(x);
            parentHashMap.put("dealer", x);
            parentHashMap.put("hasChild", true);
            List childernList = new ArrayList();
            Set childKeySet = childHashMap.keySet();
            childKeySet.stream().forEach(y -> {
                HashMap childern = (HashMap) childHashMap.get(y);
                childern.put("dealer", y);
                childern.put("dealerName", x);
                childernList.add(childern);
                parentHashMap.put("children", childernList);
            });
            finalArray.add(parentHashMap);
        });

        ArrayList<String> columnListHead = new ArrayList<String>(Arrays.asList("columns_display"));
        HashMap<String, String> columnInfoHead = new HashMap<>();
        columnListHead.stream()
                .forEach(x -> columnInfoCategory.put(x, x));
        ArrayList rowsHead = dao.loadColumnList(headQuery, "columns_display", false);


        rowsHead.add(0, "dealer");
        Map output = new HashMap();
        output.put("data", finalArray);
        output.put("head", rowsHead);

        service.writeOutput(response, output);
    }

    public static class OemScoresPack {
        String score;
        String cal;
        String underperforming;
        String growthPointColor;
        public OemScoresPack(String score,  String cal, String underperforming, String growthPointColor) {
            this.score=score;
            this.cal=cal;
            this.underperforming=underperforming;
            this.growthPointColor=growthPointColor;
        }

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }

        public String getCal() {
            return cal;
        }

        public void setCal(String cal) {
            this.cal = cal;
        }

        public String getUnderperforming() {
            return underperforming;
        }

        public void setUnderperforming(String underperforming) {
            this.underperforming = underperforming;
        }

        public String getGrowthPointColor() {
            return growthPointColor;
        }

        public void setGrowthPointColor(String growthPointColor) {
            this.growthPointColor = growthPointColor;
        }
    }
}
class ScoresClass {

    String score;
    String growth;
    String symbol;
    String underperforming;
    String showGrowth;
    String dealer;

    public ScoresClass(String score, String dealer,String growth, String symbol, String underperforming, String showGrowth) {
        this.score = score;
        this.growth = growth;
        this.symbol = symbol;
        this.underperforming = underperforming;
        this.showGrowth = showGrowth;
        this.dealer=dealer;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getGrowth() {
        return growth;
    }

    public void setGrowth(String growth) {
        this.growth = growth;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getUnderperforming() {
        return underperforming;
    }

    public void setUnderperforming(String underperforming) {
        this.underperforming = underperforming;
    }

    public String getShowGrowth() {
        return showGrowth;
    }

    public void setShowGrowth(String showGrowth) {
        this.showGrowth = showGrowth;
    }

}
