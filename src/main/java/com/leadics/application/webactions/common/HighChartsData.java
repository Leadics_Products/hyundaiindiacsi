package com.leadics.application.webactions.common;

public class HighChartsData {
    double y;
    String color;
    public HighChartsData(String score, String s) {
        this.y=Double.parseDouble(score);
        this.color=s;
    }

    public double getScore() {
        return y;
    }

    public void setScore(double score) {
        this.y = score;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
