package com.leadics.application.webactions.common;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class Summary {
    public static final String GREY_COLOR = "#4CB341";
    public static final String BLACK_COLOR = "#000000";
    public static final String BLUE_COLOR = "#087894";
    public static final String AMBER_COLOR = "#FEAD5C";
    public static final String RED_COLOR = "#E10000";
    public static final String GREEN_COLOR = "#4CB341";
    public static final String BLUE_COLOR_BENCHMARK = "#087894";
    public static final String RED_COLOR_BENCHMARK = "#E10000";
    public static final String GREEN_COLOR_BENCHMARK = "#4CB341";
    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                Method method = this.getClass()
                        .getMethod(actionType, HttpServletRequest.class, HttpServletResponse.class, String.class);
                method.invoke(this, request, response, actionType);
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }
    }
    public void getSummaryFactorScores(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = service.getQureyRebuild(request);
        query = service.getQureyRebuild("dealers-region-marker",query,request);
        String upperBenchmark = request.getParameter("upper_benchmark");
        String lowerBenchmark = request.getParameter("lower_benchmark");
        String[] columns = {"score", "maxscore", "minscore", "categories"};
        System.out.println("query checks *********** " + query);
        HashMap<String, String> columnInfo = new HashMap<>();
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        columnList.stream().forEach(x -> columnInfo.put(x, x));
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();
        List<String> categories = getCategories(rows);
        List FactorScore = packingutils.getScoresAsIntWithFilterValue(rows, "#6F7CEF", "score","filterValue");
        if (FactorScore.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        List Maxscore = packingutils.getScoresAsInt(rows, GREEN_COLOR_BENCHMARK, "maxscore");
        String colorAverage;
        if (lowerBenchmark.contains("Average")) {
            colorAverage = BLUE_COLOR_BENCHMARK;
        } else {
            colorAverage = RED_COLOR_BENCHMARK;
        }
        List MinScore = packingutils.getScoresAsInt(rows, colorAverage, "minscore");
        Map innerOutput = new HashMap();
        innerOutput.put("categories", categories);
        innerOutput.put("factor", FactorScore);
        innerOutput.put("maxscore", Maxscore);
        innerOutput.put("minscore", MinScore);
        Map output = new HashMap();
        output.put("data", innerOutput);
        Map outeOoutput = new HashMap();
        outeOoutput.put("data", output);
        ArrayList seriesName = new ArrayList();
        seriesName.add("factor");
        ArrayList seriesValue = new ArrayList();
        seriesValue.add("Factor Scores");
        if ((upperBenchmark != null) && !upperBenchmark.contains("None")) {
            seriesValue.add(upperBenchmark);
        }

        if ((lowerBenchmark != null) && !lowerBenchmark.contains("None")) {
            seriesValue.add(lowerBenchmark);
        }
        Boolean checkUpperStudyBench = (upperBenchmark != null) && upperBenchmark.contains("Best") ? seriesName.add("maxscore") : false;
        Boolean checkLowerStudyBench = (lowerBenchmark != null) && (lowerBenchmark.contains("Worst") || lowerBenchmark.contains("Average")) ? seriesName.add("minscore") : false;
        output.put("seriesName", seriesName);
        output.put("seriesValue", seriesValue);
        outeOoutput.put("data", output);
        service.writeOutput(response, outeOoutput);

    }

    public void getSummaryTop5Bottom5(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = service.getQureyRebuild(request);

        String[] columns = {"categories", "score", "samplecount"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("score", "score");
        columnInfo.put("categories", "categories");
        columnInfo.put("samplecount", "samplecount");
        System.out.println(query);
        packingutils packingutils=new packingutils();
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        List<String> categories = getCategories(rows);
        List<DataClassWithBadges> innerList = rows
                .stream()
                .collect(Collectors.mapping(map -> {
                    return new DataClassWithBadges(packingutils.getConvertToInt(map.get("score")), map.get("categories"), null, null, null,null,null);
                }, Collectors.toList()));


        if (innerList.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        HashMap innerOutput=new HashMap();
        ArrayList seriesName = new ArrayList();
        seriesName.add("score");
        ArrayList seriesValue = new ArrayList();
        seriesValue.add("Dealer Scores");
        innerOutput.put("categories",categories);
        innerOutput.put("score",innerList);
        Map data=new HashMap();
        data.put("data",innerOutput);
        data.put("seriesName",seriesName);
        data.put("seriesValue",seriesValue);
        HashMap datas=new HashMap();
        datas.put("data",data);
        service.writeOutput(response, datas);

    }
    
    public void getSummaryTop5Bottom5AttributeMean(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = service.getQureyRebuild(request);

        String[] columns = {"categories", "score", "samplecount"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("score", "score");
        columnInfo.put("categories", "categories");
        columnInfo.put("samplecount", "samplecount");
        System.out.println(query);
        packingutils packingutils=new packingutils();
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        List<String> categories = getCategories(rows);
        List<DataClassWithBadgesInDouble> innerList = rows
                .stream()
                .collect(Collectors.mapping(map -> {
                    return new DataClassWithBadgesInDouble(packingutils.getConvertToDouble(map.get("score")), map.get("categories"), null, null, null,null,null);
                }, Collectors.toList()));


        if (innerList.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        HashMap innerOutput=new HashMap();
        ArrayList seriesName = new ArrayList();
        seriesName.add("score");
        ArrayList seriesValue = new ArrayList();
        seriesValue.add("Dealer Scores");
        innerOutput.put("categories",categories);
        innerOutput.put("score",innerList);
        Map data=new HashMap();
        data.put("data",innerOutput);
        data.put("seriesName",seriesName);
        data.put("seriesValue",seriesValue);
        HashMap datas=new HashMap();
        datas.put("data",data);
        service.writeOutput(response, datas);

    }
    
    public void getSummaryNationalIndexScore(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        System.out.println("index QUERY :::::::"+query);
        String[] columns = {"score", "wave"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnList.forEach(y->{
            columnInfo.put(y,y);
        });
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        List<HashMap> innerRows=rows.stream().map(x-> getDataPackNationaScore(x.get("score"),x.get("wave"))).collect(Collectors.toList());
        Map data=new HashMap();
        data.putAll(innerRows.get(0));
        data.put("chartData",innerRows);
        service.writeOutput(response,data );
    }

    private Boolean getConvertedToBoolean(String isgrowthIncremented) {
        if(isgrowthIncremented.equalsIgnoreCase("1")){
            return true;
        }else{
            return false;
        }
    }

    private HashMap getTrendForNationalCard(HttpServletRequest request) throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQuery("National-chart-trend");
        query=service.getQureyRebuild("National-chart-trend",query,request);
        String[] columns = {"score", "categories","color"};
        System.out.println("trend query");
        System.out.println(query);
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnList.forEach(x->{
            columnInfo.put(x,x);
        });
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        packingutils packingutils=new packingutils();

        Set categories=packingutils.getCategories(rows);
        List<DataClass> innerData = rows
                .stream()
                .map(s -> new DataClass(s.get("score"),s.get("categories"),s.get("color")))
                .collect(Collectors.toList());
        HashMap data=new HashMap();
        data.put("categories",categories);
        
        data.put("score",innerData.size()>1?innerData:Collections.EMPTY_LIST);
        HashMap innerDatas=new HashMap();
        innerDatas.put("data",data);
        String[] seriesName={"score"};
        ArrayList<String> seriesNames = new ArrayList<String>(Arrays.asList(seriesName));
        innerDatas.put("seriesName",seriesName);
        innerDatas.put("seriesValue",seriesName);

        Set dataCheck=rows
                .stream()
                .filter(x->x.get("score")!=null)
                .map(s->s.get("score")).collect(Collectors.toSet());
        if(categories.size() >1 && dataCheck.size()>1 ){
            return innerDatas;
        }else {
            return new HashMap();
        }


    }

    private List<String> getCategories(List<HashMap<String, String>> rows) {
        List<String> Categories = rows.stream()
                .filter(map -> map.containsKey("categories"))
                .map(map -> map.get("categories"))
                .collect(Collectors.toList());
        System.out.println("Categories ::::::::: "+Categories);
        return Categories;
    }
    public void getSummaryRegionWiseIndexScore(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        String[] columns = {"title", "latitude", "longitude", "value","color","regionId"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));

        HashMap<String, String> columnInfo = new HashMap<>();
        columnList.forEach(x->{
            columnInfo.put(x,x);
        });
        System.out.println("QUERY :::::::::::::::: "+query);
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);

        Map outPut =new HashMap();
        outPut.put("data",rows);

        if(rows.size()<=0){
            response.setStatus(204);
            service.writeOutput(response, " ");
        }else{
            service.writeOutput(response, outPut);
        }

    }
   public  void getSummaryLoyalityData(HttpServletRequest request, HttpServletResponse response, String methodName)
           throws Exception {
       LIDAO dao = new LIDAO();
       LIService service = new LIService();
       String query=service.getQureyRebuild(request);
       System.out.println("QUERY ::::::::"+query);
       String[] columns = {"name", "score","color"};
       ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
       HashMap<String, String> columnInfo = new HashMap<>();
       columnList.forEach(x->{
           columnInfo.put(x,x);
       });
       List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
       packingutils packingutils=new packingutils();
       List<DataClass> innerData = rows
               .stream()
               .map(s -> new DataClass(s.get("score"),s.get("name"),s.get("color")))
               .collect(Collectors.toList());
       Set dataCheck=rows
               .stream()
               .filter(x->x.get("score")!=null)
               .map(s->s.get("score")).collect(Collectors.toSet());
       if (dataCheck.size() <= 0) {
           response.setStatus(204);
           service.writeOutput(response, " ");
       }
       Map output = new HashMap();
       output.put("dataset", innerData);
       service.writeOutput(response, output);
   }
    public void getSummaryOverallSatisfaction(HttpServletRequest request, HttpServletResponse response, String methodName)
            throws Exception {
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query=service.getQureyRebuild(request);
        System.out.println("QUERY ::::::::"+query);
        String[] columns = {"name", "value","color"};
        ArrayList<String> columnList = new ArrayList<String>(Arrays.asList(columns));
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("name", "name");
        columnInfo.put("value", "value");
        columnInfo.put("color", "color");
        List<HashMap<String, String>> rows = dao.loadValuesRowWise(query, columnInfo);
        Set dataCheck=rows
                .stream()
                .filter(x->x.get("value")!=null)
                .map(s->s.get("value")).collect(Collectors.toSet());
        if (dataCheck.size() <= 0) {
            response.setStatus(204);
            service.writeOutput(response, " ");
        }
        packingutils packingutils=new packingutils();
        List<DataClass> innerData = rows
                .stream()
                .filter(f -> !f.get("name").equalsIgnoreCase("Total Count"))
                .map(s -> packingutils.getDataObjet(s.get("value"), s.get("name"), s.get("color")))
                .collect(Collectors.toList());
        List<String> innerDatacount = rows
                .stream()
                .filter(f -> f.get("name").equalsIgnoreCase("Total Count"))
                .map(s -> s.get("value"))
                .collect(Collectors.toList());


        Map output = new HashMap();
        output.put("dataset", innerData);
        output.put("Total Count", innerDatacount.get(0));

        service.writeOutput(response, output);


    }
    private HashMap getDataPackNationaScore(String score, String wave){
        HashMap temp=new HashMap();
        temp.put("score",score);
        temp.put("wave",wave);
        return temp;
    }



}
class ClassAddColor {

    String id;
    String state;
    String title;
    String regionId;
    String value;
    String diffrence;
    String color;

    public ClassAddColor(String id, String state, String title, String regionId, String value, String diffrence, String color) {
        this.id = id;
        this.state = state;
        this.title = title;
        this.regionId = regionId;
        this.value = value;
        this.diffrence = diffrence;
        this.color = color;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setDiffrence(String diffrence) {
        this.diffrence = diffrence;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
class DataClass {
    Integer y;
    String name;
    String color;
    public DataClass(String value) {
        this.y= Integer.valueOf(value);
    }
    public DataClass(String value,String name,String color){
        Integer temp;
        try {
             temp = value == null ? null : Integer.valueOf(value);
        }catch (NumberFormatException ne){
            double d = value == null ?null:Double.parseDouble(value);
            temp = (int) d;
        }
        this.y= temp;
        this.name=name;
        this.color=color;
    }
    public DataClass(String value,String color){
        Integer temp=value==null?null:Integer.valueOf(value);
        this.y= temp;
        this.color=color;
        

    }
}
class DataClassWithBadges {
    private Integer y;
    private String badgeScore;
    private String badgePrefix;
    private String badgeColor;
    private String color;
    private String badgeToolTip;
    private String filterValue;

    public DataClassWithBadges(Integer score, String badgeColor, String badgeScore, String badgePrefix, String color,String badgeToolTip,String filterValue) {
        this.y = score;
        this.color = color;
        this.badgeScore = badgeScore;
        this.badgePrefix = badgePrefix;
        this.badgeColor = badgeColor;
        this.badgeToolTip=badgeToolTip;
        this.filterValue=filterValue;
    }
}

class DataClassWithBadgesInDouble {
    private Double y;
    private String badgeScore;
    private String badgePrefix;
    private String badgeColor;
    private String color;
    private String badgeToolTip;
    private String filterValue;

    public DataClassWithBadgesInDouble(Double score, String badgeColor, String badgeScore, String badgePrefix, String color,String badgeToolTip,String filterValue) {
        this.y = score;
        this.color = color;
        this.badgeScore = badgeScore;
        this.badgePrefix = badgePrefix;
        this.badgeColor = badgeColor;
        this.badgeToolTip=badgeToolTip;
        this.filterValue=filterValue;
    }
}
class ChartData {
    private  String name;
    private List<DataClass> data;
    public ChartData(String key, List<DataClass> value) {
        this.name=key;
        this.data=value;

    }
}
