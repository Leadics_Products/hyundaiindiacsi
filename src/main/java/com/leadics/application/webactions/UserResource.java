/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.application.webactions;

import com.leadics.application.common.LIService;
//import com.leadics.application.controller.LISystemuserController;
//to be removed
//import com.leadics.application.representation.MenuRepresentation;
import com.leadics.application.representation.MenuRepresentation;
import com.leadics.application.service.*;
import com.leadics.application.to.*;
import com.leadics.utils.DtoMapper;
//import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import com.leap.core.auth.idam.adapters.KeycloakUser;
import com.leap.core.auth.idam.adapters.KeycloakUserFactory;
import com.leap.core.auth.idam.adapters.UserContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.leadics.application.common.LIAction.AUTHORIZATION;
import com.leadics.application.common.LIDAO;
import static com.leadics.application.common.LIService.ActionPlanData;
import com.leadics.utils.ActionPlanning;
import com.leadics.utils.DivMapper;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.Collectors.groupingBy;

/**
 *
 * @author HARSHA
 */
public class UserResource {

    public static String TODO_REMOVE_ROLE_ID_GET_FROM_LEAP = "1";
    public static String USER = "user";
    public static LIConfigdivRecord[] listLIConfigdivRecord;

    static {
        String query = "select * from pre_action_plan_data ";
        System.out.println("config div");
        System.out.println(query);
        LIDAO dao = new LIDAO();
        HashMap<String, String> columnInfo = new HashMap<>();
        columnInfo.put("area", "area");
        columnInfo.put("kpi", "kpi");
        columnInfo.put("category", "category");
        columnInfo.put("slugname", "slugname");
        try {
            ActionPlanData = dao.loadValuesRowWise(query, columnInfo);

        } catch (SQLException ex) {
            Logger.getLogger(LIService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {

        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {

                if (!StringUtils.isNullOrEmpty(actionType)) {

                    Method method = this.getClass().getMethod(actionType, HttpServletRequest.class,
                            HttpServletResponse.class);
                    method.invoke(this, request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
                response.setStatus(500);
                out.println("-1");
                return;
            }
        }

    }

    public void getPages(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIService service = new LIService();
        String authHeader = request.getHeader(AUTHORIZATION);
        UserContext userContext = (UserContext) request.getAttribute(USER);
        List<Integer> roles = userContext.getRoles();
        Integer roleId = roles.get(0);
        System.out.println("role id");
        System.out.println(roleId.toString());
        List<MenuRepresentation> menus = this.getPages(roleId.toString());
        service.writeOutput(response, menus);

    }

    public void getFilters(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIService service = new LIService();
        String authHeader = request.getHeader(AUTHORIZATION);
        UserContext userContext = (UserContext) request.getAttribute(USER);
        List<Integer> roles = userContext.getRoles();
        String roleId = "" + roles.get(0);
        List<FilterTemplate> filters = this.getFilters(roleId);
        service.writeOutput(response, filters);
    }

    public void getFiltersConfig(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIService service = new LIService();
        UserContext userContext = (UserContext) request.getAttribute(USER);
        List<Integer> roles = userContext.getRoles();
        String roleId = "" + roles.get(0);
        LIConfigfiltersRecord[] filtersConfig = this.getFiltersConfig(roleId);
        service.writeOutput(response, filtersConfig);
    }

    public void getDivConfig(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIService service = new LIService();
        UserContext userContext = (UserContext) request.getAttribute(USER);
        List<Integer> roles = userContext.getRoles();
        String roleId = "" + roles.get(0);
        LIConfigdivRecord[] divConfig = this.getDivConfig(roleId);
        List list = new LinkedList();
        for (LIConfigdivRecord div : divConfig) {
            if (div.getSub()) {
                list.add(div);
            }

        }
         System.out.println(list);
        service.writeOutput(response, list);

    }

    public void getChartsConfig(HttpServletRequest request, HttpServletResponse response) throws Exception {
        LIService service = new LIService();


        Set<ChartsConfig> SubDivs = Stream.of(listLIConfigdivRecord).filter(y -> y.getSub())
                .map(DivMapper::toLIConfigdivRecord)
                .collect(Collectors.toSet());

        Map<Integer, List<ChartsConfig>> subDivmap = Stream.of(listLIConfigdivRecord)
                .filter(record -> !record.getSub())
                .map(DivMapper::toLIConfigdivRecord)
                .collect(groupingBy(ChartsConfig::getParentId));

        
              SubDivs = SubDivs.stream()
                .map(div -> {
                    div.setSub(subDivmap.get(div.getId()));
                    return div;
                }).collect(Collectors.toSet());




       Set<ChartsConfig> mainData = Stream.of(listLIConfigdivRecord).filter(y -> !y.getSub())
                .map(DivMapper::toLIConfigdivRecord)
                .collect(Collectors.toSet());
        SubDivs.addAll(mainData);
        Map<String, List<ChartsConfig>> maindataDiv=SubDivs.stream().collect(Collectors.groupingBy(ChartsConfig::getPageName));
        System.out.print(maindataDiv);
          service.writeOutput(response, maindataDiv);


    }


    private List<MenuRepresentation> getPages(String identity) throws Exception {

        String menusQuery = "SELECT A.* FROM config_pages AS A \n" + "INNER JOIN \n"
                + "CONFIG_MODULES B ON A.MODULE_NAME = B.MODULE_NAME\n" + "INNER JOIN \n"
                + "USER_ROLE C ON B.ROLE_NAME = C.ROLENAME AND C.ID = " + identity + " ORDER BY A.rank;";

        LIConfigpagesService pagesService = new LIConfigpagesService();

        LinkedHashMap<Integer, LinkedList<MenuRepresentation>> menuMap = new LinkedHashMap<>();
        LIConfigpagesRecord[] records = pagesService.loadLIConfigpagesRecords(menusQuery);

        List<MenuRepresentation> menusWithSubMenus = Stream.of(records).filter(record -> record.getSub())
                .map(DtoMapper::toMenuRepresentation).collect(Collectors.toList());

        Map<String, List<MenuRepresentation>> subMenumap = Stream.of(records).filter(record -> !record.getSub())
                .map(DtoMapper::toMenuRepresentation).collect(Collectors.groupingBy(MenuRepresentation::getParentId));

        menusWithSubMenus = menusWithSubMenus.stream().map(menu -> {
            menu.setSub(subMenumap.get(menu.getId()));
            return menu;
        }).collect(Collectors.toList());
        return menusWithSubMenus;
    }

    // get the filters with respect to page according to the user role id
    private List<FilterTemplate> getFilters(String id) throws Exception {

//        String filtersQuery = "(SELECT B.SLUG_NAME as PAGE_NAME,FILTER_NAME,SLUG_FILTER_NAME,FILTER_TYPE,FILTER_PLACEHODLER,FILTER_PRIORITY,\n"
//                + "    PARENTS,CHILDS,DEFAULT_INDEX,CONTAINS_ALL,HIDE_WHEN,PERSIST_AFTER_ROUTE_CHANGE,CONTAINS_NONE,CONTAINS_FILTER_GROUP\n"
//                + "    FROM CONFIG_PAGE_FILTER AS A \n" + "    INNER JOIN CONFIG_PAGES AS B \n"
//                + "    ON A.PAGE_NAME = B.SLUG_NAME\n" + "    INNER JOIN CONFIG_MODULES AS C \n"
//                + "    ON C.MODULE_NAME = B.MODULE_NAME WHERE C.ROLE_NAME = (SELECT ROLENAME FROM USER_ROLE WHERE ID = "
//                + id + "  ))"
//                + "    UNION  SELECT B.SLUG_NAME as PAGE_NAME,FILTER_NAME,SLUG_FILTER_NAME,FILTER_TYPE,FILTER_PLACEHODLER,FILTER_PRIORITY,\n"
//                + "    PARENTS,CHILDS,DEFAULT_INDEX,CONTAINS_ALL,HIDE_WHEN,PERSIST_AFTER_ROUTE_CHANGE,CONTAINS_NONE,CONTAINS_FILTER_GROUP\n"
//                + "    FROM CONFIG_PAGE_FILTER AS A \n" + "    INNER JOIN CONFIG_PAGES AS B \n"
//                + "    ON A.PAGE_NAME = B.SLUG_NAME\n" + "    INNER JOIN CONFIG_MODULES AS C \n"
//                + "    ON C.MODULE_NAME = B.MODULE_NAME ;";
        String filtersQuery="SELECT * FROM config_page_filter;";
        System.out.println("Filtes List get Query");
        System.out.println(filtersQuery);
        LIConfigpagefilterService filtersService = new LIConfigpagefilterService();

        LIConfigpagefilterRecord[] records = filtersService.loadLIConfigpagefilterRecords(filtersQuery);
        LIService service = new LIService();
        List<FilterTemplate> filterTemplate = new LinkedList<>();

        HashMap<String, List<FilterProperties>> tempTemplate = new HashMap<>();
        List<FilterProperties> li;
        if (records != null) {
            for (LIConfigpagefilterRecord record : records) {
                String pageName = record.getPagename();
                String filterName = record.getFiltername();
                String filterType = record.getFiltertype();
                String placeholder = record.getFilterplacehodler();
                String hideWhen = record.getHidewhen();
                double priority = record.getFilterpriority();
                String parents = record.getParents();
                String childs = record.getChilds();
                int defaultIndex = record.getDefaultindex();
                boolean containsAll = record.getContainsall();
                boolean containsNone = record.getContainsnone();
                boolean containsFilterGroup = record.getContainsfiltergroup();
                boolean persistAfterRouteChange = record.getPersistAfterRouteChange();
                FilterProperties fp = new FilterProperties();
                fp.setName(filterName);
                fp.setType(filterType);
                fp.setPlaceholder(placeholder);
                fp.setPriority(priority);
                fp.setParents(service.convertCommaBasedToArray(parents));
                fp.setChilds(service.convertCommaBasedToArray(childs));
                fp.setDefaultIndex(defaultIndex);
                fp.setContainsAll(containsAll);
                fp.setContainsNone(containsNone);
                fp.setHideWhen(hideWhen);
                fp.setPersistAfterRouteChange(persistAfterRouteChange);

                li = tempTemplate.get(pageName);
                if (li == null) {
                    li = new LinkedList<>();
                }
                li.add(fp);
                tempTemplate.put(pageName, li);

            }
        }

        Set<String> pageNames = tempTemplate.keySet();

        Iterator setIterator = pageNames.iterator();

        while (setIterator.hasNext()) {
            String pageName = setIterator.next().toString();

            FilterTemplate ft = new FilterTemplate();
            ft.setPageName(pageName);
            ft.setFilterInfo(tempTemplate.get(pageName));
            filterTemplate.add(ft);
        }

        return filterTemplate;
    }

    private LIConfigfiltersRecord[] getFiltersConfig(String id) throws Exception {
        LIConfigfiltersRecord records[];

        LIConfigfiltersService service = new LIConfigfiltersService();

        String query = "SELECT A.* FROM CONFIG_FILTERS A \n" + "INNER JOIN CONFIG_MODULES B \n"
                + "ON A.MODULE_NAME = B.MODULE_NAME\n" + "INNER JOIN user_role C \n"
                + "ON C.ROLENAME = B.ROLE_NAME AND C.ID = " + id + "  ";

        records = service.loadLIConfigfiltersRecords(query);

        return records;
    }
    // get the div config according to the user role id

    private LIConfigdivRecord[] getDivConfig(String id) throws Exception {

        String query = "select * from config_div A \n"
                + "INNER JOIN \n"
                + "CONFIG_MODULES B ON A.MODULE_NAME = B.MODULE_NAME\n"
                + "INNER JOIN \n"
                + "USER_ROLE C ON B.ROLE_NAME = C.ROLENAME AND C.ID = " + id + ";";
//         added only for hyindai to use same for all logins
//        String query = "select * from config_div ";
        System.out.println("config div");
        System.out.println(query);
        LIConfigdivService divService = new LIConfigdivService();
        LIService service = new LIService();

        LIConfigdivRecord[] listLIConfigdivRecords = divService.loadLIConfigdivRecords(query);

        for (LIConfigdivRecord div : listLIConfigdivRecords) {

            if(div.getSub() && !div.getParentId().equalsIgnoreCase("0"))
            {
                continue;
            }
            ActionPlanning actPalan=getActionPlanData(div.getDivName());
            if(actPalan!=null){
                div.setActionPlanning(actPalan);
            }

        }
        listLIConfigdivRecord = listLIConfigdivRecords;
        if (listLIConfigdivRecord == null) {
            throw new Exception("configuration for divs are missing");
        }
        for (LIConfigdivRecord div : listLIConfigdivRecord) {

            div.filterList = service.convertCommaBasedToArray(div.getFilters());

        }

        return listLIConfigdivRecord;
    }

    private ActionPlanning getActionPlanData(String divName) {

        List<HashMap<String, String>> acData = ActionPlanData.stream().filter(row -> row.get("slugname").equalsIgnoreCase(divName)).map(x -> x).collect(Collectors.toList());
        if(acData.size()>0){
            String area = acData.get(0).get("area");
            String kpi = acData.get(0).get("kpi");
            String category = acData.get(0).get("category");
            NameValuePair areaPair = new NameValuePair(area, area);
            NameValuePair kpiPair = new NameValuePair(kpi, kpi);
            NameValuePair categoryPair = new NameValuePair(category, category);
            ActionPlanning actPalan = new ActionPlanning();
            actPalan.setArea(areaPair);
            actPalan.setCategory(categoryPair);
            actPalan.setKpi(kpiPair);

            return actPalan;
        }else {
            return null;
        }


    }

}
