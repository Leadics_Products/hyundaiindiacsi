package com.leadics.application.webactions;



public class DataClassWithBadges {
    private Integer y;
    private String badgeScore;
    private String badgePrefix;
    private String badgeColor;
    private String color;
    public DataClassWithBadges(Integer score, String badgeColor, String badgeScore, String badgePrefix, String color) {
        this.y=score;
        this.color=color;
        this.badgeScore=badgeScore;
        this.badgePrefix=badgePrefix;
        this.badgeColor=badgeColor;
    }
}
