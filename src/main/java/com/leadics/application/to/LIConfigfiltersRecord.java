
/*
 * LIConfigfiltersRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.to;

import com.leadics.application.common.LIRecord;

import com.leadics.utils.StringUtils;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class LIConfigfiltersRecord extends LIRecord
{

	private String id;
	private String moduleName;
	private String filterName;
	private String filterActionString;
	private String createdat;
	private String createdby;
	private String modifiedat;
	private String modifiedby;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getModuleName()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(moduleName);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(moduleName);
		}
		else
		{
			return moduleName;
		}
	}

	public String getFilterName()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(filterName);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(filterName);
		}
		else
		{
			return filterName;
		}
	}

	public String getFilterActionString()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(filterActionString);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(filterActionString);
		}
		else
		{
			return filterActionString;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setModuleName(String value)
	{
		moduleName = value;
	}

	public void setFilterName(String value)
	{
		filterName = value;
	}

	public void setFilterActionString(String value)
	{
		filterActionString = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nmodulename:" + moduleName +
				"\nfiltername:" + filterName +
				"\nfilteractionstring:" + filterActionString +
				"\ncreatedat:" + createdat +
				"\ncreatedby:" + createdby +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\n";
	}

	public void loadContent(LIConfigfiltersRecord inputRecord)
	{
		setId(inputRecord.getId());
		setModuleName(inputRecord.getModuleName());
		setFilterName(inputRecord.getFilterName());
		setFilterActionString(inputRecord.getFilterActionString());
		setCreatedat(inputRecord.getCreatedat());
		setCreatedby(inputRecord.getCreatedby());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
	}

	public void loadNonNullContent(LIConfigfiltersRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getModuleName(), inputRecord.getModuleName()))
		{
			setModuleName(StringUtils.noNull(inputRecord.getModuleName()));
		}
		if (StringUtils.hasChanged(getFilterName(), inputRecord.getFilterName()))
		{
			setFilterName(StringUtils.noNull(inputRecord.getFilterName()));
		}
		if (StringUtils.hasChanged(getFilterActionString(), inputRecord.getFilterActionString()))
		{
			setFilterActionString(StringUtils.noNull(inputRecord.getFilterActionString()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id", StringUtils.noNull(id));
		obj.put("modulename", StringUtils.noNull(moduleName));
		obj.put("filtername", StringUtils.noNull(filterName));
		obj.put("filteractionstring", StringUtils.noNull(filterActionString));
		obj.put("createdat", StringUtils.noNull(createdat));
		obj.put("createdby", StringUtils.noNull(createdby));
		obj.put("modifiedat", StringUtils.noNull(modifiedat));
		obj.put("modifiedby", StringUtils.noNull(modifiedby));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");
		moduleName = StringUtils.getValueFromJSONObject(obj, "modulename");
		filterName = StringUtils.getValueFromJSONObject(obj, "filtername");
		filterActionString = StringUtils.getValueFromJSONObject(obj, "filteractionstring");
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id", StringUtils.noNull(id));
		obj.put("module_name", StringUtils.noNull(moduleName));
		obj.put("filter_name", StringUtils.noNull(filterName));
		obj.put("filter_action_string", StringUtils.noNull(filterActionString));
		obj.put("created_at", StringUtils.noNull(createdat));
		obj.put("created_by", StringUtils.noNull(createdby));
		obj.put("modified_at", StringUtils.noNull(modifiedat));
		obj.put("modified_by", StringUtils.noNull(modifiedby));
		return obj;
	}

	public void log()
	{

	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "config_filters");

		columnList.add("id");				
		columnList.add("module_name");				
		columnList.add("filter_name");				
		columnList.add("filter_action_string");				
		columnList.add("created_at");				
		columnList.add("created_by");				
		columnList.add("modified_at");				
		columnList.add("modified_by");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}
