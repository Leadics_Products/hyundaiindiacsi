
/*
 * LIConfigdivRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.to;

import com.google.gson.JsonObject;
import com.leadics.application.common.LIRecord;
import com.leadics.utils.ActionPlanning;

import com.leadics.utils.StringUtils;
import org.json.JSONException;
import org.json.simple.JSONObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class LIConfigdivRecord extends LIRecord {

    private String id;
    private String moduleName;
    private String divName;
    private String actionString;
    private String filters;
    private String createdat;
    private String createdby;
    private String modifiedat;
    private String modifiedby;
    private String chartName;
    private Boolean sub;
    private String pageName;
    private JsonObject extraConfig;
    private JsonObject data;

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }
    private ActionPlanning actionPlanning;

    private ChartsConfig subMenu;

    public JsonObject getExtraConfig() {
        return extraConfig;
    }

    public void setExtraConfig(JsonObject extraConfig) {
        System.out.println(extraConfig);
        this.extraConfig = extraConfig;
    }

    public JsonObject getData() {
        return data;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }

    public ChartsConfig getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(ChartsConfig subMenu) {
        this.subMenu = subMenu;
    }

    public ActionPlanning getActionPlanning() {
        return actionPlanning;
    }

    public void setActionPlanning(ActionPlanning actionPlanning) {
        this.actionPlanning = actionPlanning;
    }



    public Boolean getSub() {
        return sub;
    }

    public void setSub(Boolean sub) {
        this.sub = sub;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    private String parentId;

    public String getChartName() {
        return chartName;
    }

    public void setChartName(String chartName) {
        this.chartName = chartName;
    }

    public String getChartType() {
        return chartType;
    }

    public void setChartType(String chartType) {
        this.chartType = chartType;
    }
    private String chartType;
    public List<String> filterList = new LinkedList<>();

    public String getId() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(id);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(id);
        } else {
            return id;
        }
    }

    public String getModuleName() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(moduleName);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(moduleName);
        } else {
            return moduleName;
        }
    }

    public String getDivName() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(divName);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(divName);
        } else {
            return divName;
        }
    }

    public String getActionString() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(actionString);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(actionString);
        } else {
            return actionString;
        }
    }

    public String getFilters() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(filters);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(filters);
        } else {
            return filters;
        }
    }

    public String getCreatedat() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(createdat);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(createdat);
        } else {
            return createdat;
        }
    }

    public String getCreatedby() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(createdby);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(createdby);
        } else {
            return createdby;
        }
    }

    public String getModifiedat() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(modifiedat);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(modifiedat);
        } else {
            return modifiedat;
        }
    }

    public String getModifiedby() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(modifiedby);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(modifiedby);
        } else {
            return modifiedby;
        }
    }

    public void setId(String value) {
        id = value;
    }

    public void setModuleName(String value) {
        moduleName = value;
    }

    public void setDivName(String value) {
        divName = value;
    }

    public void setActionString(String value) {
        actionString = value;
    }

    public void setFilters(String value) {
        filters = value;
    }

    public void setCreatedat(String value) {
        createdat = value;
    }

    public void setCreatedby(String value) {
        createdby = value;
    }

    public void setModifiedat(String value) {
        modifiedat = value;
    }

    public void setModifiedby(String value) {
        modifiedby = value;
    }

    public String toString() {
        return "\nid:" + id
                + "\nmodulename:" + moduleName
                + "\ndivname:" + divName
                + "\nactionstring:" + actionString
                + "\nfilters:" + filters
                + "\ncreatedat:" + createdat
                + "\ncreatedby:" + createdby
                + "\nmodifiedat:" + modifiedat
                + "\nmodifiedby:" + modifiedby
                + "\n";
    }

    public void loadContent(LIConfigdivRecord inputRecord) {
        setId(inputRecord.getId());
        setModuleName(inputRecord.getModuleName());
        setDivName(inputRecord.getDivName());
        setActionString(inputRecord.getActionString());
        setFilters(inputRecord.getFilters());
        setCreatedat(inputRecord.getCreatedat());
        setCreatedby(inputRecord.getCreatedby());
        setModifiedat(inputRecord.getModifiedat());
        setModifiedby(inputRecord.getModifiedby());
    }

    public void loadNonNullContent(LIConfigdivRecord inputRecord) {
        if (StringUtils.hasChanged(getId(), inputRecord.getId())) {
            setId(StringUtils.noNull(inputRecord.getId()));
        }
        if (StringUtils.hasChanged(getModuleName(), inputRecord.getModuleName())) {
            setModuleName(StringUtils.noNull(inputRecord.getModuleName()));
        }
        if (StringUtils.hasChanged(getDivName(), inputRecord.getDivName())) {
            setDivName(StringUtils.noNull(inputRecord.getDivName()));
        }
        if (StringUtils.hasChanged(getActionString(), inputRecord.getActionString())) {
            setActionString(StringUtils.noNull(inputRecord.getActionString()));
        }
        if (StringUtils.hasChanged(getFilters(), inputRecord.getFilters())) {
            setFilters(StringUtils.noNull(inputRecord.getFilters()));
        }
        if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat())) {
            setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
        }
        if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby())) {
            setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
        }
        if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat())) {
            setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
        }
        if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby())) {
            setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
        }
    }

    public JSONObject getJSONObject() throws JSONException {
        JSONObject obj = new JSONObject();

        obj.put("id", StringUtils.noNull(id));
        obj.put("modulename", StringUtils.noNull(moduleName));
        obj.put("divname", StringUtils.noNull(divName));
        obj.put("actionstring", StringUtils.noNull(actionString));
        obj.put("filters", StringUtils.noNull(filters));
        obj.put("createdat", StringUtils.noNull(createdat));
        obj.put("createdby", StringUtils.noNull(createdby));
        obj.put("modifiedat", StringUtils.noNull(modifiedat));
        obj.put("modifiedby", StringUtils.noNull(modifiedby));
        return obj;
    }

    public void loadJSONObject(org.json.simple.JSONObject obj)
            throws Exception {
        if (obj == null) {
            return;
        }

        id = StringUtils.getValueFromJSONObject(obj, "id");
        moduleName = StringUtils.getValueFromJSONObject(obj, "modulename");
        divName = StringUtils.getValueFromJSONObject(obj, "divname");
        actionString = StringUtils.getValueFromJSONObject(obj, "actionstring");
        filters = StringUtils.getValueFromJSONObject(obj, "filters");
        createdat = StringUtils.getValueFromJSONObject(obj, "createdat");
        createdby = StringUtils.getValueFromJSONObject(obj, "createdby");
        modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");
        modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");
        return;
    }

    public JSONObject getJSONObjectUI() throws JSONException {
        JSONObject obj = new JSONObject();

        obj.put("id", StringUtils.noNull(id));
        obj.put("module_name", StringUtils.noNull(moduleName));
        obj.put("div_name", StringUtils.noNull(divName));
        obj.put("action_string", StringUtils.noNull(actionString));
        obj.put("filters", StringUtils.noNull(filters));
        obj.put("created_at", StringUtils.noNull(createdat));
        obj.put("created_by", StringUtils.noNull(createdby));
        obj.put("modified_at", StringUtils.noNull(modifiedat));
        obj.put("modified_by", StringUtils.noNull(modifiedby));
        return obj;
    }

    public void log() {

    }

    public HashMap getTableMap() {
        HashMap resultMap = new HashMap();
        ArrayList columnList = new ArrayList();
        resultMap.put("table", "config_div");

        columnList.add("id");
        columnList.add("module_name");
        columnList.add("div_name");
        columnList.add("action_string");
        columnList.add("filters");
        columnList.add("created_at");
        columnList.add("created_by");
        columnList.add("modified_at");
        columnList.add("modified_by");
        resultMap.put("ColumnList", columnList);

        return resultMap;
    }

}
