/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.application.to;

/**
 *
 * @author Deepika Kothamasu
 */
public class SeriesDoubleWithFilterProperties {


    double y;
    String color;
    String filterValue;
    public SeriesDoubleWithFilterProperties(double y, String color,String filterValue) {
        this.y = y;
        this.color = color;
        this.filterValue=filterValue;
    }


    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public String getFiterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

}