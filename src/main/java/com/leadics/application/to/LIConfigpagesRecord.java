
/*
 * LIConfigpagesRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.to;

import com.leadics.application.common.LIRecord;

import com.leadics.utils.StringUtils;

public class LIConfigpagesRecord extends LIRecord {



    private String id;
    private String parentId;
    private String moduleName;
    private String slugName;
    private String title;
    private String pageName;
    private String icon;
    private Boolean sub;
    private Double rank;
    private String routing;
    private String externalLink;
    private String budge;
    private String budgeColor;
    private boolean active;
    private boolean groupTitle;
    private String createdat;
    private String createdby;
    private String modifiedat;
    private String modifiedby;

   
    public Double getRank() {
        return rank;
    }

    public void setRank(Double rank) {
        this.rank = rank;
    }

   
    
    


    public boolean isGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(boolean groupTitle) {
        this.groupTitle = groupTitle;
    }

    public String getModuleName() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(moduleName);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(moduleName);
        } else {
            return moduleName;
        }
    }

    public String getSlugName() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(slugName);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(slugName);
        } else {
            return slugName;
        }
    }

    public String getTitle() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(title);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(title);
        } else {
            return title;
        }
    }

    public String getPageName() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(pageName);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(pageName);
        } else {
            return pageName;
        }
    }

    public String getIcon() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(icon);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(icon);
        } else {
            return icon;
        }
    }

    public Boolean getSub() {
        return sub;
    }

    public String getRouting() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(routing);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(routing);
        } else {
            return routing;
        }
    }

    public String getExternalLink() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(externalLink);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(externalLink);
        } else {
            return externalLink;
        }
    }

    public String getBudge() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(budge);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(budge);
        } else {
            return budge;
        }
    }

    public String getBudgeColor() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(budgeColor);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(budgeColor);
        } else {
            return budgeColor;
        }
    }

    public boolean getActive() {

        return active;

    }

    public boolean getGrouptitle() {

        return groupTitle;

    }

    public String getCreatedat() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(createdat);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(createdat);
        } else {
            return createdat;
        }
    }

    public String getCreatedby() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(createdby);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(createdby);
        } else {
            return createdby;
        }
    }

    public String getModifiedat() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(modifiedat);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(modifiedat);
        } else {
            return modifiedat;
        }
    }

    public String getModifiedby() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(modifiedby);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(modifiedby);
        } else {
            return modifiedby;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }



    public void setModuleName(String value) {
        moduleName = value;
    }

    public void setSlugName(String value) {
        slugName = value;
    }

    public void setTitle(String value) {
        title = value;
    }

    public void setPageName(String value) {
        pageName = value;
    }

    public void setIcon(String value) {
        icon = value;
    }

    public void setSub(Boolean value) {
        sub = value;
    }

    public void setRouting(String value) {
        routing = value;
    }

    public void setExternalLink(String value) {
        externalLink = value;
    }

    public void setBudge(String value) {
        budge = value;
    }

    public void setBudgeColor(String value) {
        budgeColor = value;
    }

    public void setActive(boolean value) {
        active = value;
    }

    public void setGroupTItle(boolean value) {
        groupTitle = value;
    }

    public void setCreatedat(String value) {
        createdat = value;
    }

    public void setCreatedby(String value) {
        createdby = value;
    }

    public void setModifiedat(String value) {
        modifiedat = value;
    }

    public void setModifiedby(String value) {
        modifiedby = value;
    }

    public String toString() {
        return "\nid:" + id
                + "\nmodulename:" + moduleName
                + "\nslugname:" + slugName
                + "\ntitle:" + title
                + "\npagename:" + pageName
                + "\nicon:" + icon
                + "\nsub:" + sub
                + "\nrouting:" + routing
                + "\nexternallink:" + externalLink
                + "\nbudge:" + budge
                + "\nbudgecolor:" + budgeColor
                + "\nactive:" + active
                + "\ngrouptitle:" + groupTitle
                + "\ncreatedat:" + createdat
                + "\ncreatedby:" + createdby
                + "\nmodifiedat:" + modifiedat
                + "\nmodifiedby:" + modifiedby
                + "\n";
    }

}
