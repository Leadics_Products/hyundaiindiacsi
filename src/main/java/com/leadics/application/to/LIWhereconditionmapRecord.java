
/*
 * LIWhereconditionmapRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.to;

import com.leadics.application.common.LIRecord;
import com.leadics.utils.StringUtils;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class LIWhereconditionmapRecord extends LIRecord
{

	private String id;
	private String wheretemplate;
	private String colnames;
	private String type;
	private String createdat;
	private String createdby;
	private String modifiedat;
	private String modifiedby;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getWheretemplate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(wheretemplate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(wheretemplate);
		}
		else
		{
			return wheretemplate;
		}
	}

	public String getColnames()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(colnames);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(colnames);
		}
		else
		{
			return colnames;
		}
	}

	public String getType()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(type);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(type);
		}
		else
		{
			return type;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setWheretemplate(String value)
	{
		wheretemplate = value;
	}

	public void setColnames(String value)
	{
		colnames = value;
	}

	public void setType(String value)
	{
		type = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nwheretemplate:" + wheretemplate +
				"\ncolnames:" + colnames +
				"\ntype:" + type +
				"\ncreatedat:" + createdat +
				"\ncreatedby:" + createdby +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\n";
	}

	public void loadContent(LIWhereconditionmapRecord inputRecord)
	{
		setId(inputRecord.getId());
		setWheretemplate(inputRecord.getWheretemplate());
		setColnames(inputRecord.getColnames());
		setType(inputRecord.getType());
		setCreatedat(inputRecord.getCreatedat());
		setCreatedby(inputRecord.getCreatedby());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
	}

	public void loadNonNullContent(LIWhereconditionmapRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getWheretemplate(), inputRecord.getWheretemplate()))
		{
			setWheretemplate(StringUtils.noNull(inputRecord.getWheretemplate()));
		}
		if (StringUtils.hasChanged(getColnames(), inputRecord.getColnames()))
		{
			setColnames(StringUtils.noNull(inputRecord.getColnames()));
		}
		if (StringUtils.hasChanged(getType(), inputRecord.getType()))
		{
			setType(StringUtils.noNull(inputRecord.getType()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id", StringUtils.noNull(id));
		obj.put("wheretemplate", StringUtils.noNull(wheretemplate));
		obj.put("colnames", StringUtils.noNull(colnames));
		obj.put("type", StringUtils.noNull(type));
		obj.put("createdat", StringUtils.noNull(createdat));
		obj.put("createdby", StringUtils.noNull(createdby));
		obj.put("modifiedat", StringUtils.noNull(modifiedat));
		obj.put("modifiedby", StringUtils.noNull(modifiedby));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");
		wheretemplate = StringUtils.getValueFromJSONObject(obj, "wheretemplate");
		colnames = StringUtils.getValueFromJSONObject(obj, "colnames");
		type = StringUtils.getValueFromJSONObject(obj, "type");
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id", StringUtils.noNull(id));
		obj.put("where_template", StringUtils.noNull(wheretemplate));
		obj.put("colnames", StringUtils.noNull(colnames));
		obj.put("type", StringUtils.noNull(type));
		obj.put("created_at", StringUtils.noNull(createdat));
		obj.put("created_by", StringUtils.noNull(createdby));
		obj.put("modified_at", StringUtils.noNull(modifiedat));
		obj.put("modified_by", StringUtils.noNull(modifiedby));
		return obj;
	}

	public void log()
	{

	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "where_condition_map");

		columnList.add("id");				
		columnList.add("where_template");				
		columnList.add("colnames");				
		columnList.add("type");				
		columnList.add("created_at");				
		columnList.add("created_by");				
		columnList.add("modified_at");				
		columnList.add("modified_by");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}
