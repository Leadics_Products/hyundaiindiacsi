/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.application.to;

import com.google.gson.JsonObject;
import com.leadics.utils.ActionPlanning;


import java.util.List;

/**
 *
 * @author krishna-leadics
 */
public class ChartsConfig {

    private Integer id;
    private Integer parentId;
    private String title;
    private String name;
    private String subTitle;
    private String slugName;
    private String type;
    private String pageName;

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }
    private ActionPlanning actionPlanning;
    private JsonObject extraConfig;
    private JsonObject data;
    private List<ChartsConfig> sub;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public List<ChartsConfig> getSub() {
        return sub;
    }

    public void setSub(List<ChartsConfig> sub) {
        this.sub = sub;
    }
   
   
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getSlugName() {
        return slugName;
    }

    public void setSlugName(String slugName) {
        this.slugName = slugName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ActionPlanning getActionPlanning() {
        return actionPlanning;
    }

    public void setActionPlanning(ActionPlanning actionPlanning) {
        this.actionPlanning = actionPlanning;
    }

    public JsonObject getExtraConfig() {
        return extraConfig;
    }

    public void setExtraConfig(JsonObject extraConfig) {
        this.extraConfig = extraConfig;
    }

    public JsonObject getData() {
        return data;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }
}



