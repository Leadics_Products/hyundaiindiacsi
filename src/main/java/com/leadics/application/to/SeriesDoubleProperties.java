/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.application.to;

/**
 *
 * @author Deepika Kothamasu
 */
public class SeriesDoubleProperties {


    double y;
    String color;

    public SeriesDoubleProperties(double y, String color) {
        this.y = y;
        this.color = color;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
