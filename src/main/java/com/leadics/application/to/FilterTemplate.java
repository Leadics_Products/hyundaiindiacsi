/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.application.to;

import java.util.List;

/**
 *
 * @author HARSHA
 */
public class FilterTemplate {
    
    String pageName;
    List<FilterProperties> filterInfo;

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public List<FilterProperties> getFilterInfo() {
        return filterInfo;
    }

    public void setFilterInfo(List<FilterProperties> filterInfo) {
        this.filterInfo = filterInfo;
    }
    
}
