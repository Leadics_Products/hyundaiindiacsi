
/*
 * LIConfigpagefilterRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.to;

import com.leadics.application.common.LIRecord;

import com.leadics.utils.StringUtils;

public class LIConfigpagefilterRecord extends LIRecord {


    private String id;
    private String pagename;
    private String filtername;
    private String slugfiltername;
    private String filtertype;
    private String filterplacehodler;
    private double filterpriority;
    private String parents;
    private String childs;
    private int defaultindex;
    private boolean containsall;
    private boolean containsnone;
    private boolean containsfiltergroup;
     private boolean persistafterroutechange;
    private String createdat;
    private String createdby;
    private String modifiedat;
    private String modifiedby;

    public String getHidewhen() {
        return hidewhen;
    }

    public void setHidewhen(String hidewhen) {
        this.hidewhen = hidewhen;
    }
       private String hidewhen;
    public String getId() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(id);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(id);
        } else {
            return id;
        }
    }
 public boolean getPersistAfterRouteChange() {
            return persistafterroutechange;
    }
  
  
    public String getPagename() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(pagename);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(pagename);
        } else {
            return pagename;
        }
    }

    public String getFiltername() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(filtername);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(filtername);
        } else {
            return filtername;
        }
    }

    public String getSlugfiltername() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(slugfiltername);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(slugfiltername);
        } else {
            return slugfiltername;
        }
    }

    public String getFiltertype() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(filtertype);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(filtertype);
        } else {
            return filtertype;
        }
    }

    public String getFilterplacehodler() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(filterplacehodler);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(filterplacehodler);
        } else {
            return filterplacehodler;
        }
    }

    public double getFilterpriority() {
        return filterpriority;
    }

    public String getParents() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(parents);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(parents);
        } else {
            return parents;
        }
    }

    public String getChilds() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(childs);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(childs);
        } else {
            return childs;
        }
    }

    public int getDefaultindex() {
        return defaultindex;
    }

    public boolean getContainsall() {
        return containsall;
    }

    public boolean getContainsnone() {
        return containsnone;
    }

    public boolean getContainsfiltergroup() {
        return containsfiltergroup;
    }

    public String getCreatedat() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(createdat);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(createdat);
        } else {
            return createdat;
        }
    }

    public String getCreatedby() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(createdby);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(createdby);
        } else {
            return createdby;
        }
    }

    public String getModifiedat() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(modifiedat);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(modifiedat);
        } else {
            return modifiedat;
        }
    }

    public String getModifiedby() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(modifiedby);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(modifiedby);
        } else {
            return modifiedby;
        }
    }

    public void setId(String value) {
        id = value;
    }

    public void setPagename(String value) {
        pagename = value;
    }
 
    public void setFiltername(String value) {
        filtername = value;
    }

    public void setSlugfiltername(String value) {
        slugfiltername = value;
    }

    public void setFiltertype(String value) {
        filtertype = value;
    }

    public void setFilterplacehodler(String value) {
        filterplacehodler = value;
    }

    public void setFilterpriority(double value) {
        filterpriority = value;
    }

    public void setParents(String value) {
        parents = value;
    }

    public void setChilds(String value) {
        childs = value;
    }

    public void setDefaultindex(int value) {
        defaultindex = value;
    }

    public void setContainsall(boolean value) {
        containsall = value;
    }

    public void setContainsnone(boolean value) {
        containsnone = value;
    }

    public void setContainsfiltergroup(boolean value) {
        containsfiltergroup = value;
    }

    public void setCreatedat(String value) {
        createdat = value;
    }

    public void setCreatedby(String value) {
        createdby = value;
    }

    public void setModifiedat(String value) {
        modifiedat = value;
    }

    public void setModifiedby(String value) {
        modifiedby = value;
    }
    
     public void setPersistAfterRouteChange(boolean value) {
        persistafterroutechange = value;
    }

}
