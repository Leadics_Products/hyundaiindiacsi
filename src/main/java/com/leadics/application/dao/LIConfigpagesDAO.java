
/*
 * LIConfigpagesDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.dao;

import com.leadics.application.common.LIDAO;
import com.leadics.application.to.LIConfigpagesRecord;
//import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class LIConfigpagesDAO extends LIDAO {
    
//    static LogUtils //    logger = new LogUtils(LIConfigpagesDAO.class.getName());
    
    public LIConfigpagesRecord[] loadLIConfigpagesRecords(String query, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            query = query + MAX_RECORD_LIMIT_APPENDER;
            query = updateQuery(query);           
            ps = con.prepareStatement(query);
            System.out.println(query);
            rs = ps.executeQuery();
            ArrayList recordSet = new ArrayList();
            while (rs.next()) {
                LIConfigpagesRecord record = new LIConfigpagesRecord();
                record.setId(rs.getString("ID"));
                record.setTitle(rs.getString("TITLE"));
                record.setRank(rs.getDouble("RANK"));
                record.setParentId(rs.getString("PARENT_ID"));
                record.setPageName(rs.getString("PAGE_NAME"));
                record.setIcon(rs.getString("ICON"));
                record.setSub(rs.getBoolean("SUB"));
                record.setRouting(rs.getString("ROUTING"));
                record.setExternalLink(rs.getString("EXTERNAL_LINK"));
                record.setBudge(rs.getString("BUDGE"));
                record.setBudgeColor(rs.getString("BUDGE_COLOR"));
                record.setActive(rs.getBoolean("ACTIVE"));
                record.setGroupTItle(rs.getBoolean("GROUP_TITLE"));
                recordSet.add(record);
            }         
            LIConfigpagesRecord[] tempLIConfigpagesRecords = new LIConfigpagesRecord[recordSet.size()];
            for (int index = 0; index < recordSet.size(); index++) {
                tempLIConfigpagesRecords[index] = (LIConfigpagesRecord) (recordSet.get(index));
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            return tempLIConfigpagesRecords;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }
    
    public LIConfigpagesRecord[] loadLIConfigpagesRecords(String query)
            throws Exception {
        return loadLIConfigpagesRecords(query, null, true);
    }
    
    public LIConfigpagesRecord loadFirstLIConfigpagesRecord(String query)
            throws Exception {
        LIConfigpagesRecord[] results = loadLIConfigpagesRecords(query);
        if (results == null) {
            return null;
        }
        if (results.length < 1) {
            return null;
        }
        return results[0];
    }
    
    public LIConfigpagesRecord loadLIConfigpagesRecord(Integer id, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            String Query = "SELECT * FROM config_pages WHERE (ID = ?)";
            Query = updateQuery(Query);          
            ps = con.prepareStatement(Query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                ps.close();
                releaseDatabaseConnection(con, closeConnection);
                return null;
            }
            LIConfigpagesRecord record = new LIConfigpagesRecord();
            record.setId(rs.getString("ID"));
            record.setModuleName(rs.getString("MODULE_NAME"));
            record.setSlugName(rs.getString("SLUG_NAME"));
            record.setTitle(rs.getString("TITLE"));
            record.setPageName(rs.getString("PAGE_NAME"));
            record.setIcon(rs.getString("ICON"));
//            record.setSub(rs.getString("SUB"));
            record.setRouting(rs.getString("ROUTING"));
            record.setExternalLink(rs.getString("EXTERNAL_LINK"));
            record.setBudge(rs.getString("BUDGE"));
            record.setBudgeColor(rs.getString("BUDGE_COLOR"));
            record.setActive(rs.getBoolean("ACTIVE"));
            record.setGroupTItle(rs.getBoolean("GROUP_TITLE"));
            record.setCreatedat(rs.getString("CREATED_AT"));
            record.setCreatedby(rs.getString("CREATED_BY"));
            record.setModifiedat(rs.getString("MODIFIED_AT"));
            record.setModifiedby(rs.getString("MODIFIED_BY"));
            ps.close();
            //    logger.trace("loadLIConfigpagesRecord\t" + record + "\t");
            releaseDatabaseConnection(con, closeConnection);
            return record;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }
    
    public LIConfigpagesRecord loadLIConfigpagesRecord(Integer id)
            throws Exception {
        return loadLIConfigpagesRecord(id, null, true);
    }
   
}
