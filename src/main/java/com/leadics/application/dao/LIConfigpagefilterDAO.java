
/*
 * LIConfigpagefilterDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.dao;

import com.leadics.application.common.LIDAO;
import com.leadics.application.to.LIConfigpagefilterRecord;
//import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class LIConfigpagefilterDAO extends LIDAO {

//    static LogUtils //    logger = new LogUtils(LIConfigpagefilterDAO.class.getName());
    public LIConfigpagefilterRecord[] loadLIConfigpagefilterRecords(String query, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            query = query + MAX_RECORD_LIMIT_APPENDER;
            query = updateQuery(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList recordSet = new ArrayList();
            while (rs.next()) {
                LIConfigpagefilterRecord record = new LIConfigpagefilterRecord();
                record.setPagename(rs.getString("PAGE_NAME"));
                record.setFiltername(rs.getString("FILTER_NAME"));
                record.setSlugfiltername(rs.getString("SLUG_FILTER_NAME"));
                record.setFiltertype(rs.getString("FILTER_TYPE"));
                record.setFilterplacehodler(rs.getString("FILTER_PLACEHODLER"));
                record.setFilterpriority(rs.getDouble("FILTER_PRIORITY"));
                record.setParents(rs.getString("PARENTS"));
                record.setChilds(rs.getString("CHILDS"));
                record.setDefaultindex(rs.getInt("DEFAULT_INDEX"));
                record.setContainsall(rs.getBoolean("CONTAINS_ALL"));
                record.setContainsnone(rs.getBoolean("CONTAINS_NONE"));
                record.setContainsfiltergroup(rs.getBoolean("CONTAINS_FILTER_GROUP"));
                record.setHidewhen(rs.getString("HIDE_WHEN"));
                record.setPersistAfterRouteChange(rs.getBoolean("PERSIST_AFTER_ROUTE_CHANGE"));
                recordSet.add(record);
            }
            LIConfigpagefilterRecord[] tempLIConfigpagefilterRecords = new LIConfigpagefilterRecord[recordSet.size()];
            for (int index = 0; index < recordSet.size(); index++) {
                tempLIConfigpagefilterRecords[index] = (LIConfigpagefilterRecord) (recordSet.get(index));
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            return tempLIConfigpagefilterRecords;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigpagefilterRecord[] loadLIConfigpagefilterRecords(String query)
            throws Exception {
        return loadLIConfigpagefilterRecords(query, null, true);
    }

    public LIConfigpagefilterRecord loadFirstLIConfigpagefilterRecord(String query)
            throws Exception {
        LIConfigpagefilterRecord[] results = loadLIConfigpagefilterRecords(query);
        if (results == null) {
            return null;
        }
        if (results.length < 1) {
            return null;
        }
        return results[0];
    }

    public LIConfigpagefilterRecord loadLIConfigpagefilterRecord(String id, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            String Query = "SELECT * FROM config_page_filter WHERE (ID = ?)";
            Query = updateQuery(Query);
            //    logger.trace("loadLIConfigpagefilterRecord\t" + closeConnection + "\t" + id + "\t" + Query);
            ps = con.prepareStatement(Query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                ps.close();
                releaseDatabaseConnection(con, closeConnection);
                return null;
            }
            LIConfigpagefilterRecord record = new LIConfigpagefilterRecord();
            record.setId(rs.getString("ID"));
            record.setPagename(rs.getString("PAGE_NAME"));
            record.setFiltername(rs.getString("FILTER_NAME"));
            record.setSlugfiltername(rs.getString("SLUG_FILTER_NAME"));
            record.setFiltertype(rs.getString("FILTER_TYPE"));
            record.setFilterplacehodler(rs.getString("FILTER_PLACEHODLER"));
            record.setFilterpriority(rs.getInt("FILTER_PRIORITY"));
            record.setParents(rs.getString("PARENTS"));
            record.setChilds(rs.getString("CHILDS"));
            record.setDefaultindex(rs.getInt("DEFAULT_INDEX"));
            record.setContainsall(rs.getBoolean("CONTAINS_ALL"));
            record.setContainsnone(rs.getBoolean("CONTAINS_NONE"));
            record.setContainsfiltergroup(rs.getBoolean("CONTAINS_FILTER_GROUP"));
            record.setCreatedat(rs.getString("CREATED_AT"));
            record.setCreatedby(rs.getString("CREATED_BY"));
            record.setModifiedat(rs.getString("MODIFIED_AT"));
            record.setModifiedby(rs.getString("MODIFIED_BY"));
            ps.close();
            //    logger.trace("loadLIConfigpagefilterRecord\t" + record + "\t");
            releaseDatabaseConnection(con, closeConnection);
            return record;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigpagefilterRecord loadLIConfigpagefilterRecord(String id)
            throws Exception {
        return loadLIConfigpagefilterRecord(id, null, true);
    }

  
}
