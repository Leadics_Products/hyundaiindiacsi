
/*
 * LIControllermapDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.dao;

import com.leadics.application.common.LIDAO;
import com.leadics.application.to.LIControllermapRecord;
//import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class LIControllermapDAO extends LIDAO {
//	static LogUtils //    logger = new LogUtils(LIControllermapDAO.class.getName());

    public LIControllermapRecord[] loadLIControllermapRecords(String query, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            query = query + MAX_RECORD_LIMIT_APPENDER;
            query = updateQuery(query);
            //    logger.trace("loadLIControllermapRecords\t" + closeConnection + "\t" + query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList recordSet = new ArrayList();
            while (rs.next()) {
                LIControllermapRecord record = new LIControllermapRecord();
                record.setId(rs.getString("ID"));
                record.setCode(rs.getString("CODE"));
                record.setClassname(rs.getString("CLASSNAME"));
                record.setAuthflag(rs.getString("AUTH_FLAG"));
                recordSet.add(record);
            }
            //    logger.trace("loadLIControllermapRecords:Records Fetched:" + recordSet.size());
            LIControllermapRecord[] tempLIControllermapRecords = new LIControllermapRecord[recordSet.size()];
            for (int index = 0; index < recordSet.size(); index++) {
                tempLIControllermapRecords[index] = (LIControllermapRecord) (recordSet.get(index));
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            return tempLIControllermapRecords;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIControllermapRecord[] loadLIControllermapRecords(String query)
            throws Exception {
        return loadLIControllermapRecords(query, null, true);
    }

    public LIControllermapRecord loadFirstLIControllermapRecord(String query)
            throws Exception {
        LIControllermapRecord[] results = loadLIControllermapRecords(query);
        if (results == null) {
            return null;
        }
        if (results.length < 1) {
            return null;
        }
        return results[0];
    }

    public LIControllermapRecord loadLIControllermapRecord(String id, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            String Query = "SELECT * FROM controller_map WHERE (ID = ?)";
            Query = updateQuery(Query);
            //    logger.trace("loadLIControllermapRecord\t" + closeConnection + "\t" + id + "\t" + Query);
            ps = con.prepareStatement(Query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                ps.close();
                releaseDatabaseConnection(con, closeConnection);
                return null;
            }
            LIControllermapRecord record = new LIControllermapRecord();
            record.setId(rs.getString("ID"));
            record.setCode(rs.getString("CODE"));
            record.setClassname(rs.getString("CLASSNAME"));
            record.setAuthflag(rs.getString("AUTH_FLAG"));
            ps.close();
            //    logger.trace("loadLIControllermapRecord\t" + record + "\t");
            releaseDatabaseConnection(con, closeConnection);
            return record;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIControllermapRecord loadLIControllermapRecord(String id)
            throws Exception {
        return loadLIControllermapRecord(id, null, true);
    }

    public LIControllermapRecord[] searchLIControllermapRecords(LIControllermapRecord searchRecord)
            throws Exception {
        String WhereCondition = "";
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ID", formatSearchField(searchRecord.getId()));
        WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CODE", formatSearchField(searchRecord.getCode()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CLASSNAME", formatSearchField(searchRecord.getClassname()));
        WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "AUTH_FLAG", formatSearchField(searchRecord.getAuthflag()));
        if (isNull(WhereCondition)) {
            WhereCondition += "(1=1)";
        }
        if (!isNull(WhereCondition)) {
            WhereCondition = "WHERE " + WhereCondition;
        }
        if (hasCustomCondition()) {
            WhereCondition += getCustomCondition();
        }
        String Query = "select * from controller_map " + WhereCondition + " order by " + ORDERBYSTRING;
        if (isMSSQL8()) {
            Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM controller_map ) acvmfs " + WhereCondition;
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING), true);
        }
        if (isOracleDatabase()) {
            Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM controller_map $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
            Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition, true);
            Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(), true);
            Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING), true);
        }
        Query = updateQuery(Query);

        return loadLIControllermapRecords(Query);
    }
}
