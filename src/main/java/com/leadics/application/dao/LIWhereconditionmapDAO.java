
/*
 * LIWhereconditionmapDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.dao;

import com.leadics.application.common.LIDAO;
import com.leadics.application.to.LIWhereconditionmapRecord;
//import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class LIWhereconditionmapDAO extends LIDAO
{
//	static LogUtils //    logger = new LogUtils(LIWhereconditionmapDAO.class.getName());


	public LIWhereconditionmapRecord[] loadLIWhereconditionmapRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			//    logger.trace("loadLIWhereconditionmapRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LIWhereconditionmapRecord record = new LIWhereconditionmapRecord();
				record.setId(rs.getString("id"));
				record.setWheretemplate(rs.getString("where_template"));
				record.setColnames(rs.getString("colNames"));
				record.setType(rs.getString("type"));
				record.setCreatedat(rs.getString("created_at"));
				record.setCreatedby(rs.getString("created_by"));
				record.setModifiedat(rs.getString("modified_at"));
				record.setModifiedby(rs.getString("modified_by"));
				recordSet.add(record);
			}
			//    logger.trace("loadLIWhereconditionmapRecords:Records Fetched:" + recordSet.size());
			LIWhereconditionmapRecord[] tempLIWhereconditionmapRecords = new LIWhereconditionmapRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLIWhereconditionmapRecords[index] = (LIWhereconditionmapRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLIWhereconditionmapRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LIWhereconditionmapRecord[] loadLIWhereconditionmapRecords(String query)
	throws Exception
	{
		return loadLIWhereconditionmapRecords(query, null, true);
	}


	public LIWhereconditionmapRecord loadFirstLIWhereconditionmapRecord(String query)
	throws Exception
	{
		LIWhereconditionmapRecord[] results = loadLIWhereconditionmapRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LIWhereconditionmapRecord loadLIWhereconditionmapRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM where_condition_map WHERE (id = ?)";
			Query = updateQuery(Query);
			//    logger.trace("loadLIWhereconditionmapRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LIWhereconditionmapRecord record = new LIWhereconditionmapRecord();
			record.setId(rs.getString("id"));
			record.setWheretemplate(rs.getString("where_template"));
			record.setColnames(rs.getString("colNames"));
			record.setType(rs.getString("type"));
			record.setCreatedat(rs.getString("created_at"));
			record.setCreatedby(rs.getString("created_by"));
			record.setModifiedat(rs.getString("modified_at"));
			record.setModifiedby(rs.getString("modified_by"));
			ps.close();
			//    logger.trace("loadLIWhereconditionmapRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LIWhereconditionmapRecord loadLIWhereconditionmapRecord(String id)
	throws Exception
	{
		return loadLIWhereconditionmapRecord(id, null, true);
	}

}
