
/*
 * LIConfigdivDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.dao;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.leadics.application.common.LIDAO;
import com.leadics.application.to.LIConfigdivRecord;
//import com.leadics.utils.LogUtils;
import org.json.JSONException;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class LIConfigdivDAO extends LIDAO
{	public JsonObject strToJson(String extraConfig) throws JSONException {
	JsonParser parser = new JsonParser();
	JsonObject jsonObj = parser.parse(extraConfig).getAsJsonObject();
	return jsonObj;
}

	public LIConfigdivRecord[] loadLIConfigdivRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			//    logger.trace("loadLIConfigdivRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LIConfigdivRecord record = new LIConfigdivRecord();
				record.setId(rs.getString("id"));
				record.setModuleName(rs.getString("module_name"));
				record.setDivName(rs.getString("div_name"));
				record.setActionString(rs.getString("action_string"));
				record.setFilters(rs.getString("filters"));
                                record.setChartName(rs.getString("chartName"));
                                record.setChartType(rs.getString("chartType"));
                                record.setSub(rs.getBoolean("sub"));
                                record.setPageName(rs.getString("pageName"));
                                record.setParentId(rs.getString("parentId"));

				record.setExtraConfig(strToJson(rs.getString("extraConfig")));
				record.setData(strToJson(rs.getString("data")));
				record.setCreatedat(rs.getString("created_at"));
				record.setCreatedby(rs.getString("created_by"));
				record.setModifiedat(rs.getString("modified_at"));
				record.setModifiedby(rs.getString("modified_by"));
				recordSet.add(record);
			}			
			LIConfigdivRecord[] tempLIConfigdivRecords = new LIConfigdivRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLIConfigdivRecords[index] = (LIConfigdivRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLIConfigdivRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LIConfigdivRecord[] loadLIConfigdivRecords(String query)
	throws Exception
	{
		return loadLIConfigdivRecords(query, null, true);
	}


	public LIConfigdivRecord loadFirstLIConfigdivRecord(String query)
	throws Exception
	{
		LIConfigdivRecord[] results = loadLIConfigdivRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LIConfigdivRecord loadLIConfigdivRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM config_div WHERE (id = ?)";
			Query = updateQuery(Query);
			//    logger.trace("loadLIConfigdivRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LIConfigdivRecord record = new LIConfigdivRecord();
			record.setId(rs.getString("id"));
			record.setModuleName(rs.getString("module_name"));
			record.setDivName(rs.getString("div_name"));
			record.setActionString(rs.getString("action_string"));
			record.setFilters(rs.getString("filters"));
			record.setCreatedat(rs.getString("created_at"));
			record.setCreatedby(rs.getString("created_by"));
			record.setModifiedat(rs.getString("modified_at"));
			record.setModifiedby(rs.getString("modified_by"));
			ps.close();
			//    logger.trace("loadLIConfigdivRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LIConfigdivRecord loadLIConfigdivRecord(String id)
	throws Exception
	{
		return loadLIConfigdivRecord(id, null, true);
	}

}
