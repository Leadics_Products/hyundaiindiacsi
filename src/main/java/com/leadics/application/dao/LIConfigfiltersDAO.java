
/*
 * LIConfigfiltersDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.dao;

import com.leadics.application.common.LIDAO;
import com.leadics.application.to.LIConfigfiltersRecord;
//import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class LIConfigfiltersDAO extends LIDAO {



    public LIConfigfiltersRecord[] loadLIConfigfiltersRecords(String query, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            query = query + MAX_RECORD_LIMIT_APPENDER;
            query = updateQuery(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList recordSet = new ArrayList();
            while (rs.next()) {
                LIConfigfiltersRecord record = new LIConfigfiltersRecord();
                record.setId(rs.getString("ID"));
                record.setModuleName(rs.getString("MODULE_NAME"));
                record.setFilterName(rs.getString("FILTER_NAME"));
                record.setFilterActionString(rs.getString("FILTER_ACTION_STRING"));
                record.setCreatedat(rs.getString("CREATED_AT"));
                record.setCreatedby(rs.getString("CREATED_BY"));
                record.setModifiedat(rs.getString("MODIFIED_AT"));
                record.setModifiedby(rs.getString("MODIFIED_BY"));
                recordSet.add(record);
            }

            LIConfigfiltersRecord[] tempLIConfigfiltersRecords = new LIConfigfiltersRecord[recordSet.size()];
            for (int index = 0; index < recordSet.size(); index++) {
                tempLIConfigfiltersRecords[index] = (LIConfigfiltersRecord) (recordSet.get(index));
            }
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            return tempLIConfigfiltersRecords;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigfiltersRecord[] loadLIConfigfiltersRecords(String query)
            throws Exception {
        return loadLIConfigfiltersRecords(query, null, true);
    }

    public LIConfigfiltersRecord loadFirstLIConfigfiltersRecord(String query)
            throws Exception {
        LIConfigfiltersRecord[] results = loadLIConfigfiltersRecords(query);
        if (results == null) {
            return null;
        }
        if (results.length < 1) {
            return null;
        }
        return results[0];
    }

    public LIConfigfiltersRecord loadLIConfigfiltersRecord(String id, Connection con, boolean closeConnection)
            throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            String Query = "SELECT * FROM config_filters WHERE (ID = ?)";
            Query = updateQuery(Query);

            ps = con.prepareStatement(Query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (!rs.next()) {
                ps.close();
                releaseDatabaseConnection(con, closeConnection);
                return null;
            }
            LIConfigfiltersRecord record = new LIConfigfiltersRecord();
            record.setId(rs.getString("ID"));
            record.setModuleName(rs.getString("MODULE_NAME"));
            record.setFilterName(rs.getString("FILTER_NAME"));
            record.setFilterActionString(rs.getString("FILTER_ACTION_STRING"));
            record.setCreatedat(rs.getString("CREATED_AT"));
            record.setCreatedby(rs.getString("CREATED_BY"));
            record.setModifiedat(rs.getString("MODIFIED_AT"));
            record.setModifiedby(rs.getString("MODIFIED_BY"));
            ps.close();
            releaseDatabaseConnection(con, closeConnection);
            return record;
        } finally {
            releaseDatabaseConnection(rs, ps, con, closeConnection);
        }
    }

    public LIConfigfiltersRecord loadLIConfigfiltersRecord(String id)
            throws Exception {
        return loadLIConfigfiltersRecord(id, null, true);
    }
}
