/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.application.representation;

import java.util.List;

/**
 *
 * @author HARSHA
 */
public class MenuRepresentation {

   
    private String id;
    private String title;
    
    private String pageName;
    private String icon;
    private List<MenuRepresentation> sub;
    private String routing;
    private String externalLink;
    private String budge;
    private String budgeColor;
    private boolean active;
    private boolean groupTitle;
    private Double rank;
   
    private String parentId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<MenuRepresentation> getSub() {
        return sub;
    }

    public void setSub(List<MenuRepresentation> sub) {
        this.sub = sub;
    }

    public String getRouting() {
        return routing;
    }

    public void setRouting(String routing) {
        this.routing = routing;
    }

    public String getExternalLink() {
        return externalLink;
    }

    public void setExternalLink(String externalLink) {
        this.externalLink = externalLink;
    }

    public String getBudge() {
        return budge;
    }

    public void setBudge(String budge) {
        this.budge = budge;
    }

    public String getBudgeColor() {
        return budgeColor;
    }

    public void setBudgeColor(String budgeColor) {
        this.budgeColor = budgeColor;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(boolean groupTitle) {
        this.groupTitle = groupTitle;
    }

    public Double getRank() {
        return rank;
    }

    public void setRank(Double rank) {
        this.rank = rank;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }



}
