
/*
 * LIControllermapService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.service;

import com.leadics.application.common.LIService;
import com.leadics.application.dao.LIControllermapDAO;
import com.leadics.application.to.LIControllermapRecord;

import org.json.simple.*;

import java.sql.Connection;

public class LIControllermapService extends LIService {

    public LIControllermapRecord[] loadLIControllermapRecords(String query)
            throws Exception {
        try {
            LIControllermapDAO dao = new LIControllermapDAO();
            LIControllermapRecord[] results = dao.loadLIControllermapRecords(query);
            int resultRecordCount = 0;
            if (results != null) {
                resultRecordCount = results.length;
            }

            return results;
        } catch (Exception exception) {

            throw new Exception(exception);
        }
    }

    public LIControllermapRecord loadFirstLIControllermapRecord(String query)
            throws Exception {
        try {
            LIControllermapDAO dao = new LIControllermapDAO();
            LIControllermapRecord result = dao.loadFirstLIControllermapRecord(query);

            return result;
        } catch (Exception exception) {

            throw new Exception(exception);
        }
    }

    public LIControllermapRecord[] searchLIControllermapRecords(LIControllermapRecord record)
            throws Exception {
        try {

            LIControllermapDAO dao = new LIControllermapDAO();
            LIControllermapRecord[] records = dao.searchLIControllermapRecords(record);

            return records;
        } catch (Exception exception) {

            throw new Exception(exception);
        }
    }

    public LIControllermapRecord loadLIControllermapRecord(String key)
            throws Exception {
        try {
            LIControllermapDAO dao = new LIControllermapDAO();
            LIControllermapRecord result = dao.loadLIControllermapRecord(key);
            return result;
        } catch (Exception exception) {
            throw exception;
        }
    }

    public JSONObject getJSONLIControllermapRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
            throws Exception {
        try {
            LIControllermapDAO dao = new LIControllermapDAO();
            int totalCount = dao.loadRecordCount(con, countQuery);
            dao.setLimits(offset, maxrows);
            LIControllermapRecord[] records = null;
            if (totalCount > 0) {
                records = dao.loadLIControllermapRecords(query, con, true);
            }
            JSONObject resultObject = new JSONObject();
            resultObject.put("total", totalCount + "");
            JSONArray dataArray = new JSONArray();
            int recordCount = 0;
            if (records != null) {
                recordCount = records.length;
            }
            for (int index = 0; index < recordCount; index++) {
                dataArray.add(records[index].getJSONObjectUI());
            }
            resultObject.put("rows", dataArray);
            return resultObject;
        } catch (Exception exception) {
            throw new Exception(exception);
        }
    }

}
