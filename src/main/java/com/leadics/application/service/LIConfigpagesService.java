
/*
 * LIConfigpagesService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors IQS
 * Module                   : Hundai Motors IQS
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.application.service;

import com.leadics.application.common.LIService;
import com.leadics.application.dao.LIConfigpagesDAO;
import com.leadics.application.to.LIConfigpagesRecord;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.Connection;

public class LIConfigpagesService extends LIService {

    public LIConfigpagesRecord[] loadLIConfigpagesRecords(String query)
            throws Exception {
        try {

            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            LIConfigpagesRecord[] results = dao.loadLIConfigpagesRecords(query);
            int resultRecordCount = 0;
            if (results != null) {
                resultRecordCount = results.length;
            }

            return results;
        } catch (Exception exception) {

            throw new Exception(exception);
        }
    }

    public LIConfigpagesRecord loadFirstLIConfigpagesRecord(String query)
            throws Exception {
        try {

            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            LIConfigpagesRecord result = dao.loadFirstLIConfigpagesRecord(query);

            return result;
        } catch (Exception exception) {

            throw new Exception(exception);
        }
    }

    public LIConfigpagesRecord loadLIConfigpagesRecord(String key)
            throws Exception {
        try {

            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            LIConfigpagesRecord result = dao.loadLIConfigpagesRecord(0);

            return result;
        } catch (Exception exception) {

            throw exception;
        }
    }

    public JSONObject getJSONLIConfigpagesRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
            throws Exception {
        try {
            LIConfigpagesDAO dao = new LIConfigpagesDAO();
            int totalCount = dao.loadRecordCount(con, countQuery);
            dao.setLimits(offset, maxrows);
            LIConfigpagesRecord[] records = null;
            if (totalCount > 0) {
                records = dao.loadLIConfigpagesRecords(query, con, true);
            }
            JSONObject resultObject = new JSONObject();
            resultObject.put("total", totalCount + "");
            JSONArray dataArray = new JSONArray();
            int recordCount = 0;
            if (records != null) {
                recordCount = records.length;
            }
            for (int index = 0; index < recordCount; index++) {
//                dataArray.add(records[index].getJSONObjectUI());
            }
            resultObject.put("rows", dataArray);

            return resultObject;
        } catch (Exception exception) {

            throw new Exception(exception);
        }
    }
}
