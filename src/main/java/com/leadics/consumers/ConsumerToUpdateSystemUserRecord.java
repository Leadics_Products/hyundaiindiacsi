/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.consumers;

import com.leadics.application.common.LIAction;
import com.leadics.application.common.LIDAO;

import com.leadics.utils.PropertyUtil;
import com.leap.core.events.admin.consumers.AUserRoleUpdateEventConsumer;
import com.leap.core.events.admin.representations.PluginUserUpdateRepresentation;
import com.leap.core.services.admin.dao.SystemUserDao;

import java.sql.Connection;

/**
 *
 * @author shrinidhi
 */
public class ConsumerToUpdateSystemUserRecord extends AUserRoleUpdateEventConsumer {
	

	public ConsumerToUpdateSystemUserRecord() throws Exception {
		super();
	}

	@Override
	public void onEvent(PluginUserUpdateRepresentation object) {

		try {
			Connection connection = new LIDAO().getCPDatabaseConnection();
			SystemUserDao dao = new SystemUserDao();
			dao.updateOrDelete(connection, object, Integer.parseInt(PropertyUtil.getProperty("APPLICATION_ID")));
		} catch (Exception ex) {

		}

	}

}
