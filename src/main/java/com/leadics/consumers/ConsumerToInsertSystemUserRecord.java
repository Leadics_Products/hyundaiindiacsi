/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.consumers;

import com.leadics.application.common.LIAction;
import com.leadics.application.common.LIDAO;

import com.leadics.utils.PropertyUtil;
import com.leap.core.events.admin.consumers.AUserCreationEventConsumer;
import com.leap.core.events.admin.representations.SSOUserRepresentation;
import com.leap.core.services.admin.dao.SystemUserDao;

import java.sql.Connection;

/**
 *
 * @author shrinidhi
 */
public class ConsumerToInsertSystemUserRecord  extends AUserCreationEventConsumer {
	

			
     public ConsumerToInsertSystemUserRecord() throws Exception {
         super();
    }

     @Override
    public void onEvent(SSOUserRepresentation object) {
        try {
        Connection connection = new LIDAO().getCPDatabaseConnection();
        SystemUserDao dao = new SystemUserDao();
            dao.insertSystemUserRecord(connection, object, Integer.parseInt(PropertyUtil.getProperty("APPLICATION_ID")));
         } catch (Exception e) {

         }
    }
    
}
