package com.leadics.utils;
//Java imports
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.ResourceBundle;

public class PropertyUtil
{
	private static String dbdriver = null;
	private static String dburl = null;
	private static String dbuid = null;
	private static String dbpwd = null;
	private static String enckey =null;
	private static HashMap propertyMap = new HashMap();

	public static String getEnckey() {
		return enckey;
	}

	public static void setEnckey(String enckey) {
		PropertyUtil.enckey = enckey;
	}

	static
	{
		loadProperties();	
		//loadDefaultPropertiesFromFile();
	}
	
	public static void loadDefaultPropertiesFromFile()
	{
		try
		{
//			Properties defaultProps = new Properties();
//			FileInputStream in = new FileInputStream("/app/properties/da.properties");
//			defaultProps.load(in);
//			
//			Iterator iterator = defaultProps.keySet().iterator();
//			while(iterator.hasNext())
//			{
//				String key = iterator.next().toString();
//				String value = defaultProps.getProperty(key);
//				propertyMap.put(key,value);
//			}
			
//			LogUtils.println(defaultProps.values());
//			in.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void loadProperties()

	{
    	if (dbdriver == null)
    	{
			ResourceBundle rb = ResourceBundle.getBundle("application", Locale.getDefault());
			dbdriver = rb.getString("dbdriver");
			dburl=rb.getString("dburl");
			dbuid=rb.getString("dbuid");
			dbpwd=rb.getString("dbpwd");
			enckey=rb.getString("enckey");
			
			Iterator iterator = rb.keySet().iterator();
			while(iterator.hasNext())
			{
				String key = iterator.next().toString();
				String value = rb.getString(key);
				propertyMap.put(key,value);
			}
    	}
	}
	
	private static boolean isNonMigrateProperty(String propertyName)
	{
		if (propertyName.contains("dbdriver")) return true;
		if (propertyName.contains("dburl")) return true;
		if (propertyName.contains("dbuid")) return true;
		if (propertyName.contains("dbpwd")) return true;
		if (propertyName.contains("enckey")) return true;
		if (propertyName.contains("appenckey")) return true;
		return false;
	}
	
	public static void main(String[] args)
	throws Exception
	{

		
	}
	
	public static boolean isPropertyEquals(String propertyName, String val, boolean ignoreCase)
	{
		String propertyVal = StringUtils.noNull(getProperty(propertyName));
		if (ignoreCase)
		{
			return propertyVal.equalsIgnoreCase(val);
		}
		else
		{
			return propertyVal.equals(val);
		}
	}
	
	public static boolean isPropertyEquals(String propertyName, String val)
	{
		val = StringUtils.noNull(val);
		String propertyVal = StringUtils.noNull(getProperty(propertyName));
		return propertyVal.equals(val);
	}
	
	public static boolean isPropertyEqualsIC(String propertyName, String val)
	{
		val = StringUtils.noNull(val);
		String propertyVal = StringUtils.noNull(getProperty(propertyName));
		return propertyVal.equalsIgnoreCase(val);
	}		
	
	public static String getPropertyNoNull(String key)
	{
		return StringUtils.noNull(getProperty(key));
	}
	
	public static double getPropertyAsDouble(String key)
	throws Exception
	{
		return Double.parseDouble(StringUtils.noNull(getProperty(key)));
	}
	
	public static int getPropertyAsInt(String key)
	throws Exception
	{
		return Integer.parseInt(StringUtils.noNull(getProperty(key)));
	}
	
	public static String getProperty(String key)
	{
		if (!propertyMap.containsKey(key))
		{
			return "";
		}
		else return  propertyMap.get(key).toString();
	}
	
	public static String getPropertyFileContent(String key)
	{
		try
		{
			String fileName = getProperty(key);
			return StringUtils.loadFileAsString(fileName);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "";
		}
	}
	
	public PropertyUtil()
	{
		loadProperties();
	}

	public static String getDbdriver() {
		return dbdriver;
	}

	public static void setDbdriver(String dbdriver) {
		PropertyUtil.dbdriver = dbdriver;
	}

	public static String getDburl() {
		return dburl;
	}

	public static void setDburl(String dburl) {
		PropertyUtil.dburl = dburl;
	}

	public static String getDbuid() {
		return dbuid;
	}

	public static void setDbuid(String dbuid) {
		PropertyUtil.dbuid = dbuid;
	}

	public static String getDbpwd() {
		return dbpwd;
	}

	public static void setDbpwd(String dbpwd) {
		PropertyUtil.dbpwd = dbpwd;
	}

}
