package com.leadics.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.RandomStringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.security.spec.KeySpec;

public class LISecurityUtil 
{

	
    private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private KeySpec ks;
    private SecretKeyFactory skf;
    private Cipher cipher;
    byte[] arrayBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;

    public LISecurityUtil() throws Exception 
    {
        myEncryptionKey = PropertyUtil.getEnckey();
        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
        arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
        ks = new DESedeKeySpec(arrayBytes);
        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
        cipher = Cipher.getInstance(myEncryptionScheme);
        key = skf.generateSecret(ks);
    }

    public String generateRandomPassword()
    {
    	if (PropertyUtil.isPropertyEquals("GenerateNumericOnlyPassword", "Y"))
    	{
        	RandomStringUtils rs = new RandomStringUtils();
        	return rs.randomNumeric(8);
    	}
    	else
    	{
	    	RandomStringUtils rs = new RandomStringUtils();
	    	return rs.randomAlphanumeric(10);
    	}
    }

    public String generateRandomPin()
    {
    	int pinLength = 6;
    	try
    	{
    		pinLength = Integer.parseInt(PropertyUtil.getProperty("SecurityPinLength"));
    		if (pinLength < 4) pinLength = 6;
    	}
    	catch(Exception exception)
    	{
    		pinLength = 6;
    	}
    	
    	RandomStringUtils rs = new RandomStringUtils();
    	return rs.randomNumeric(pinLength);
    }
    
    public String generateOTP()
    {
    	RandomStringUtils rs = new RandomStringUtils();
    	return rs.randomNumeric(6);
    }
    
    public String generateOTPByLen(int len)
    {
    	RandomStringUtils rs = new RandomStringUtils();
    	return rs.randomNumeric(len);
    }
    
    public String generateOTPExternal()
    {
    	RandomStringUtils rs = new RandomStringUtils();
    	return rs.randomNumeric(10);
    }

    public String encrypt(String unencryptedString) 
    {

    	if (PropertyUtil.isPropertyEquals("CustomSecurityEncryptionAlgorithm", "SHA1"))
    	{
    		LICryptoUtilsSHA1 sha1 = new LICryptoUtilsSHA1();
    		return sha1.encryptPassword(unencryptedString);
    	}
    	else if (PropertyUtil.isPropertyEquals("CustomSecurityEncryptionAlgorithm", "AES"))
    	{
    		LICryptoUtilsAES aes1 = new LICryptoUtilsAES();
    		return aes1.encryptPassword(unencryptedString);
    	}    	
    	else
    	{
    		return encryptRegular(unencryptedString);
    	}
    }
    
    public String encrypt(String cif, String unencryptedString) 
    {


    	if (PropertyUtil.isPropertyEquals("CustomSecurityEncryptionAlgorithm", "SHA1"))
    	{
    		LICryptoUtilsSHA1 sha1 = new LICryptoUtilsSHA1();
    		return sha1.encryptPassword(cif,unencryptedString);
    	}
    	else if (PropertyUtil.isPropertyEquals("CustomSecurityEncryptionAlgorithm", "AES"))
    	{
    		LICryptoUtilsAES aes1 = new LICryptoUtilsAES();
    		return aes1.encryptPassword(unencryptedString);
    	}    	    	
    	else
    	{
    		return encryptRegular(unencryptedString);
    	}
    }
    
    public String convertRegularEncryptedToSha1(String value)
    {
    	if (StringUtils.isNullOrEmpty(value)) return "";
    	try
    	{
    		String decrypted = decryptRegular(value);
    		LICryptoUtilsSHA1 sha1 = new LICryptoUtilsSHA1();
    		String shaEncrypted = sha1.encryptPassword(decrypted);
    		return shaEncrypted;
    	}
    	catch(Exception e)
    	{

    		return value;
    	}
    }
    
    public String convertRegularEncryptedToAes(String value)
    {
    	if (StringUtils.isNullOrEmpty(value)) return "";
    	try
    	{
    		String decrypted = decryptRegular(value);
    		LICryptoUtilsAES aes1 = new LICryptoUtilsAES();
    		String aesEncrypted = aes1.encryptPassword(decrypted);
    		return aesEncrypted;
    	}
    	catch(Exception e)
    	{

    		return value;
    	}
    }

    public String encryptRegular(String unencryptedString) 
    {
        String encryptedString = null;
        try {
        	
//        	if (PropertyUtil.isPropertyEquals("CustomSecurityEncryptionAlgorithm", "AES"))
//        	{
//        		LICryptoUtilsAES aes1 = new LICryptoUtilsAES();
//        		return aes1.encryptPassword(unencryptedString);
//        	}

            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
//            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString = new String(Base64.encodeBase64(plainText));
        } catch (Exception e) {

        }
        return encryptedString;
    }

    public boolean validatePassword(String inputPassword, String dbPassword)
    {
    	if (inputPassword == null) return false;
    	if (dbPassword == null) return false;

    	String encryptedVal = encrypt(inputPassword);
    	if (inputPassword.equals(dbPassword)) return true;
    	return false;
    }

    public String decrypt(String encryptedString) 
    {
    	return decryptRegular(encryptedString);
    }

    public String decryptRegular(String encryptedString) {

    	if (PropertyUtil.isPropertyEquals("CustomSecurityEncryptionAlgorithm", "AES"))
    	{
    		LICryptoUtilsAES aes1 = new LICryptoUtilsAES();
    		return aes1.decryptPassword(encryptedString);
    	}

    	String decryptedText=null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText= new String(plainText);
        } catch (Exception e) {

        }
        return decryptedText;
    }
    
    public String decryptStraight(String encryptedString) 
    {
    	String decryptedText=null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText= new String(plainText);
        } catch (Exception e) {

        }
        return decryptedText;
    }
    
    public static void main(String args []) throws Exception
    {
        LISecurityUtil td= new LISecurityUtil();
        
        System.out.println(td.encrypt("root"));
        //System.out.println(td.decrypt("O7Wn62yzJb8="));
        
    }
}
