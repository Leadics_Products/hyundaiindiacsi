/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.utils;

import com.leadics.application.to.ChartsConfig;
import com.leadics.application.to.LIConfigdivRecord;


/**
 *
 * @author krishna-leadics
 */
public class DivMapper {
        public static ChartsConfig toLIConfigdivRecord(LIConfigdivRecord record) {
        ChartsConfig rps = new ChartsConfig();
        rps.setId(Integer.parseInt(record.getId()));
        rps.setParentId(Integer.parseInt(record.getParentId()));
        rps.setTitle(record.getChartName());
        rps.setName(record.getChartName());
        rps.setSubTitle(record.getChartName());
        rps.setSlugName(record.getDivName());
        rps.setType(record.getChartType());
        rps.setActionPlanning((record.getActionPlanning()));
        rps.setExtraConfig(record.getExtraConfig());
        rps.setData(record.getData());
        rps.setPageName(record.getPageName());
        rps.setSub(null);
        
        return rps;
    }
}
