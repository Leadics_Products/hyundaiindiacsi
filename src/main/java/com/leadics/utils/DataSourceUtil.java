package com.leadics.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DataSourceUtil {
    public static Properties properties = new Properties();

    static {
        Log log = LogFactory.getLog(DataSourceUtil.class);
        try {
            String resourceName = "datasource.properties";
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
                properties.load(resourceStream);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        return properties.get(key).toString();
    }
}
