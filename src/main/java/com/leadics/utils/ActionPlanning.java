/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.utils;

import com.leadics.application.to.NameValuePair;

/**
 *
 * @author krishna-leadics
 */
public class ActionPlanning {

    private NameValuePair area;
    private NameValuePair kpi;
    private NameValuePair category;

    public NameValuePair getArea() {
        return area;
    }

    public void setArea(NameValuePair area) {
        this.area = area;
    }

    public NameValuePair getKpi() {
        return kpi;
    }

    public void setKpi(NameValuePair kpi) {
        this.kpi = kpi;
    }

    public NameValuePair getCategory() {
        return category;
    }

    public void setCategory(NameValuePair category) {
        this.category = category;
    }
}
