package com.leadics.utils;

import org.apache.log4j.Logger;

import java.security.MessageDigest;

public class LICryptoUtilsSHA1 {

	private static Logger logger = Logger.getLogger(LICryptoUtilsSHA1.class);
	public static byte[] computeHash(String x) throws Exception {
		MessageDigest digest = MessageDigest.getInstance("SHA-1");
		digest.reset();
		digest.update(x.getBytes());
		return digest.digest();
	}

	public static String encryptPassword(String password) 
	{
		try 
		{
			byte[] pinData = LICryptoUtilsSHA1.computeHash(password);
			return LICryptoUtilsSHA1.byteArrayToHexString(pinData);
		}
		catch(Exception ex) 
		{
			logger.error(ex);
			//log.warn("Error while encrypting Password :"+ex);
			return "";
		}
	}
	
	public static String encryptPassword(String cif, String password) 
	{
		try 
		{
			if (PropertyUtil.isPropertyEquals("PrependCIFTOPIN", "Y"))
			{
				password = StringUtils.noNull(cif) + StringUtils.noNull(password);
			}
			
			byte[] pinData = LICryptoUtilsSHA1.computeHash(password);
			return LICryptoUtilsSHA1.byteArrayToHexString(pinData);
		}
		catch(Exception ex) 
		{
			logger.error(ex);
			//log.warn("Error while encrypting Password :"+ex);
			return "";
		}
	}

	public static String byteArrayToHexString(byte[] b) {
		StringBuffer sb = new StringBuffer(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			int v = b[i] & 0xff;
			if (v < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
		}
		return sb.toString().toUpperCase();
	}

}
