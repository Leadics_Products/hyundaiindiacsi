package com.leadics.utils;

public class ChartData {
	private Double y;
    private String color;
    private String name;
    private ChartData data;
    private String filterValue;
    private String chartType;

    public ChartData getData() {
        return data;
    }

    public void setData(ChartData data) {
        this.data = data;
    }

    public ChartData(Double y, String color, String name) {
        this.y = y;
        this.color = color;
        this.name = name;
    }

    public ChartData(Double y, String color) {
        this.y = y;
        this.color = color;
    }

    public ChartData(String filterValue, Double y, String color,String chartType){
        this.y = y;
        this.color = color;
        this.name = name;
        this.filterValue=filterValue;
        this.chartType=chartType;
    }
    public ChartData(Double y, String color, String name,String filterValue) {
        this.y = y;
        this.color = color;
        this.name = name;
        this.filterValue=filterValue;
    }

    public ChartData(String filterValue,Double y,String color){
        this.y=y;
        this.color=color;
        this.filterValue=filterValue;
    }
    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public String getColor() {
        return color;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
