package com.leadics.utils;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.leap.core.services.users.spi.UserClient;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import static com.leadics.application.common.LIService.DEFAULT_CRITERIA;

public class ScopeGuavaCacheUtil {
    private static LoadingCache<String, Map<String,String>> scopeCache;
    static {
        scopeCache = CacheBuilder.newBuilder()
                .maximumSize(20)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<String, Map<String,String>>() {
                            @Override
                            public Map<String,String> load(String id) throws Exception {
                                return getScopeById(id);
                            }

                        }
                );
    }
    public static LoadingCache<String, Map<String,String>> getLoadingCache() {
        return scopeCache;
    }
    public static Map<String,String> getScopeById(String id) throws Exception {
                    Set<String> objectTypes = new LinkedHashSet<>();
                    objectTypes.add(DEFAULT_CRITERIA);
                    UserClient userClient = new UserClient();
                    HierarchicalLookupData hierarchicalLookupData = new HierarchicalLookupData();
                    Set transformerRepresentations = hierarchicalLookupData.getHierarchicalLookupData();
                    Map<String, String> criteriaMap = userClient.fetchUserDataScope(id, Integer.parseInt(PropertyUtil.getProperty("APPLICATION_ID")), objectTypes, transformerRepresentations);
                    return criteriaMap;
    }
}
