/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.utils;

import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leadics.application.common.LIDAO;
import com.leadics.application.common.LIService;
import com.leap.core.common.representations.DataTransformerRepresentation;
import com.leap.core.common.representations.NameValueRepresentation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author root
 */
public class HierarchicalLookupData {

    public Set<DataTransformerRepresentation> getHierarchicalLookupData() throws Exception {
        LIService service = new LIService();
        LIDAO dao = new LIDAO();
        String queryDealer = "select distinct dealer_code as dealer,id as dealer_id from master_table_dealer;";
        HashMap<String, String> columnInfos = new HashMap<>();
        columnInfos.put("dealer", "dealer");
        columnInfos.put("dealer_id", "dealer_id");
        System.out.println("Hirechy data_________________________++++++++++++++ "+queryDealer);
        List<HashMap<String, String>> dealerData = dao.loadValuesRowWise(queryDealer, columnInfos);
        Set<DataTransformerRepresentation> outData = dealerData.stream().collect(
                Collectors.mapping(row -> new DataTransformerRepresentation("Dealer", getDataInSet(row.get("dealer"), row.get("dealer_id"))),
                        Collectors.toSet())
        );



        String queryCity = "select distinct region,region_id from filter_aggregate;";
        HashMap<String, String> columnInfoCity = new HashMap<>();

        columnInfoCity.put("region", "region");
        columnInfoCity.put("region_id", "region_id");
        System.out.println("Hirechy data_________________________++++++++++++++ "+queryCity);
        List<HashMap<String, String>> cityData = dao.loadValuesRowWise(queryCity, columnInfoCity);


        outData.addAll(cityData.stream().collect(
                Collectors.mapping(row -> new DataTransformerRepresentation("Region", getDataInSet(row.get("region"), row.get("region_id"))),
                        Collectors.toSet())
        ));
        return outData;

    }

    private Set<NameValueRepresentation<String, String>> getDataInSet(String Name, String Value) {
        Set<NameValueRepresentation<String, String>> returnData = new HashSet();
        NameValueRepresentation nameValue = new NameValueRepresentation();
        nameValue.setName(Name);
        nameValue.setValue(Value);
        returnData.add(nameValue);
        return returnData;

    }

}
