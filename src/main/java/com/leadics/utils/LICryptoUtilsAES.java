package com.leadics.utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class LICryptoUtilsAES
{
 	public static String encryptPassword(String strToEncrypt)
    {
        try
        {
        	String key = "KPr42187Bar22185"; // 128 bit key
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            final String encryptedString = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes()));
            return encryptedString;
        }
        catch (Exception e)
        {
           e.printStackTrace();
        }
        return null;
    }

    public static String decryptPassword(String strToDecrypt)
    {
        try
        {
        	String key = "KPr42187Bar22185"; // 128 bit key
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            final SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            final String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
            return decryptedString;
        }
        catch (Exception e)
        {
          e.printStackTrace();

        }
        return null;
    }

    public static void main(String args[])
    {

        final String strToEncrypt = "VBUSER123";
        final String encryptedStr = LICryptoUtilsAES.encryptPassword(strToEncrypt.trim());
        final String decryptedStr = LICryptoUtilsAES.decryptPassword(encryptedStr.trim());
        //System.out.println("Original:" + strToEncrypt);
        //System.out.println("Encrypted:" + encryptedStr);
        //System.out.println("Decrypted:" + decryptedStr);
    }
}
