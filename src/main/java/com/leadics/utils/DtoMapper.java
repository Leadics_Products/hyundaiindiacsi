/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.utils;

import com.leadics.application.representation.MenuRepresentation;
import com.leadics.application.to.LIConfigpagesRecord;

/**
 *
 * @author harsha
 */
public class DtoMapper {
    
    public static MenuRepresentation toMenuRepresentation(LIConfigpagesRecord record) {
        MenuRepresentation rps = new MenuRepresentation();
        rps.setActive(record.getActive());
        rps.setBudge(record.getBudge());
        rps.setBudgeColor(record.getBudgeColor());
        rps.setExternalLink(rps.getExternalLink());
        rps.setGroupTitle(record.getGrouptitle());
        rps.setIcon(record.getIcon());
        rps.setId(record.getId());
        rps.setPageName(record.getPageName());
        rps.setParentId(record.getParentId());
        rps.setRouting(record.getRouting());
        rps.setSub(null);
        rps.setRank(record.getRank());
        rps.setTitle(record.getTitle());
        return rps;
    }
    
    
    
    
}
